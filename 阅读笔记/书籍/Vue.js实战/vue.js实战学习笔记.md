>书本来源: vue.js实战




# 概念
- 思考渲染的概念, 结合生命周期
-MVVM
    -  Model-View-View Model
- 数据绑定, 还双向?
  Vue的核心是数据与视图的双向绑定
    - 通过插值与表达式vue会在实体dom与js代码之间建立关联, 二者操作同一对象.
- 插值与表达式: {{}}
    - Vue.data中的变量
    - 三目运算符
    - Vue.computed 中定义的计算属性
    - Vue.methods 定义的方法 使用时需要加括号, 例如:`fun()`  
    - Vue.filters 中定义的方法, 需要通过|连接,
        - 示例: {{x | filter1}}, 这时候会将x当做参数传递给filter1方法, 并且显示返回值,
        - 多个filters的执行顺序是从左到右


- 指令（ Directives ） ，它带有前缀 v-, 指令的主要职责就是当其表达式的值改变时，相应地将某些行为应用到 DOM 上.
    - v-if v-html v-pre


- 计算属性(computed)1-5demo.html
    -  每个计算属性包含两个方法getter和setter, 使用同名方法时就相当于用getter方法
    - computed什么时候执行计算的? (methods也是如此)
      - 比如通过computed将两个date对象绑定在一起返回个页面显示
      - 当任意一个对象修改时,页面的值都会发生变化
- 方法(methods)
    - 他和computed很相似,区别是对缓存的依赖, methods不依赖于缓存
    