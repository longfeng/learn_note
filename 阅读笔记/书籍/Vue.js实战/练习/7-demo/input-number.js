Vue.component('input-number', {
    template:
        '<div class="input-number">' +
        '<input type= "text" :value="currentValue" @change= "handleChange"/>' +
        '<button  @click= "handleDown" :disabled= "currentValue <= min" >-</button> ' +
        '<button  @click= "handleUp" :disabled="currentValue >= max">+</button>' +
        '</div>',
    data: function () {
        return {

            currentValue: this.value
        }
    },
    watch: {
        currentValue: function (val) {
            this.$emit('input', val);
            this.$emit('on-change', val);
        },
        value: function (val) {
            this.updateValue(val);
        },
    },
    methods: {
        handleDown: function () {
            if (this.currentValue <= this.min) return;
            this.currentValue -= 1;
        },
        handleUp: function () {
            if (this.currentValue >= this.max) return;
            this.currentValue += 1;
        },
        updateValue: function (val) {
            if (val > this.max) val = this.max;
            if (val < this.min) val = this.min;
            this.currentValue = val;
        }

    },
    mounted: function () {
        this.updateValue(this.value);
    },
    props: {
        max: {
            type: Number,
            default: Infinity
        },
        min: {
            type: Number,
            default: -Infinity
        },
        value: {
            type: Number,
            default: 0
        },
    }
});