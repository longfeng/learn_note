# 类加载机制

双亲委派机制

# 图灵学院架构师视频4期学习笔记

熟悉程度:
了解: 完整看完视频
熟悉: 敲过部分代码或者整理过笔记
掌握: 将内容应用到工作上,并且有效
精通: 做公开课

# 性能调优
## JVM[掌握]
## TOMCAT[熟悉]
## MYSQL[掌握]

---
# JVM内存模型 

---
# 垃圾收集算法
## 标记清除
## 标记整理

---
# 垃圾收集器
## CMS 
## G1
## ZGC

---

# 调优
- 根据实际业务场景设计内存模型(old对象大小定义, old对象岁数定义, old区域大小定义) -- cms only
- 在jvm出现oom时打印堆栈信息(怎么解析?)

---
# 工具
## jvisualvm - 使用方式
- 是啥: jvm分析工具, JDK安装目录bin里提供的工具(JDK14起中不在携带, 需要手动[下载](https://visualvm.github.io/download.html)) 相比JConsole更多特性。
- 远程使用:
```shell
# 在jar命令后面追加命令
# -Djava.rmi.server.hostname=192.168.12.211 // 主机地址,visualVM使用此地址连接
# -Dcom.sun.management.jmxremote=true // 运行远程链接jvm虚拟机
# -Dcom.sun.management.jmxremote.port=48084 // 远程链接jvm的端口(自定义,每个应用独立)
# -Dcom.sun.management.jmxremote.rmi.port=58084  // RMI端口,不指定的话jvm会自动随机分配,此时需要关注系统防火墙
# -Dcom.sun.management.jmxremote.ssl=false // 是否使用ssl加密传输
# -Dcom.sun.management.jmxremote.authenticate=false // 是否需要认证
java -jar -Djava.rmi.server.hostname=192.168.12.211 -Dcom.sun.management.jmxremote=true -Dcom.sun.management.jmxremote.port=48084 -Dcom.sun.management.jmxremote.rmi.port=58084  -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Dfile.encoding=UTF-8 -Dspring.profiles.active=dev xiaohh-cost-1.0.0.jar

```
- 插件:
  - 安装方式： 工具->插件->可用插件->(选择)->安装
  - 常用插件:
    - Visual GC: GC可视化分析插件：
- 在idea快捷启动: (启动时会自动打开VisualVM工具)
  - 在idea中安装`VisualVM Launcher`插件
  - 安装完成后,依次进入`File`->`Settings`->`Other Settings`->`VisualVM Launcher`
  - 指定executable:`C:/Program Files/Java/jdk1.8.0_281/bin/jvisualvm.exe`
  - 在调试按钮旁边多了两个按钮(Run with VisualVM和Debug with VisualVM)按钮, 相当于它在jar启动前自动加了那几行参数

- CPU高企问题排查
- JVM内存溢出问题


## 常用命令
### GC日志 [cankao](https://blog.csdn.net/shudaqi2010/article/details/104666951)
```shell
# 只能得到当前系统运行情况的一行指标
jstat -gc PID
# 每秒执行一次，一直不停的执行
jstat -gc PID 1000
# 每秒执行一次，共执行10次
jstat -gc PID 1000 10
```
- S0C：第一个幸存区的大小，单位KB
- S1C：第二个幸存区的大小
- S0U：第一个幸存区的使用大小
- S1U：第二个幸存区的使用大小
- EC：伊甸园区的大小
- EU：伊甸园区的使用大小
- OC：老年代大小
- OU：老年代使用大小
- MC：方法区大小(元空间)
- MU：方法区使用大小
- CCSC:压缩类空间大小
- CCSU:压缩类空间使用大小
- YGC：年轻代垃圾回收次数
- YGCT：年轻代垃圾回收消耗时间，单位s
- FGC：老年代垃圾回收次数
- FGCT：老年代垃圾回收消耗时间，单位s
- GCT：垃圾回收消耗总时间，单位s

### 堆内存统计
```shell
jstat -gccapacity PID

# 使用下面的命令可以将结果/1000,近似MB单位
jstat -gccapacity 1 | awk '{printf("%.2fMB %.2fMB %.2fMB %.2fMB %.2fMB %.2fMB %.2fMB %.2fMB %.2fMB %.2fMB %.2fMB %.2fMB %.2fMB %.2fMB %.2fMB %d %d\n", $1/1000, $2/1000, $3/1000, $4/1000, $5/1000, $6/1000, $7/1000, $8/1000, $9/1000, $10/1000, $11/1000, $12/1000, $13/1000, $14/1000, $15/1000, $16, $17)}'
```
- NGCMN: 当前新生代（Young Generation）占用的最小内存容量（KB）
- NGCMX: 当前新生代占用的最大内存容量（KB）
- NGC: 当前新生代占用的当前内存容量（KB）
- S0C: 当前Survivor 0区（From Space）占用的内存容量（KB）
- S1C: 当前Survivor 1区（To Space）占用的内存容量（KB）
- EC: 当前Eden Space（新生代中的非Survivor区）占用的内存容量（KB）
- OGCMN: 当前老年代（Old Generation）占用的最小内存容量（KB）
- OGCMX: 当前老年代占用的最大内存容量（KB）
- OGC: 当前老年代占用的当前内存容量（KB）
- OC: 当前堆（Heap）占用的当前内存容量（KB）
- MCMN: 当前元数据（Metaspace）占用的最小内存容量（KB）
- MCMX: 当前元数据占用的最大内存容量（KB）
- MC: 当前元数据占用的当前内存容量（KB）
- CCSMN: 当前压缩类空间（Compressed Class Space）占用的最小内存容量（KB）
- CCSMX: 当前压缩类空间占用的最大内存容量（KB）
- CCSC: 当前压缩类空间占用的当前内存容量（KB）
- YGC: 当前Young GC的次数
- FGC: 当前Full GC的次数

##  问题
### 使用jmap时报错
```shell
[root@CCS-ES-UAT jdk]# jmap -heap 12295
Attaching to process ID 12295, please wait...
Exception in thread "main" java.lang.reflect.InvocationTargetException
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:606)
	at sun.tools.jmap.JMap.runTool(JMap.java:197)
	at sun.tools.jmap.JMap.main(JMap.java:128)
Caused by: sun.jvm.hotspot.runtime.VMVersionMismatchException: Supported versions are 24.79-b02. Target VM is 25.40-b25
	at sun.jvm.hotspot.runtime.VM.checkVMVersion(VM.java:234)
	at sun.jvm.hotspot.runtime.VM.<init>(VM.java:297)
	at sun.jvm.hotspot.runtime.VM.initialize(VM.java:368)
	at sun.jvm.hotspot.bugspot.BugSpotAgent.setupVM(BugSpotAgent.java:598)
	at sun.jvm.hotspot.bugspot.BugSpotAgent.go(BugSpotAgent.java:493)
	at sun.jvm.hotspot.bugspot.BugSpotAgent.attach(BugSpotAgent.java:331)
	at sun.jvm.hotspot.tools.Tool.start(Tool.java:163)
	at sun.jvm.hotspot.tools.HeapSummary.main(HeapSummary.java:40)
```
- 原因: 机器上有两个jdk, jmap命令的jdk版本比运行java应用的jdk低.

### 如何查看JVM使用了哪些参数
- jinfo -flags PID

