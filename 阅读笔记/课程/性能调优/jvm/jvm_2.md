-- 查看jvm运行参数
jinfo -flags 48845

-- 查看堆栈 类占用
jmap -histo 48845 > ./log.txt
-- 查看堆栈 分配
jmap -heap 48845 > ./log.txt

-- 手动生成堆内存dump
jmap -dump:file=test.dump 48845

https://blog.csdn.net/sayyy/article/details/100081737
-- jvm 参数 oom时输出dump文件
-XX:+HeapDumpOnOutOfMemoryError
-XX:HeapDumpPath=./

https://blog.csdn.net/m0_52325992/article/details/119155477
-- gc日志 pid 间隔毫秒 次数
jstat -gc 48845 5000 10
S0C：第一个幸存区的大小，单位KB
S1C：第二个幸存区的大小
S0U：第一个幸存区的使用大小
S1U：第二个幸存区的使用大小
EC：伊甸园区的大小
EU：伊甸园区的使用大小
OC：老年代大小
OU：老年代使用大小
MC：方法区大小(元空间)
MU：方法区使用大小
CCSC:压缩类空间大小
CCSU:压缩类空间使用大小
YGC：年轻代垃圾回收次数
YGCT：年轻代垃圾回收消耗时间，单位s
FGC：老年代垃圾回收次数
FGCT：老年代垃圾回收消耗时间，单位s
GCT：垃圾回收消耗总时间，单位s

-- 堆内存统计 
jstat -gccapacity 48845
NGCMN：新生代最小容量
NGCMX：新生代最大容量
NGC：当前新生代容量
S0C：第一个幸存区大小
S1C：第二个幸存区的大小
EC：伊甸园区的大小
OGCMN：老年代最小容量
OGCMX：老年代最大容量
OGC：当前老年代大小
OC:当前老年代大小
MCMN:最小元数据容量
MCMX：最大元数据容量
MC：当前元数据空间大小
CCSMN：最小压缩类空间大小
CCSMX：最大压缩类空间大小
CCSC：当前压缩类空间大小
YGC：年轻代gc次数
FGC：老年代GC次数