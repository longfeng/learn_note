// 根据课程id,获取课程信息, 需要在任意课程详情页中执行
function getTitle(courseId){
    $.ajax({
        "async":false,
        "type": "GET",
        "headers":{
            "x-l-req-header":'{"deviceType":1}'
        },
        "url": "https://gate.lagou.com/v1/neirong/kaiwu/getCourseLessons?courseId="+courseId,
        "success": function(data){
            // console.log(data.content.courseName);
            let nodeNames = "\r\n--------------------------\r\n"+data.content.courseName+"\r\n--------------------------\r\n";
            let arr = {};
            let arrIndex = 0;
            for (let index in data.content.courseSectionList){
                let mode = data.content.courseSectionList[index];
                for (let index in mode.courseLessons){
                    let node = mode.courseLessons[index];
                    arr[arrIndex++] = node.id;
                    let nodeName = node.theme;
                    nodeName = nodeName.replace(/[\s\\?？。]+/g,"");
                    nodeName = nodeName.replace(/[：|，\\/<>]+/g,"_");
                    nodeNames += (nodeName+"\r\n");
                }
            }

            let nodeUrls = "";
            for(var index in arr){
                // 课程学习地址
                let url = "https://kaiwu.lagou.com/course/courseInfo.htm?courseId="+courseId+"#/detail/pc?id="+arr[index];
                nodeUrls += (url+"\r\n");
            }
            console.log(nodeNames+nodeUrls);
        }
    });
    // 课程购买地址
    console.log("https://kaiwu.lagou.com/course/courseInfo.htm?courseId="+courseId);
}
// getTitle(18);
// clear();







// // ,18,20,42,43,45,46,50,64,67,71,81,144,158,180,185,230,251,257,274,287,326,377,405,416,433,440,447
// // 根据课程id,获取课程信息
let arr = [534,608,638,69];
for (var i in arr){
    getTitle(arr[i]);
}

