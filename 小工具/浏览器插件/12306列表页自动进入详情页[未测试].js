// ==UserScript==
// @name         12306列表页自动进入详情页
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  12306列表页自动进入详情页
// @author       Leon
// @match        *://kyfw.12306.cn/otn/leftTicket/init*
// @icon         https://www.google.com/s2/favicons?domain=12306.cn
// @grant        none
// ==/UserScript==

(function() {
    'use strict';


    function test(){
        console.log("su");
        $("#qd_closeDefaultWarningWindowDialog_id").click();
        console.log(new Date().getTime() - startTime);
    }


    var startTime = new Date().getTime();
    /**
     * 等待页面加载完成,
     */
    function start(callback, callbackParam){
        if (typeof $ != 'undefined' && document.body) {
            callback(callbackParam);
        } else {
            setTimeout(start,100, callback, callbackParam)
        }
    }

    function getTicketNode(name){
        //console.log("name:"+name);
        let $node = null;
        $('a[title="点击查看停靠站信息"]').each(function(){
                //console.log($(this).text());
                if(name == $(this).text()){
                    var $aNode = $(this).parents("tr:first").find("td:last");
                    if ($aNode.length==1){
                        console.log("找到车次:"+name);
                        $node = $aNode;
                    }
                }
        });
        if($node!=null){
            let $bnode = $node.find("a");
            if($bnode.length==1){
                $node = $bnode;
            }else{
                console.log("车次未开售");
            }
        }else{
            console.log("未找到车次");
        }
        return $node;
    }

    // $("#62000G607804_AOQ_IOQ").parents("tr:first").find("td:last")
    function main(){
        setInterval(getTicketNode,1000,"G6074");
    }

    start(main);

})();