// ==UserScript==
// @name         火车票自动选座工具
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  自动选座,并且自动显示提交按钮, 只要点击一次就可以购买过车票
// @author       leon
// @match        *://kyfw.12306.cn/otn/confirmPassenger/init*
// @icon         https://www.google.com/s2/favicons?domain=12306.cn
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // 这一步先选择一个有票的车次与日期, 然后执行检查, 确认没问题之后再开始
    /** 检查按钮是否发生变化以及列举联系人 */
    function checkButton(){
        var validLinker = $("#normalPassenger_0").length==1;
        console.log("检查联系人复选框["+validLinker+"]");
        if (validLinker){
            $("#normal_passenger_id >li >label").each(function(){
                console.log($(this).text()+":"+$(this).prev().attr("id"));
            })
        }
        console.log("------------");
        console.log("检查提交按钮["+($("#submitOrder_id").text()=="提交订单")+"]");
        console.log("检查返回按钮["+($("#back_edit_id").length==1)+"]");
        console.log("检查确认按钮["+($("#qr_submit_id").text()=="确认")+"]");
    }

    // 自动抢票程序开始
    let Param = {

        /** 是否自动提交(一天做多取消三次) */
        isAutoSubmit:false,

        /** 参与买票联系人ID */
        linker:[
            "#normalPassenger_0",
            "#normalPassenger_5",
            "#normalPassenger_8",
        ],
        startTime:new Date().getTime(),
        submitStartTime:null,
    }

    /** 主方法 */
    function main(){

        // 勾选联系人
        Param.linker.forEach((item)=>{$(item).click();});

        // 提交选择联系人
        $("#submitOrder_id").click();

        // 选座 确认
        if( isContainsO ){
            Param.submitStartTime = new Date().getTime();
            autoSubmit();
        }

    }

    /** 自动选座 */
    function selectSeat(){
        console.log(Param.linker.length);
        if(Param.linker.length == 1){
            // 1人选F
            $("#1F").click();
        }else if(Param.linker.length == 2){
            // 2人选D,F
            $("#1D").click();
            $("#1F").click();
        }else if(Param.linker.length == 3){
            // 3人选A,B,C
            $("#1A").click();
            $("#1B").click();
            $("#1C").click();
        }else{
            // 超出3人(不选)自动分配
        }
    }

    /** 自动确认 */
    function autoSubmit(){
        if ($("#back_edit_id").length==1 && $("#qr_submit_id").hasClass("btn92s")) {
            console.log("加载确认按钮,耗时:["+(new Date().getTime()-Param.submitStartTime)+"]ms");
            selectSeat();
            if (Param.isAutoSubmit){
                $("#qr_submit_id").click();
                console.log("自动提交成功");
            }else{
                console.log("手动提交");
            }
            console.log("程序结束");
        } else {
            setTimeout(autoSubmit,10);
        }
    }

    /** 是否存在二等座 */
    function isContainsO(){
        let containsO = false;
        $("#seatType_1  option").each(function(){
            if($(this).val()=="O"){
                containsO=true;
            }
        });
        if (containsO == false){
            console.log("二等座已经卖完, 停止自动提交!")
        }
        return containsO;
    }

    /** 等待页面加载完成 */
    function loading(){
        if (typeof $ != 'undefined' && document.body && $("#normalPassenger_0").length==1) {
            console.log("加载页面,耗时:["+(new Date().getTime()-Param.startTime)+"]ms");
            checkButton();
            main();
        } else {
            setTimeout(loading,10);
        }
    }

    /** 开始执行 */
    loading();

})();