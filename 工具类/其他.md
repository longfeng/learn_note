### Hutool工具包备忘
- [官方文档](https://www.hutool.cn/docs/#/)

- 键值对对象 `new Pair<String, String>("key", "value");`

- 格式化字符串 `String result1 = StrFormatter.format("this is {} for {}", "a", "b");`
  
### GitHub host工具
- 页面:https://github.com/ineo6/hosts
- 先安装: https://github.com/oldj/SwitchHosts/releases
- 再安装https://github.com/ineo6/hosts
  - .\hosts-server.exe --port=18888
- 配置SwitchHosts地址
