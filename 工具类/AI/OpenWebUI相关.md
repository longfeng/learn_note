# OpenWebUI相关
## 前置准备
- 官网: https://docs.openwebui.com/

## windows非docker安装(conda) [参考B站视频](https://www.bilibili.com/video/BV1b8mVY2EZj/?spm_id_from=333.337.search-card.all.click&vd_source=bc072343c9e148f3dc558050e9a29e40)
- 步骤如下:
  - 安装miniconda: [下载地址](https://repo.anaconda.com/miniconda/)
  - 配置镜像源: https://mirrors.tuna.tsinghua.edu.cn/help/anaconda/
  - 安装python环境`conda create -n py311 python=3.11`
  - 激活环境`conda activate py311`
  - 在py311环境中安装openWebUI: `pip install -i https://mirrors.tuna.tsinghua.edu.cn/pypi/web/simple open-webui`
  - 在py311环境中启动openWebUI: `open-webui serve`
- 后续的启动
  - 先激活环境: `conda activate py311`
  - 启动: `open-webui serve`
- 默认访问地址: http://localhost:8080/