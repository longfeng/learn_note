# Ollama相关知识
## 前置准备
- 官网: https://ollama.com

## 安装 (参考官网)
- 备注: windows修改models目录(假设目标目录为:E:\Ollama), 步骤如下[参考](https://blog.csdn.net/qq_40064717/article/details/145406752)
  - 将安装目录`C:\Users\用户名\AppData\Local\Programs\Ollama` 移动到`E:\ollama`
  - 设置环境变量(建议`系统变量`)
    - 创建环境变量`OLLAMA_MODELS`,值为`E:\ollama\models`
    - 将`E:\ollama`加入到`Path`中
## 命令示例:
```shell
# 查看版本
ollama -v
# 查看帮助
ollama -h
# 查看本地模型
ollama list
# 下载文件(下载过程中可以中断, 下次继续下载)
ollama pull <模型名称>
# 运行模型
ollama run <模型名称>
# 运行模型, 以调试模式运行
ollama run --verbose <模型名称>
# 停止模型(在界面输入/bye后5分钟左右也会自动退出)
ollama stop <模型名称>
```
## 其他
- 至少使用14B及以上的模型, 其他模型都比较笨
- 建议使用32B模型, 综合性能和效果比较好
