# 刷机 - 官方安装包
## 下载[官方安装包](https://firmware-selector.openwrt.org/)
  - [推荐]SYSUPGRADE (EXT4) 表示可修改的镜像
  - SYSUPGRADE (SQUASHFS) 表示不可修改镜像, 若出问题, 可以通过系统重置功能恢复, 但是很多配置不能随意更改
## 下载刷机工具(将安装包刷入tf卡)
  - 刷机工具[下载地址](https://www.balena.io/etcher/)
  - 将tf卡插入电脑
  - 右键以`管理员身份运行`这个刷机工具, 
  - 选择`镜像` -> 选择`tf卡所在的盘` -> 开刷
  - 完成
## 系统初始化配置
### 安装必要插件
```shell
# 进入SSH
ssh root@192.168.1.1
# 更新可用软件列表
opkg update
# 安装中文语言包(无需重启),
opkg install luci-i18n-base-zh-cn
# 安装挂载点功能(需重启),
opkg install install block-mount
# 安装luci相关基础依赖
opkg install luci luci-base luci-compat
```
### 调整Free space空间大小(系统默认只给分配了100M的空间)
#### 1. 使用shell分区, 并且格式化
```shell
# 进入SSH
ssh root@192.168.1.1
# 更新可用软件列表
opkg update
# 安装磁盘工具和分区工具
opkg install cfdisk fdisk
# 查看磁盘情况
fdisk -l
# -------- start fdisk -l 
#  这里得到的磁盘路径是/dev/mmcblk0, 只做了两个分区16M,104M
# root@OpenWrt:~# fdisk -l
# Disk /dev/mmcblk0: 29.55 GiB, 31727812608 bytes, 61968384 sectors
# Units: sectors of 1 * 512 = 512 bytes
# Sector size (logical/physical): 512 bytes / 512 bytes
# I/O size (minimum/optimal): 512 bytes / 512 bytes
# Disklabel type: dos
# Disk identifier: 0x5452574f
# 
# Device         Boot  Start    End Sectors  Size Id Type
# /dev/mmcblk0p1 *     65536  98303   32768   16M 83 Linux
# /dev/mmcblk0p2      131072 344063  212992  104M 83 Linux
# -------- end fdisk -l

# 磁盘分区
cfdisk /dev/mmcblk0
# -------- start cfdisk /dev/mmcblk0 
#                                   Disk: /dev/mmcblk0
#                  Size: 29.55 GiB, 31727812608 bytes, 61968384 sectors
#                           Label: dos, identifier: 0x5452574f
# 
#     Device               Boot     Start            End        Sectors      Size      Id Type
#     Free space                     2048          65535          63488       31M
#     /dev/mmcblk0p1       *        65536          98303          32768       16M      83 Linux
#     Free space                    98304         131071          32768       16M
#     /dev/mmcblk0p2               131072         344063         212992      104M      83 Linux
# >>  Free space                   344064       61968383       61624320     29.4G      83 Linux
# 
#  接下来的操作步骤
#  1.选中最后一个Fee space,选择[new]菜单, 回车
#  2.选择[primary]菜单, 回车
#  3.选择[Write]菜单, 回车
#  4.输入yes, 回车
#  5.选择Quit退出
# -------- end cfdisk /dev/mmcblk0 

#再次查看磁盘分区情况
fdisk -l
# -------- start fdisk -l
# ...
# 此时已经看到已经多了一个分区:`/dev/mmcblk0p3`
# Device         Boot  Start      End  Sectors  Size Id Type
# /dev/mmcblk0p1 *     65536    98303    32768   16M 83 Linux
# /dev/mmcblk0p2      131072   344063   212992  104M 83 Linux
# /dev/mmcblk0p3      344064 61968383 61624320 29.4G 83 Linux
# -------- end fdisk -l

# 格式化分区
mkfs.ext4 /dev/mmcblk0p3
# 输入 y 确认

# 安装挂载点(需要重启)
opkg install block-mount

# 重启后进入web端操作
reboot
```
#### 2. 使用web端进行磁盘挂载 http://192.168.1.1
- 进入`系统`->`挂载点`页面
  - 点击`生成配置`按钮(刷新页面磁盘情况)
  - 在`挂载点`模块, 找到刚才新建的分区, 点击编辑, (此时会弹窗)
    - 勾选 `已启用`
    - 挂载点选择 `作为根文件系统使用`
    - 复制根目录准备里面的shell命令,等会需要用到, 
    - 点击保存
  - 点击保存并应用 
#### 3. 使用shell执行刚才cpy的命令
- 注意:需要将其中的`/dev/sda1`替换成需要替换成选中分区的路径(本文的分区路径为`/dev/mmcblk0p3`)
```shell
# 逐一执行, 理论上不能报错(报错需要多试几次,估计试前面某一步出错了), 否则重启后会无法进入系统
mkdir -p /tmp/introot
mkdir -p /tmp/extroot
mount --bind / /tmp/introot
mount /dev/mmcblk0p3 /tmp/extroot
tar -C /tmp/introot -cvf - . | tar -C /tmp/extroot -xf -
umount /tmp/introot
umount /tmp/extroot
# 不报错的话就重启, 否则重新刷系统吧
reboot
```

---
### [推荐]如何安装插件-通过wget直接下载到本地安装,这里以`文件传输插件`举例
```shell
## 下载安装包
wget https://op.supes.top/packages/aarch64_generic/luci-app-filetransfer_git-23.260.37888-e2c1089_all.ipk
## 下载依赖(没有这个,filetransfer安装不了)
wget https://op.supes.top/packages/aarch64_generic/luci-lib-fs_1.0-14_all.ipk
## 安装依赖插件
opkg install luci-lib-fs_1.0-14_all.ipk
## 安装插件
opkg install luci-app-filetransfer_git-23.260.37888-e2c1089_all.ipk
``` 
### 通过web界面安装
1. 先安装`luci-app-filetransfer`插件
2. 

Package aliyundrive-webdav version 1.8.0-1 has no valid architecture


: No such file or directory

Package dnsmasq-full wants to install file /etc/hotplug.d/ntp/25-dnsmasqsec
But that file is already provided by package  * dnsmasq


Existing conffile /etc/config/dhcp is different from the conffile in the new package. The new conffile will be placed at /etc/config/dhcp-opkg.
# AdGuard Home
### AdGuard Home 忘记密码解决办法 - openWrt系统
- 参考:
  - [非官方](https://opssh.cn/luyou/245.html)
- 步骤: 以下是将默认密码修改为root
1. 通过[Bcrypt密码生成计算器](https://www.jisuan.mobi/p163u3BN66Hm6JWx.html)生成密码
  - 密码填`root`
  - Rounds填`10`
2. 登录到机器,执行下面的操作
```shell
# 登录到openwrt机器
ssh root@192.168.2.1
# 备份配置
cp /etc/AdGuardHome.yaml /etc/AdGuardHome.yaml.bk
# 停止AdGuardHome服务
service AdGuardHome stop
# 为了确保服务已经停止, 通过`ps -ef|grep AdGuardHome`再检查一次, 如果还有进程, 则通过kill -9 杀死

# 编辑AdGuardHome配置
vim /etc/AdGuardHome.yaml
# 找到`users`下面的password, 
# 按i键, 进入编辑模式

# 将值修改为$2a$10$Jmc0QFOhDrZTQMVToPy/6O9DWp3W6pR2faEsdUAriP.XwqrC3XWD6
# 注意password: 与密码之间有空格, 否则会认不出来

# 修改完成后按`Ctrl+c`键, 退出编辑模式
# 然后按`shift+:`键, 接着输入wq

# 启动
service AdGuardHome start
```

### 广告规则列表
- 英文: https://easylist-downloads.adblockplus.org/easylist.txt
- 中文: https://easylist-downloads.adblockplus.org/easylistchina.txt

# opkg 命令使用 https://openwrt.org/zh/docs/techref/opkg

# 问题
- 原版openwrt为啥没有服务选项
  - 因为没有安装插件的缘故
- 系统服务里面找不到`挂载点`功能
  - `opkg update`
  - `opkg install block-mount`
- 常用插件[下载地址](https://op.supes.top/packages/aarch64_generic/)
  - luci-app-filetransfer 文件传输插件
  - ikoolproxy: 去广告插件, 支持https(需要客户端安装证书), `koolproxy`的改版, 因为`koolproxy`已经不更新了
  - luci-app-adguardhome : 去广告插件 
  - luci-app-unblockmusic: 解锁网易云音乐
  - luci-app-vssr(又名helloword): vpn客户端, 支持ss, ssr, v2ray等
  - luci-app-aliyundrive-webdav 阿里云盘WebDAV, 挂载阿里云盘, 当做本地磁盘
  - luci-app-transmission BT下载
  - 上网时间控制
  - 应用过滤（OpenAppFilter）
  - Turbo ACC 网络加速
- Packages for ikoolproxy found, but incompatible with the architectures configured
  - 找到了 ikoolproxy 的包，但与配置的架构不兼容
  - 怎么查看系统架构`opkg print-architecture`,如下的架构是aarch64_generic
  ```out
  arch all 1
  arch noarch 1
  arch aarch64_generic 10

  ```
  

卖家提供的固件下载地址:
https://www.kancloud.cn/jackylee666666/r2s-r4s/3040857