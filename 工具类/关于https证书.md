# 一、关于https证书
- 说明: https证书是一种加密技术,用于保护网络通信安全,通过证书可以验证服务器的身份,防止中间人攻击
## 证书安装文件可能会有以下几个
- .crt: 证书文件
  - 也有可能是.txt格式
  - 如果证书颁发机构为二级机构,则可能会附加一个CA.crt证书, 这个证书用于验证机构是否合法, 因此会有多个.crt证书文件
  - 内容格式如下
	```
	-----BEGIN CERTIFICATE-----
	MIIFGjCCBAKgAwIBAgIQCgRw0Ja8ihLIkKbfgm7sSzAN
	....
	....
	-----END CERTIFICATE-----
	```
- .pem: (.crt)证书文件合并后的文件
  - 通常来说会分开提供, 有时也会只提供一个.pem
- .key: 私钥文件
	```
	-----BEGIN RSA PRIVATE KEY-----
	MIIEpAIBAAKCAQEAsYMJYZXELoIO41POcpiqkablZ2Gq
	...
	...
	-----END RSA PRIVATE KEY-----
	```
- .csr: 证书请求文件 (这个用于证书的申请, 一般由域名服务商提供, 与后续的安装无关)
## 注意事项
- 如果证书颁发机构为二级机构, 则在生成.pem文件是需要将所有的.crt文件合并成一个.pem文件, 否则可能会导致证书不被认可
- 多个证书合成方式, 可以使用cat命令, 意思是CA放前面, 服务器证书放后面
	- `cat CA.crt xxx.com.crt > ssl.pem`
- 证书安装后的验证域名为: 
  - [国内](https://myssl.com/)
  - [国外](https://www.ssllabs.com/ssltest/analyze.html)

### TLS与SSL 
- 两者的关系: TLS是SSL的升级版, 但是两者的功能是一样的, 但是TLS更加安全, 因此现在基本都是使用TLS

# 二、自制https证书
- 说明:这种证书是不被认可的,会被浏览器拦截,因此生产环境需要向域名申请商申请
## 自制证书步骤
### 本地生成key文件(需要设置key的密码,请记住密码后续需要使用)
`openssl genrsa -des3 -out ca.key 2048`
### 根据key文件生成pem文件(有效期设置了1024天)
`openssl req -x509 -new -nodes -key ca.key -sha256 -days 1024 -out ca.pem`
```
- 以下内容需要手动输入(除了密码,乱填都可以)
1. 输入key的密码(Enter pass phrase for ca.key):
2. 输入国家代码,2个字母(Country Name):CN
3. 输入省份全称(State or Province Name (full name)):Hunan
4. 输入城市全称(Locality Name (eg, city) [Default City]):Hengyang
5. 输入组织名称（如公司）[默认公司](Organization Name (eg, company) [Default Company Ltd]):LY.Co.Ltd
6. 组织单位名称（如部门）(Organizational Unit Name (eg, section)):Technology Department
7. 公用名（例如，您的姓名或服务器的主机名）(Common Name (eg, your name or your server's hostname)):LY
8. Email地址(Email Address):admin@leon.com
```
### 生成网站数字证书
- 创建 csr.cnf 文件 `vim csr.cnf`
```
[req]
default_bits = 2048
prompt = no
default_md = sha256
distinguished_name = dn

[dn]
C=CN
ST=Hunan
L=Hengyang
O=LY.Co.Ltd
OU=Technology Department
emailAddress=admin@leon.com
CN=leon.com
```
- 创建 csr.ext 文件 `vim csr.ext`
```
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = leon.com
DNS.2 = *.leon.com
DNS.3 = localhost
```
- 根据 csr.cnf 与 csr.ext 生成私钥(给nginx用),在万维网中申请的也是这两个东西
```shell
#生成key文件[leon.com.key]
openssl req -new -sha256 -nodes -out server.csr -newkey rsa:2048 -keyout leon.com.key -config csr.cnf
#生成crt文件[leon.com.crt]中途需要输入密码
openssl x509 -req -in server.csr -CA ca.pem -CAkey ca.key -CAcreateserial -out leon.com.crt -days 800 -sha256 -extfile csr.ext

# 将这两个文件copy到nginx目录下
mkdir /usr/local/webserver/nginx/ssl
mv leon.com.* /usr/local/webserver/nginx/ssl/
```
- 再次提醒：因为证书颁发机构不合法,所以需要将证书导入到本地浏览器中,否则会被拦截

# 三、nginx配置https证书
```nginx
http{
  ...
  server{
    listen 80;
    listen 443 ssl;
    # 域名, 多个以空格分开
    server_name leon.com;

		# ssl证书地址
		ssl_certificate     /usr/local/nginx/cert/ssl.pem;  # pem文件的路径
		ssl_certificate_key  /usr/local/nginx/cert/ssl.key; # key文件的路径
		
		# ssl验证相关配置
		ssl_session_timeout  5m;    #缓存有效期
		ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4;    #加密算法
		ssl_protocols TLSv1 TLSv1.1 TLSv1.2;    #安全链接可选的加密协议
		ssl_prefer_server_ciphers on;   #使用服务器端的首选算法
		...
	}
}
```
