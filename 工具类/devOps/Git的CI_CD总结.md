# Git的CI_CD总结
|深度|期望|达成|时间|
|---|---|---|---|
|了解|<input type="checkbox" checked/>|<input type="checkbox" checked/>|2022-03-01|
|demo|<input type="checkbox" checked/>|<input type="checkbox" checked/>|2022-03-01|
|总结|<input type="checkbox" checked/>|<input type="checkbox" checked/>|2022-03-01|

### 什么是CI/CD?
- CI: 持续集成(Continuous Integration)
- CD: 持续交付(Continuous Delivery)
- Pipeline: 管道, 也是进行CI/CD 的一系列流程
- [参考文档]
    - [什么是持续集成（CI）/持续部署（CD）？](https://zhuanlan.zhihu.com/p/42286143)
        - [别人的笔记](https://blog.csdn.net/OneDeveloper/article/details/105029118)
    - 我的笔记
        - 持续: “持续”用于描述遵循我在此提到的许多不同流程实践。这并不意味着“一直在运行”，而是“随时可运行”
        - CI/CD通过`任务task`或者`作业job`将源码转换为可发布产品, 这些`task`或者`job`通常是串联执行, 一个成功之后流向下一个, 因此也统称`管道`

### Git的CI/CD实现原理?
- [GitLab CI/CD](https://www.cnblogs.com/cjsblog/p/12256843.html)    
- 通过`gitlab的配置`或者通过`pipeline(task/job)触发`后在`gitlab runner`上执行预定的脚本
    - pipeline常用[命令](https://www.cnblogs.com/sanduzxcvbnm/p/13891757.html)
    
### Git的CI/CD怎么使用?
- 安装gitlab runner[Centos搭建Gitlab-runner环境](git/Centos搭建Gitlab-runner环境.md)
- 将gitlab runner注册到gitlab上[Centos搭建Gitlab-runner环境](git/Centos搭建Gitlab-runner环境.md)
- 在git项目的根目录下新增.gitlab-ci.yml文件(文件名称可在gitlab项目中配置:`Settings->CI/CD->General pipelines`),并且配置策略
- 注意:  
    - gitlab在提交时若不打tag,默认不会执行ci命令
      - 解决办法: 在gitlab的ci界面上, 编辑具体的runner, 让他可执行未打tag的提交
    - 可以配置在merger时自动触发ci[参考官方文档](http://192.168.12.102/help/ci/merge_request_pipelines/index.md#pipelines-for-merge-requests)
    
### `.gitlab-ci.yml`的编写语法
- hello word
```yaml 
job:
  script:
    - echo "hello cicd"
```
- pipeline表示(整个ci/cd过程)的生命周期,通常由一个或多个job组成
- job是ci/cd执行过程中的基本单位, 通过设置stage来指定job所属的pipeline阶段, 而且名称可以自定义
- stage用于定义job所属的阶段, 
    - 默认的stage阶段与他们的顺序如下: build -> test(若不在job中定义stage,默认会按照test来执行) ->deploy
    - 你可以通过stages来定义属于你的stage值与顺序
- stages用于配置自定义的stage以及stage的执行顺序, 通常需要放在stage使用前
- script 表示你执行`gitlab-runner register`选择的`executor`脚本
    - 注意这个脚本是以`gitlab-runner`用户身份执行的, 要测试请先用此用户进行脚本测试
```yaml
# 这里会先执行job_a再执行job_b
# 注意: job_a和job_a2可能会并发执行, runner不会保证他们的顺序
stages:
  - a
  - b
job_b:
  stage: b
  script:
    - echo "b"
job_a:
  stage: a
  script:
    - echo "a"
job_a2:
  stage: a2
  script:
    - echo "a2"
```


### 常见问题
- 刚新增完runner之后, 提示`New runner has not been connected yet`
    - 请先确保在git runner上所有命令都已经执行完毕
- 开源系统(https://www.jianshu.com/p/c505fdb38396)
- 执行脚本时报错:`Host key verification failed.`
    - 请检查`gitlab-runner`服务器上的`gitlab-runner`用户是否有权限执行该命令
- 怎么重置git-runner中的脚本?
    - 删除对应目录下的0目录,或者在gitlab上remove git-runner 然后重新注册
- 使用maven命令jar包无法正确更新下来
    - 为`gitlab-runner`用户设置maven的 repository 读写权限
    - 建议步骤
        1. 通过root账户备份原repository目录
        2. 通过root账户新建`repository`目录并授予775权限
        3. 通过`gitlab-runner`账户执行ci中的脚本, 更新`repository`中的jar,此时`maven`应该就可以正常使用了