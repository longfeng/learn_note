# 搭建Jenkins
|深度|期望|达成|时间|
|---|---|---|---|
|了解|<input type="checkbox" checked/>|<input type="checkbox" checked/>|2022-03-01|
|demo|<input type="checkbox" checked/>|<input type="checkbox" checked/>|2022-03-01|
|总结|<input type="checkbox" checked/>|<input type="checkbox" checked/>|2022-03-01|


## jenkins安装[官方文档](https://www.jenkins.io/zh/doc/book/installing/#unlocking-jenkins)
## 以war包方式运行jenkins
1. 下载 ``
```shell
cd /home

#
mkdir /usr/local/jenkins

#下载
wget http://mirrors.jenkins.io/war-stable/latest/jenkins.war

cp jenkins.war /usr/local/jenkins/

cd /usr/local/jenkins

# 默认就是8080端口, 日志 当前目录out.log
# nohup java -jar jenkins.war >out.log 2>&1 &
# 也可以手动指定端口
nohup java -jar jenkins.war --ajp13Port=-1 --httpPort=8080 >out.log 2>&1 &
echo "nohup java -jar jenkins.war --ajp13Port=-1 --httpPort=8083 >$logPath 2>&1 &"
nohup java -jar jenkins.war --ajp13Port=-1 --httpPort=8083 >$logPath 2>&1 &


# 开启防火墙
systemctl start firewalld
# 防火墙放行
# 配置中心端口
firewall-cmd --zone=public --add-port=8080/tcp --permanent
# 防火墙刷新
firewall-cmd --reload
# 检查端口是否正常开启, 结果是yes
firewall-cmd --permanent --query-port=8080/tcp

# 初始化密码(默认的用户名是admin)
cat /root/.jenkins/secrets/initialAdminPassword


```

## 配置jenkins环境与插件
### 初始化
- 登录: http://192.168.200.100:8080/
- 输入密码:
- 选择推荐安装(左1)
  - 安装包中包含了中文化语言包
- 创建用户
  - 建议直接填admin,然后密码自己填
  - 如果过这一步,则需要使用admin用户+初始密码登录
### 环境
#### 配置git/maven/jdk环境: Manage Jenkins -> Global Tool Configuration
- jdk
  - 新增jdk(取消自动安装jdk)
    - jdk
    - /usr/local/lib/jdk1.8.0_301
- maven
  - 新增maven(取消自动安装maven)
    - maven
    - /usr/local/maven
- git
  - name: git
  - Path to Git executable: git
#### 安装插件 Manage Jenkins -> Manage Plugins -> Available
- 选择好之后点击`Install without restart`安装
- Maven Integration
- Pipeline Maven Integration
- Publish Over SSH[可选的远程服务器ssh配置插件,如果多台机器且密码一样的情况下,可以用这个插件]

#### 配置远程服务器  Manage Jenkins -> Configure System
- 配置全局远程服务器(也可以在新建项目的时候再配置)
- 需要安装Publish Over SSH
- 下拉到Configure System最下面
  - Passphrase:服务器的密码
  - SSH Servers(可以有多个)
    - Name: 主机名,随便填
    - Hostname: ip地址
    - Username:
    - Remote Directory: 远程部署的目录
### 新建用户并授权
- 新建用户
  1. 系统管理
  2. 管理用户
  3. 新建用户
- 为用户授予系统基本权限
  1. 系统管理
  2. 全局安全配置
  3. 设置授权策略`[项目矩阵授权策略]`
  4. Add user
  5. 勾选
    - 全部
      - Read
    - 任务
      - Create
      - Cancel
      - Build
    - 视图
      - Read
      - Configure
-若出现如下警告, 请直接忽略:Builds in Jenkins run as the virtual Jenkins SYSTEM user with full Jenkins permissions by default. This can be a problem if some users have restricted or no access to some jobs, but can configure others. If that is the case, it is recommended to install a plugin implementing build authentication, and to override this default.
  - ✅ An implementation of access control for builds is present.
  - ❌ Access control for builds is possible, but not configured. Configure it in the global security configuration.


## 操作问题
- 查看jenkins版本
  - 菜单 -> Manage Jenkins -> About Jenkins
- Building on the controller node can be a security issue. You should set up distributed builds. See the documentation.
  - 在控制器节点上构建可能是一个安全问题。您应该设置分布式构建。请参阅文档。
- Java 11 is the recommended version to run Jenkins on; please consider upgrading.
  - Java 11 是运行jenkins的推荐版本;请考虑升级。
  - DISMISS 不再显示
- 如何重启jenkins
  - 访问: http://192.168.200.100:8080/restart
  - 然后在页面上点击[是]
- jenkins 保存时提示403错误`No valid crumb was included in the reques`
  - 系统管理->全局安全配置->跨站请求伪造保护->Crumb Issuer下[勾选]启用代理兼容,点击应用即可
  - [参考](https://www.jianshu.com/p/10b85bb75f66)
- jenkins中使用的shell脚本示例
  ```shell
  # copy文件到目标服务器
  scp mp-css-web/target/*.jar 192.168.12.211:/home/service/mp-css/
  # --- deploy start
  ## 执行目标服务器的远程命令
  ssh 192.168.12.211 <<EOF
  /home/service/mp-css/deploy_mp-css-web.sh restart
  EOF
  # --- deploy end
  ```

-- 安装语言包(如果初始化的时候选择的是自定义安装jenkins)
https://www.likecs.com/show-204140476.html#sc=1163
