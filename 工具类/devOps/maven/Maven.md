# maven备忘
## maven概念
### maven干啥的?
- 项目管理工具, 主要用来构建项目
--- 

### 坐标是啥?
- maven用来管理识别依赖(构件)的唯一标识,
> 详情请参考下文<a href="#pom_coordinate">`pom文件结构#坐标`</a>
---

### maven项目结构(约定优于配置)
```yml
src
    main  #放你主程序java代码和配置文件
        java       #你的程序包和包中的java文件
        resources  #你的java程序中要使用的配置文件
    test  #放测试程序代码和文件的（可以没有）
        java       #测试程序包和包中的java文件
        resources  #测试java程序中要使用的配置文件
pom.xml  #项目对象模型
```
- 资源是啥? 就是指maven项目中resource那个目录的东西, 通常我们用来存放一些配置文件, maven默认会将这些配置文件打包到classes目录中
--- 

### 配置以及优先级
- 优先级(高->低)
  1. param指定
  2. pom.xml
  3. -s 参数指定settings.xml
  4. {USER_HOME}/.m2/settings.xml
  5. {MAVEN_HOME}/conf/settings.xml
  6. {MAVEN_HOME}/lib/maven-model-builder-x.x.x.jar
- 类似于java的继承, 优先级从仅到远越来越低
- pom文件的继承关系也是类似
- maven-model-builder-x.x.x.jar里面的pom-4.0.0.xml是所有pom文件的父类, 比如这里定义了central,也能解释为什么我们定义central之后这里的central就会被覆盖.
- 可以通过`mvn help:effective-pom`与`mvn help:effective-settings`查看当前项目的所有配置,更多参考help插件的用法
--- 

### 依赖的自动性与冲突解决
- 依赖会自动传递, maven通过自动加载子项目中的依赖来实现依赖管理, 原则如下
  - 最短路径: `假设 a->b->x(1.0) a->c->d->x(2.0), 结果 a->x(1.0)`
  - 最先声明: `假设 a->b->x(1.0) a->c->x(2.0), 结果 a->x(1.0)`
- 如何`终止依赖传递`, 通过`<exclusions>`标签, 例如`屏蔽从oss项目中自动依赖的commons-lang`
```xml
<dependency>
	<groupId>com.aliyun.oss</groupId>	<artifactId>aliyun-sdk-oss</artifactId>
	<version>${aliyun.oss.version}</version>
	<exclusions>
		<exclusion>
			<artifactId>commons-lang</artifactId>
			<groupId>commons-lang</groupId>
		</exclusion>
	</exclusions>
</dependency>
```
> 详情请参考下文<a href="#pom_dependency">`pom文件结构#dependency标签`</a>
--- 

### 仓库与镜像
- 仓库只分为两类
  - 本地仓库: 本机的文件夹, 定义一个
  - 远程仓库: 远程的私服或者仓库, 可定义多个
> `本地仓库`请参考下文<a href="#settings_localRepository">`settings文件结构#localRepository标签`</a>
>
> `远程仓库`请参考下文<a href="#pom_repository">`settings文件结构#repository标签`</a>或<a href="#settings_profile">`settings文件结构#profile标签与activeProfiles标签`</a>
- 镜像
  - 作为远程仓库的补充, maven根据`mirrorOf`配置, 将发往仓库的所有请求转发给镜像
> `远程仓库`请参考下文<a href="#settings_mirror">`settings文件结构mirror标签`</a>
>
> 外部文章: https://www.sojson.com/blog/168.html
--- 

### 快照版(snapshot)与发布版(release)
- 快照版:
  - pom文件中定义的version后缀为SNAPSHOT结尾 `<version>1.0.0-SNAPSHOT</version>`
  - deploy时不覆盖私服中同版本jar，带时间戳，例如: `ecu-pk-api-1.0.0-20220215.013457-4.jar`
  - 加载依赖的时自动选择last版本
- 快照版:
  - pom文件中定义的version后缀为非~~SNAPSHOT~~结尾 `<version>1.0.0-beta</version>`
  - deploy时覆盖私服中同版本jar， 例如: `ecu-pk-api-1.0.0-beta.jar`
--- 

### 生命周期与阶段(phase)与插件(plugin)与目标(goal)
#### 生命周期与阶段(phase)与插件(plugin)与目标(goal)关系图![参考](./附件/maven生命周期与插件与目标.png)
- maven拥有三套生命周期,分别是`clean`,`build`,`site`,详情参考[官方说明:maven生命周期与插件与目标](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html#Built-in_Lifecycle_Bindings)
- 每套生命周期都由若干个`有序`的阶段来完成, maven为了方便使用, 为每个阶段都绑定了一个`指令`, 就是我们常用的`package`等
- 生命周期是maven对项目构建过程的抽象, 插件是maven对构建过程的实现
- 每个插件都可以有多个'实现', 作者称之为`目标(goal)`
- 通过将插件的`目标`绑定到生命周期的`阶段`, 加之阶段与`指令`的绑定, 这样我们就可以通过指令来使用插件了
  - 目标与阶段的绑定关系定义在插件中, 可以通过在pom文件中重新定义插件的`build`标签来绑定不同的阶段
#### build有哪些主要的阶段, 以及每个阶段都干了什么
- compile阶段
  - 作用: 编译项目的源代码(class文件)到target目录
  - 插件: 绑定`maven-compiler-plugin`插件的`compile`目标
- test阶段:
  - 作用: 编译test资源到target目录,执行单元测试(junit)
  - 插件: 绑定`maven-surefire-plugin`插件的`test`目标
  - 备注:
    - 跳过此阶段的方式:
      - 通过参数`-Dmaven.test.skip=true`(推荐), 注意还有一个参数是`-DskipTests`, 他们的区别是前者跳过test目录下的编译与测试, 后者会编译test目录,但是不执行测试
      - 重新定义`org.apache.maven.plugin`插件, 并配置skip参数为`true`, 路径:`build.plugins.plugin.configuration.skip`
- package阶段:
  - 作用: 将资源打包(比如jar/war),到target目录
  - 插件: 这个阶段很特殊, 是根据pom文件中的packaging值来动态绑定相应的插件的.
    - `<packaging>pom<packaging>`, 没绑定,所以啥也不干.
    - `<packaging>jar<packaging>`, 绑定了`maven-jar-plugin`插件的`jar`目标
    - `<packaging>war<packaging>`, 绑定了`maven-war-plugin`插件的`war`目标
- install阶段:
  - 作用: 将构建后的项目输出到本地仓库
  - 插件: 绑定`maven-install-plugin`插件的`install`目标
- deploy阶段:
  - 作用: 将构建后的项目输出到远程仓库
  - 插件: 绑定`maven-deploy-plugin`插件的`deploy`目标
#### site有哪些主要的阶段, 以及每个阶段都干了什么
- site阶段：
  - 作用: 生成[项目站点文档](./附件/maven_site_out.7z))
  - 插件: 绑定了`maven-site-plugin`插件的`site`目标
  - 啥?执行`mvn site`出错? 重新定义一下`maven-site-plugin`,将版本弄成最新的.
#### `mvn test`与`mvn surefire:test`的区别
- 当运行`mvn test`命令时，先运行 test 阶段之前的 compile、test-compile, 然后在执行maven-surefire-plugin的test目标
- 当运行`mvn surefire:test`命令时，直接执行maven-surefire-plugin的test目标
> 其他的周期与阶段请参考: https://www.jianshu.com/p/f6895f2dc887
--- 

## settings文件结构
### 参考[settings_demo](./附件/settings_demo.xml)

### localRepository标签<span id="settings_localRepository"/>
```xml
<settings>
	<!-- 配置本地仓库 -->
	<localRepository>D:\work\repository\maven-repository</localRepository>
</settings>
```
- localRepository 本地文件目录, linux 请使用/分隔
---

### offline标签<span id="settings_offline"/>
- settings.xml->settings.offline: 默认false, 建议不修改
- 离线模式,开启后不从仓库下载依赖
- [可能的使用场景](https://blog.csdn.net/lewky_liu/article/details/83904606)
---

### server标签<span id="settings_server"></span>
```xml
<servers>
  <server>
    <id>deploymentRepo</id>
    <username>repouser</username>
    <password>repopwd</password>
  </server>  
</servers>
```
- 用于配置deploy时仓库(私服)的鉴权信息,
- id: 仓库的id, 例如:
  - <a href="#settings_profile">settings中的repository标签</a>,
  - 以及pom文件中的repository/snapshotRepository/pluginRepository/标签中的ID
- username: 仓库用户名
- password: 仓库密码
---

### profile标签与activeProfiles标签<span id="settings_profile"></span>
- 注意:当`设置为激活`(参照Profile说明激活方式)或者`使用-P参数`时才会生效
```xml
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
  
  <profiles>
    <profile>
      <!-- profile_id -->
      <id>dev</id>
      <repositories>
        <repository>
          <!-- 参照pom文件的repository -->
        </repository>
      </repositories>
      <build>
        <resources>
          <!-- 不同环境设置不同的资源路径, springCloud之后基本上通过配置中心来设置环境参数 -->
        </resources>
      </build>
    </profile>
  </profiles>
  
  <!-- 与profile对应出现的通常还有activeProfiles -->
  <!-- activeProfiles用于告诉maven使用哪个profile来执行编译, 可多选 -->
  <activeProfiles>
    <activeProfile>dev</activeProfile>
    <activeProfile>profile_id2</activeProfile>
  </activeProfiles>
</settings>
```
--- 

### mirror标签-镜像<span id="settings_mirror"/>
```xml
<settings>
  <mirrors>
      <mirror>
        <id>alimaven</id>
        <name>aliyun maven</name>
        <url>https://maven.aliyun.com/repository/public/</url>
        <mirrorOf>central</mirrorOf>        
      </mirror>
  </mirrors>
</settings>
```
- mirrorOf
  - 通常是仓库的id
  - 可使用英文`,`分割,表示并且的关系
    - 例如: 匹配多个仓库`<mirrorOf>central,myRepository</mirrorOf>`
    - 例如: 匹配除了center以外的所有仓库`<mirrorOf>!central,*</mirrorOf>`
  - 相同的mirrorOf后面的会覆盖前面的配置
  - 当mirrorOf为*时表示此mirror优先级最高(此时其他的mirror将不起作用)

## pom文件结构
### 参考demo
- [demo](./附件/pom_demo.xml)
- [配置xsd中文描述](./附件/pom_xsd_cn_desc.xml)

### 坐标<span id="pom_coordinate"/>
```xml
<project>
    <groupId>com.leon.learn</groupId>
    <artifactId>maven_demo</artifactId>
    <version>1.0-SNAPSHOT</version>
    <!-- 默认jar -->
    <packaging>pom</packaging>
</project>
```
- groupId[必填]
- artifactId[必填]
- version[必填]
- packaging[选填,默认值:jar]
- classifier[依赖时选填, 用于区分附属构件]
  - https://www.cnblogs.com/lnlvinso/p/10111328.html
  - https://www.cnblogs.com/summary-2017/p/10927009.html
--- 

### dependency标签<span id="pom_dependency"/>
```xml
<project>
  <!-- 项目所有依赖集合 -->
  <dependencies>
    <!-- 项目的某个依赖 -->
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
      <version>3.3.2</version>
    </dependency>
  </dependencies>
</project>
```
- `groupId`,`artifactId`,`version`: 坐标, 必填
- `type`: 依赖的类型 对应坐标的 `packaging`, 选填, 默认jar
- `scope`: [依赖的范围](https://blog.csdn.net/kimylrong/article/details/50353161), 选填
  - `compile`, 默认(全部范围)
  - `test`, 如果依赖只希望在test时生效,使用这个标记
  - `optional`: <font color=yellow >阻止传递依赖(可以在当前项目中使用,父项目中需要显式声明)</font>, 选填, `默认值为false不阻止`
    - 假设: a->b->x
      - 如果在`b依赖x的配置中`设定`<optional>true</optional>`, 则a不依赖x
    - 参考(https://www.cnblogs.com/jakaBlog/p/11547401.html)
  - `exclusions`: <font color=yellow >阻止传递依赖</font>列表, 选填
    - 假设: a->b->x
      - 如果在`a依赖b的配置中`设定(如下所示), 则a不依赖x
      ```xml
      <exclusions>
        <exclusion> 
            <groupId>com.leon.learn</groupId>
            <artifactId>x</artifactId>
        </exclusion>
      </exclusions>
      ```
--- 

### repository标签<span id="pom_repository"/>
```xml
<project>
  <repositories>
    <repository>
      <id>codehausSnapshots</id>
      <name>Codehaus Snapshots</name>
      <url>https://repository-master.mulesoft.org/nexus/content/groups/public/</url>
      <layout>default</layout>
      <releases>
        <enabled>false</enabled>
        <updatePolicy>always</updatePolicy>
        <checksumPolicy>warn</checksumPolicy>
      </releases>
      <snapshots>
        <enabled>true</enabled>
        <updatePolicy>never</updatePolicy>
        <checksumPolicy>fail</checksumPolicy>
      </snapshots>
    </repository>
  </repositories>
  <!-- maven3.6.3默认的插件仓库配置 -->
  <pluginRepositories>
    <pluginRepository>
      <id>central</id>
      <name>Central Repository</name>
      <url>https://repo.maven.apache.org/maven2</url>
      <layout>default</layout>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
      <releases>
        <updatePolicy>never</updatePolicy>
      </releases>
    </pluginRepository>
  </pluginRepositories>
</project>
```
- id，库的ID, 任何一个仓库声明的id必须是唯一的, Maven自带的中央仓库使用的id为central，如果其他的仓库声明也使用该id，就会覆盖中央仓库的配置。
- name，库的名称
- url，库的URL
- layout，在Maven 2/3中都是default，只有在Maven 1.x中才是legacy
- releases，库中版本为releases的构件
- snapshots，库中版本为snapshots的构件
  - enabled，是否支持更新, 默认为true
    - 设置为true时表示表示更新当前版本的构件, 比如在snapshots中配置的话就表示接收snapshots构件
  - updatePolicy，构件更新的策略，默认为daily
    - updatePolicy用来配置Maven从远程仓库检查更新的频率
    - 值为daily时, 每天检查一次
    - 值为always时, 每次构建都检查更新
    - 值为never时, 从不检查更新
    - interval:X(其中的X是一个数字，表示间隔的时间，单位min)
  - checksumPolicy，校验码异常的策略，默认为warn
    - checksumPolicy用来配置校验码异常的处理策略. 当构件被部署到Maven仓库中时，会同时部署对应的校验和文件。在下载构件的时候，Maven会验证校验和文件
    - 值为warn时，Maven会在执行构建时输出警告信息。
    - 值为fail时，Maven遇到校验和错误就让构建失败。
    - 值为ignore时，使Maven完全忽略校验和错误。
- 参考:maven实战,6.4章
- 参考:https://blog.csdn.net/taiyangdao/article/details/52287856
--- 

### build标签<span id="pom_build"/>
```xml
<build>
    <resources>
        <resource>
			<!-- 资源目录 -->
            <directory>src/main/resources</directory>
            <!-- 默认为true,将所有文件打包到classpath下, 配置为false,则只打包includes定义的文件 -->
            <filtering>false</filtering>
            <includes>
                <include>context.xml</include>
            </includes>
        </resource>

        <resource>
            <directory>src/main/resources</directory>
            <filtering>true</filtering>
            <excludes>
                <exclude>context.xml</exclude>
            </excludes>
        </resource>
    </resources>
</build>
```
- [resources.resource标签, 配置资源方式一](https://my.oschina.net/anxiaole/blog/1613348)
- 没有定义resources标签时,默认会将src/main/resource打包到classes目录中,但是不会替换properties定义的值
- 通常resource会与profile结合,用于打包不同环境的配置文件
- 默认存在同名资源时不覆盖, 若需要覆盖, 请配置`<properties><maven.resources.overwrite>true</maven.resources.overwrite></properties>`
- 多个build相冲突,就近生效原则。
  - 子pom与父pom同时定义相同的build.resources时,以子pom的为准
  - 我曾看见过有人在子pom中直接配置了build.resources的路径,然后在父pom中通过profile.build.resources定义了差异, 实际父pom的配置并不生效.
- includes与excludes冲突时,以excludes为准
- filtering为true的时候，这时只会把过滤的文件打到classpath下，filtering为false的时候，会把不需要过滤的文件打到classpath下
---

### dependencyManagement标签<span id="pom_dependencyManagement"/>
```xml
<dependencyManagement>
  <dependencies>
    <dependency>
      <groupId>org.mybatis</groupId>
      <artifactId>mybatis</artifactId>
      <version>${mybatis-version}</version>
    </dependency>
  </dependencies>
</dependencyManagement>
```
- dependencyManagement用于在parent pom.xml文件中声明当前项目所用到的依赖
- 此声明并不会产生实际依赖, 主要为了方便版本管理, 比如在子项目 pom.xml文件中定义依赖时, 可以不指定版本, 从而达成整个项目的版本依赖规范
- [看别人怎么描述](https://blog.csdn.net/weixin_42114097/article/details/81391024)
---

## maven插件
### maven-help-plugin: maven帮助插件
- 配置方式:系统自带
- 插件目标(goal)用法示例:(只列几个常用的,用`mvn help:help`查看)
  - `mvn help:active-profiles`: 显示激活的环境(profile)
  - `mvn help:effective-pom`: 显示项目使用的pom配置(在控制台生成一个xml,将用到的配置都在上面, 包括默认的)
  - `mvn help:effective-settings`: 显示项目使用的settings配置(在控制台生成一个xml,将用到的配置都在上面, 包括默认的)
  - `mvn help:describe`: 用来解释命令或者插件等, 报错了? 哈哈,自己看error,demo都在里面了, 比如查看插件描述`mvn help:describe -Dplugin=org.apache.maven.plugins:maven-help-plugin`
--- 

### maven-compiler-plugin: compile阶段的默认绑定插件(建议显示定义source/target/encoding)
```xml
<plugin>                      
  <groupId>org.apache.maven.plugins</groupId>
  <artifactId>maven-compiler-plugin</artifactId>
  <version>3.1</version>
  <configuration>
      <!-- 源代码使用的JDK版本 -->
      <source>1.8</source>
      <!-- 需要生成的目标class文件的编译版本 -->
      <target>1.8</target>
      <!-- 字符集编码 -->
      <encoding>UTF-8</encoding>
  </configuration>
</plugin>
```
- 建议手动定义
  - 原因:需要改一下jdk版本(maven3默认jdk1.5),
  - `source`与`target`可在`properties`标签中配置. [不推荐,原因:可能设置不成功](https://www.cnblogs.com/zhjh256/p/9263009.html)
    ```xml
    <properties>
      <maven.compiler.source>1.8</maven.compiler.source>
      <maven.compiler.target>1.8</maven.compiler.target>
    </properties>
    ```
  - `encoding`可在`properties`标签中配置`project.build.sourceEncoding`
- 插件目标(goal)用法示例:
  - 被动执行:`mvn compile` 因默认绑定到了compile阶段
  - 主动执行:`mvn compiler:compile`
--- 

### maven-jar-plugin: package阶段,jar项目类型的默认绑定插件[配置参考](https://www.jianshu.com/p/d44f713b1ec9)
```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-jar-plugin</artifactId>
    <version>3.2.0</version>
    <configuration>
        <archive>
            <manifest>
                <!-- 设置lib路径(这个路径相对于jar的执行位置而言),并且将lib路径加入classpath -->
                <classpathPrefix>lib/</classpathPrefix>
                <addClasspath>true</addClasspath>
                <!-- 设置main方法所在的class -->
                <mainClass>com.leon.learn.maven.Main</mainClass>
            </manifest>
        </archive>
    </configuration>
</plugin>
```
- 这个插件不会将依赖的jar打入目标jar包内, 因此不适合打可执行jar(可执行jar参考maven-shade-plugin)
- 插件目标(goal)用法示例:
  - 被动执行:`mvn package`, 因默认绑定到了package阶段
  - 主动执行:`mvn jar:jar`
--- 

### maven-shade-plugin: 适合构建可执行jar包, 将依赖的jar解压并与当前项目一起打包生成新的jar(不适合太复杂项目)
```xml
<plugin>
   <groupId>org.apache.maven.plugins</groupId>
   <artifactId>maven-shade-plugin</artifactId>
   <version>3.2.4</version>
   <executions>
       <execution>
           <phase>package</phase>
           <goals>
              <goal>shade</goal>
           </goals>
       </execution>
   </executions>
   <configuration>
       <transformers>
           <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
               <mainClass>com.leon.learn.maven.Main</mainClass>
           </transformer>
       </transformers>
   </configuration>
</plugin>
```
- 还有一个类似的插件[maven-assembly-plugin](https://my.oschina.net/u/2377110/blog/1584205),也可以用来打可运行jar包, 但是更适合做jar包的聚合
- [配置参考1](https://my.oschina.net/u/2377110/blog/1585553)
- [配置参考2](https://cloud.tencent.com/developer/article/1622207)
--- 

### maven-war-plugin: package阶段,war项目类型的默认绑定插件
```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-war-plugin</artifactId>
    <version>3.3.1</version>
    <configuration>
        <warSourceDirectory>${basedir}/src/main/resources</warSourceDirectory>
        <webResources>
            <resouce>
                <directory>${basedir}/src/main/resources</directory>
                <targetPath>WEB-INF/classes</targetPath>
            </resouce>
        </webResources>
    </configuration>
</plugin>
```
- 插件目标(goal)用法示例:
  - 被动执行:`mvn package`, 因默认绑定到了package阶段
  - 主动执行:`mvn war:war`
--- 

### maven-resources-plugin: 用来打包资源
- 配置方式:
```xml


```
- 插件目标(goal) `mvn help:describe -Dplugin=org.apache.maven.plugins:maven-resources-plugin`
--- 

### maven-source-plugin: 将源码以jar的形式构建到target目录中
- 配置方式
```xml
<plugin>  
    <groupId>org.apache.maven.plugins</groupId>  
      <artifactId>maven-source-plugin</artifactId>  
      <version>3.0.0</version>  
      <configuration>  
          <attach>true</attach>  
      </configuration>  
      <executions>  
          <execution>  
              <phase>package</phase>  
              <goals>  
                  <goal>jar</goal>  
              </goals>  
          </execution>  
      </executions>  
  </plugin>
```
- 插件目标(goal)用法示例:
  - 被动执行:`mvn package`, 因为上述配置绑定到了package阶段
  - 主动执行:`mvn source:jar`
  - 注:只要在target目录中有了package, install时会自动将jar包复制到到本地仓库,deploy同理
--- 

### maven-dependency-plugin: 依赖管理插件
```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-dependency-plugin</artifactId>
    <version>3.2.0</version>
    <executions>
        <execution>
            <id>copy</id>
            <phase>compile</phase>
            <goals>
                <goal>copy-dependencies</goal>
            </goals>
            <configuration>
                <!-- 被copy依赖的目标路径 -->  
                <outputDirectory>
                    ${project.build.directory}/lib
                </outputDirectory>
            </configuration>
        </execution>
    </executions>
</plugin>
```
- 插件目标(goal):
  - copy-dependencies: 复制依赖到指定(configuration.outputDirectory)目录
--- 

### maven-checkstyle-plugin: 检查代码质量,生成项目质量报告
- 配置方式:
```xml
<!-- 通过mvn checkstyle:checkstyle使用 -->
<plugins>
    <plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-checkstyle-plugin</artifactId>
    <version>3.1.2</version>
    <configuration>
      <logViolationsToConsole>true</logViolationsToConsole>
      <checkstyleRules>
        <module name="Checker">
          <!-- Checks for Size Violations.                    -->
            <!-- See http://checkstyle.sf.net/config_sizes.html -->
            <module name="FileLength">
            <property name="max" value="3500" />
            <property name="fileExtensions" value="java"/>
          </module>
            <!-- Checks for whitespace                               -->
            <!-- See http://checkstyle.sf.net/config_whitespace.html -->
            <module name="FileTabCharacter"/>
          <module name="TreeWalker">
            <module name="StaticVariableName"/>
            <module name="TypeName">
              <property name="format" value="^_?[A-Z][a-zA-Z0-9]*$"/>
            </module>
          </module>
        </module>
      </checkstyleRules>
    </configuration>
  </plugin>
</plugins>
```
- 插件目标(goal) `mvn help:describe -Dplugin=org.apache.maven.plugins:maven-checkstyle-plugin`
  - check 检查代码质量,且输出到target中`checkstyle-result.xml`
  - checkstyle 生成[项目质量报告](./附件/maven_checkstyle_out.7z)(太丑了)
--- 

### tomcat7-maven-plugin: 将tomcat内嵌到web项目中，直接运行webapp项目。[用法](https://www.cnblogs.com/afeng2010/p/10224597.html)
>- 官方文档[可用插件列表](http://maven.apache.org/plugins/index.html#)
--- 

## maven问题记录
### idea 自定义项目脚手架`archetype`
- https://www.jianshu.com/p/0ee76c00298d
--- 

### 如何生成可执行jar
- 方式1: 推荐(简单), 通过maven-shade-plugin,构建简单的可执行jar包,参考前文对maven-shade-plugin的介绍
- 方式2: 通过maven-jar-plugin与maven-dependency-plugin配合,形成可以允许的jar与lib,再生成exe文件(思路,未验证)
- 方式3: 通过[maven-assembly-plugin](https://my.oschina.net/u/2377110/blog/1584205) 聚合的方式生成jar包与第一种方式类似
---

### 编译报错 the trustAnchors parameter must be non-empty?
- java证书过期(https://zhuanlan.zhihu.com/p/103177882)
- 下载一个同版本(小版本无所谓)的jdk,然后将证书替换
- 或者重装一个jdk
--- 

### maven的离线模式有啥用?
- settings.xml->settings.offline: 默认false,
- 离线模式,开启后不从仓库下载依赖
- [可能的使用场景](https://blog.csdn.net/lewky_liu/article/details/83904606)
--- 

### maven怎么为仓库配置代理
- 某些服务器没有外网访问环境,此时需要为通过代理访问仓库
- [场景](https://www.cnblogs.com/0xcafedaddy/p/9829879.html)
--- 

### 手动deploy到maven私服
- jar和pom一起(建议上传pom),如果不传的话, 私服会自动生成pom文件, 而pom文件是描述依赖文件的核心, 这时可能会导致部分依赖(丢失描述)不加载
```cmd
mvn deploy:deploy-file -DgroupId=com.leon.mcu -DartifactId=ecu-pk-api -Dversion=1.0.0-SNAPSHOT -Dpackaging=jar -Dfile=E:\repository-xlm\com\leon\mcu\ecu-pk-api\1.0.0-SNAPSHOT\ecu-pk-api-1.0.0-SNAPSHOT.jar -DpomFile=E:\repository-xlm\com\leon\mcu\ecu-pk-api\1.0.0-SNAPSHOT\ecu-pk-api-1.0.0-SNAPSHOT.pom -Durl=http://username:password@127.0.0.1:8085/repository/maven-snapshots/
``` 
- 这里有个工具可以[批量生成上序mvn命令](./附件/MavenDeployGenerator.java)

### mvnd与maven的区别
- 据说是很大改善, 但是我经过几个项目实测, 在mvn命令后面加上多线程命令之后, 编译速度和mvnd差不多
- 多线程命令时 `mvn -T 1C` 表示使用多线程, 并且每1个线程处理一个任务, 
  - 使用多线程后, 不会导致依赖遗漏的问题, maven仍然会根据依赖的顺序来执行编译任务

### 版本号建议 主版本号.次版本号.补丁版本号， 以下这三种情况需要增加相应的版本号：
- 主版本号： 当API发生改变，并与之前的版本不兼容的时候
- 次版本号： 当增加了功能，但是向后兼容的时候
- 补丁版本号：当做了向后兼容的缺陷修复的时候

### maven如何通过profile来为不同环境定制版本号
- https://www.cnblogs.com/lukelook/p/11298168.html
  - 子模块的parent版本: <version>${parent.version}</version>
  - 子模块的依赖版本:    <version>${project.version}</version>
  - 父模块的版本:       <version>${parent.version}</version> 
  - 父模块的配置:       <properties><parent-version>1.0.0</parent-version></properties>
  - 最后引入插件:       org.codehaus.mojo - versions-maven-plugin
  

## end