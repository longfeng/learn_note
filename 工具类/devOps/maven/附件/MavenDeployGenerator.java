package com.leon.util.maven;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.XmlUtil;
import lombok.Data;
import org.w3c.dom.Document;

import javax.xml.xpath.XPathConstants;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 将某个目录下的jar生成deploy命令,并输出在控制台
 * <pre>
 *  - 递归目录, 解析pom文件
 *  - 每个项目的一个版本只生成一个命令
 * </pre>
 * @author leon
 */
public class MavenDeployGenerator {

    public static void main(String[] args) {
        String jarsPath = "E:\\repository-xlm-new\\com\\leon\\mcu\\ecu-pk-impl";
        MavenDeployGenerator.newInstance(MavenRepositoryType.snapshots)
                .generatorDeployCmdList(jarsPath)
                .forEach(System.out::println);
        System.out.println("\r\n");
    }

    /** 生成Deploy命令 */
    public List<String> generatorDeployCmdList(String jarsPath){
        List<Coordinate> allCoordinates = getAllCoordinates(jarsPath);
        return buildCmdForCoordinates(allCoordinates);
    }

    /**
     * 构造实例
     */
    public static MavenDeployGenerator newInstance(MavenRepositoryType type) {
        switch (type){
            case releases:
                if (releaseGenerator==null){
                    synchronized (MavenDeployGenerator.class){
                        if (releaseGenerator==null){
                            releaseGenerator = new MavenDeployGenerator();
                            releaseGenerator.repositoryType = type;
                        }
                    }
                }
                return releaseGenerator;
            case snapshots:
                if (snapshotGenerator==null){
                    synchronized (MavenDeployGenerator.class){
                        if (snapshotGenerator==null){
                            snapshotGenerator = new MavenDeployGenerator();
                            snapshotGenerator.repositoryType = type;
                        }
                    }
                }
                return snapshotGenerator;
            default:
                throw new IllegalArgumentException("MavenRepositoryType not support!");
        }
    }

    private static MavenDeployGenerator releaseGenerator;
    private static MavenDeployGenerator snapshotGenerator;

    /** 打包方式 */
    private MavenRepositoryType repositoryType;


    @Data
    public static class Coordinate{
        private String groupId;
        private String artifactId;
        private String version;
        // E:\repository\eu\bitwalker\UserAgentUtils\1.20\UserAgentUtils-1.20.pom
        private String pomFilePath;
        // E:\repository\eu\bitwalker\UserAgentUtils\1.20\UserAgentUtils-1.20.jar
        private String jarFilePath;
        private Packaging packaging;
        private MavenRepositoryType mavenRepositoryType;
    }

    public enum Packaging{
        jar,pom
    }

    /** maven 仓库类型 */
    public enum MavenRepositoryType {
        snapshots("http://admin:admin@192.168.12.119:8085/repository/maven-snapshots/"),
        releases("http://admin:admin@192.168.12.119:8085/repository/maven-releases/");
        private final String url;
        MavenRepositoryType(String url) {
            this.url = url;
        }

        public static String getUrlByModelIsSnapshot(Boolean modelIsSnapshot) {
            return modelIsSnapshot ? snapshots.url : releases.url;
        }
    }

    /** 获取所有jar的坐标 */
    public List<Coordinate> getAllCoordinates(String path){
        File rootDir = new File(path);
        if (!FileUtil.exist(rootDir) || FileUtil.isFile(rootDir)){
            throw new RuntimeException("文件不存在:"+path);
        }

        List<File> pomFiles = FileUtil.loopFiles(path, file -> {
            String parentParentName = file.getParentFile().getParentFile().getName();
            String parentName = file.getParentFile().getName();
            String fileName = parentParentName+"-"+parentName+"."+ Packaging.pom;
            return file.getName().equals(fileName);
        });

        List<Coordinate> result = new ArrayList<>();
        pomFiles.forEach(file-> result.add(buildCoordinateByPom(file)));

        return result;
    }

    private Coordinate buildCoordinateByPom(File file) {
        Coordinate coordinate = new Coordinate();

        Document document = XmlUtil.readXML(file);
        coordinate.setMavenRepositoryType(repositoryType);
        coordinate.setGroupId(getValueByXmlPath(document, "//project/groupId", "//project/parent/groupId"));
        coordinate.setArtifactId(getValueByXmlPath(document, "//project/artifactId", "//project/parent/artifactId"));
        coordinate.setVersion(buildVersion(getValueByXmlPath(document, "//project/version", "//project/parent/version"), coordinate.getMavenRepositoryType()));
        String packaginStr = getValueByXmlPath(document, "//project/packaging", "//project/packaging", "jar");
        Packaging packaging;
        try {
            packaging = Packaging.valueOf(packaginStr);
        }catch (Exception e){
            System.err.println("rem "+file.getName()+" packaging:"+packaginStr);
            packaging = Packaging.jar;
        }
        coordinate.setPackaging(packaging);
        coordinate.setPomFilePath(file.getPath());
        if (Packaging.jar.equals(coordinate.getPackaging())){
            coordinate.setJarFilePath(file.getParent()+File.separator+FileUtil.mainName(file)+"."+packaging.name());
        }

        return coordinate;
    }

    private String buildVersion(String version, MavenRepositoryType repositoryType) {
        return MavenRepositoryType.releases.equals(repositoryType) ? version.replaceAll("-SNAPSHOT", "") : version;
    }

    private String buildFileName(String name, Boolean modelIsSnapshot) {
        if (modelIsSnapshot){
            return name.replaceAll("-SNAPSHOT", "");
        }else{
            return name;
        }

    }

    private String getValueByXmlPath(Document document, String xmlPath, String xmlParentPath){
        return getValueByXmlPath(document, xmlPath, xmlParentPath, "");
    }

    /**
     * 先找节点路径上的值, 找不到取父节点路径的值, 再找不到, 用默认值
     * <pre>
     *     hutool 版本不能用5.x的,否则XmlUtil.getByXPath拿不到数据, 确定可行的是4.5.15
     * </pre>
     * @param xmlPath        xml节点路径    例如: //project/groupId
     * @param xmlParentPath  xml父节点路径  例如: //project/parent/groupId
     */
    private String getValueByXmlPath(Document document, String xmlPath, String xmlParentPath, String defaultValue) {
        String value = XmlUtil.getByXPath(xmlPath, document, XPathConstants.STRING).toString();
        if (StrUtil.isNotBlank(value)){
            return value;
        }else{
            String parentValue = XmlUtil.getByXPath(xmlParentPath, document, XPathConstants.STRING).toString();
            if (StrUtil.isNotBlank(parentValue)){
                return parentValue;
            }else{
                return defaultValue;
            }
        }
    }

    /** 根据坐标生成deploy命令 */
    public List<String> buildCmdForCoordinates(List<Coordinate> coordinates){
        if (CollUtil.isEmpty(coordinates)){
            System.err.println("文件列表为空, 跳过!");
            return new ArrayList<>();
        }

        List<String> result = new ArrayList<>(coordinates.size());
        coordinates.forEach(coordinate -> {
            String cmd;
            if (Packaging.pom.equals(coordinate.getPackaging())){
                cmd = "mvn deploy:deploy-file -DgroupId=" + coordinate.getGroupId()
                        + " -DartifactId=" + coordinate.getArtifactId()
                        + " -Dversion=" + coordinate.getVersion()
                        + " -Dpackaging=" + coordinate.getPackaging()
                        + " -Dfile=" + coordinate.getPomFilePath()
                        + " -Durl=" + coordinate.mavenRepositoryType.url;
            } else {
                cmd = "mvn deploy:deploy-file -DgroupId=" + coordinate.getGroupId()
                        + " -DartifactId=" + coordinate.getArtifactId()
                        + " -Dversion=" + coordinate.getVersion()
                        + " -Dpackaging=" + coordinate.getPackaging()
                        + " -Dfile=" + coordinate.getJarFilePath()
                        + " -DpomFile=" + coordinate.getPomFilePath()
                        + " -Durl=" + coordinate.mavenRepositoryType.url;
            }
            result.add(cmd);
        });
        return result;
    }
}
