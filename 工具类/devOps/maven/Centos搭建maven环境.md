### 安装maven脚本
```shell
# 
cd /usr/local/src
# 下载
wget https://dlcdn.apache.org/maven/maven-3/3.8.6/binaries/apache-maven-3.8.6-bin.tar.gz

# 解压到/usr/local/
tar -xf apache-maven-3.8.6-bin.tar.gz -C /usr/local/
# 创建快捷方式
ln -sv /usr/local/apache-maven-3.8.6 /usr/local/maven
#配置环境变量
vi /etc/profile
#在最后的export配置后面追加以下配置-start
export MAVEN_HOME=/usr/local/maven
export PATH=$MAVEN_HOME/bin:$PATH
#在最后的export配置后面追加以下配置-end

#更新环境变量配置
source /etc/profile

#验证
mvn -version
```

### 配置maven
```shell
# 备份配置文件
cp /usr/local/maven/conf/settings.xml /usr/local/maven/conf/settings.xml.first.bk
#
mkdir -p /home/data/maven/repository
#
vi /usr/local/maven/conf/settings.xml
```
#### 修改以下内容
```xml
     <localRepository>/home/data/maven/repository</localRepository>
     <!-- 将原有的mirror删除, 替换成阿里云的mirror -->
	 <mirror>
      <id>alimaven</id>
      <name>aliyun maven</name>
      <url>https://maven.aliyun.com/repository/central/</url>
      <mirrorOf>central</mirrorOf>        
    </mirror>
```


### 安装mvnd
#### windows [参考](https://www.cnblogs.com/cao-lei/p/15736255.html)
- 解压
- 将mvnd根目录下的bin配置到系统环境变量
- 将本机的maven settings文件copy并且覆盖mvnd根目录下的mvn/conf/settings文件
- 使用mvnd命令执行maven操作

#### linux安装mvnd
```shell
cd /usr/local/src
wget https://github.com/apache/maven-mvnd/releases/download/0.7.1/mvnd-0.7.1-linux-amd64.zip
unzip mvnd-0.7.1-linux-amd64.zip
mv mvnd-0.7.1-linux-amd64.zip /usr/local/

# 配置环境变量
vi /etc/profile
#在最后的export配置后面追加以下配置-start
export MVND_HOME=/usr/local/mvnd-0.7.1-linux-amd64
export PATH=$MVND_HOME/bin:$PATH
```
