# centos 7.9 安装yapi 1.9.2
## 安装依赖
### 安装 gcc++ (7.x)
```shell
# g++ 版本
yum install -y centos-release-scl
yum install -y devtoolset-7-gcc devtoolset-7-gcc-c++
scl enable devtoolset-7 bash
g++ --version
```
### 安装 node: v12.18.3 与 npm: 6.14.6
```shell
# 安装 EPEL 仓库
yum install -y epel-release

# 使用 NVM (Node Version Manager)
# 安装 curl 和 bash
yum install curl bash

# 安装 NVM(注意: 如果githubusercontent.com失败的话, 就安装一个git然后通过gitee来实现)
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
## 安装git与nvm
yum install git -y
git clone https://gitee.com/longfeng/nvm.git ~/.nvm && cd ~/.nvm && git checkout `git describe --abbrev=0 --tags`

# 关闭并重新打开终端，或者运行下面的命令来激活 NVM
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

# 安装 nodejs(可能比较耗时)
nvm install 12.18.3
# 检查版本
node -v

# 升级 npm 到指定版本
npm install -g npm@6.14.6

# 配置npm registry
npm config set registry https://registry.npmmirror.com

# 安装 pm2 用于后期的服务管理
npm install -g pm2
```
### 安装mongoDB(docker-compose)
- [安装docker](docker/安装docker.md)
- [安装docker-compose](docker/安装docker.md)
- 安装mongodb
```shell
# 创建目录
mkdir /data/docker-yml
# 编辑文件, 从下面的内容中复制
vi /data/docker-yml/mongodb-docker-compose.yml
# 启动(如果更新的话, 需要删除容器, 然后重新创建, 因为数据已经映射到外面, 所以不会丢失)
docker-compose -f /data/docker-yml/mongodb-docker-compose.yml up -d
# 验证
# docker ps 找到mongodb的容器id
docker exec -it <容器ID> sh
# 容器里面验证mongodb是否可用
mongo mongodb://mongo:mongo@127.0.0.1:27017/
```
- 配置文件如下
```yaml
version: '3.5'
services:
  mongo:
    container_name: mongo-yapi
    restart: always
    image: ccr.ccs.tencentyun.com/longfeng.site/mongo:4.4.26
    ports:
      - 127.0.0.1:27017:27017
    environment:
      # 默认启动mongo后在admin中添加mongo用户，拥有最高权限
      MONGO_INITDB_ROOT_USERNAME: mongo
      MONGO_INITDB_ROOT_PASSWORD: mongo
    volumes:
      - /data/yapi/mongodb:/data/db
      - /etc/localtime:/etc/localtime:ro
```
---
## 安装yapi 1.9.2
- [参考地址](https://blog.csdn.net/shan165310175/article/details/118494730)
- 下载部署包，修改配置
- 准备配置文件(config_example.json)
  - 扩展字段 "closeRegister":true, 用于关闭注册服务
```json
{
  "port": "3000",
  "adminAccount": "admin@admin.com",
  "timeout":120000,
  "db": {
    "servername": "127.0.0.1",
    "DATABASE": "yapi",
    "port": 27017,
	"user": "mongo",
    "pass": "mongo",
    "authSource": "admin"
  },
  "mail": {
    "enable": true,
    "host": "admin@admin.com",
    "port": 465,
    "from": "***@admin.com",
    "auth": {
      "user": "***@admin.com",
      "pass": "*****"
    }
  }
}
```
- 安装
```shell
cd /data/yapi
# 下载部署包
wget http://registry.npm.taobao.org/yapi-vendor/download/yapi-vendor-1.9.2.tgz
# 解压
tar -zxvf yapi-vendor-1.9.2.tgz
# 重命名
mv package vendors
# 进入目录
cd /data/yapi/vendors
 
# 拷贝文件到上级目录
#cp config_example.json /data/yapi/config.json
 
# 编辑文件, 修改内容并保存, 请参考下面的yapi配置
vi /data/yapi/config.json

# 安装依赖(注意: 观察是否出现ERROR, WARN不用管)
cd /data/yapi/vendors
npm install --production --registry https://registry.npmmirror.com --unsafe-perm
 
# 这一步必须要(可能耗时较久)
npm install mongoose@5.9.10 --unsafe-perm
 
# 安装
npm run install-server

## 安装成功提示：这时候并没有启动
#[root@node0 vendors]# npm run install-server
#  
# > yapi-vendor@1.9.2 install-server /data/yapi/vendors
# > node server/install.js
#  
# log: mongodb load success...
# 初始化管理员账号成功,账号名："admin@admin.com"，密码："ymfe.org"

# 启动服务(一次性),不推荐, 只做测试用
#node /data/yapi/vendors/server/app.js

# 使用pm2管理服务(这时候是启动状态了)
pm2 start /data/yapi/vendors/server/app.js --name yapi

# 开放防火墙
firewall-cmd --zone=public --add-port=3000/tcp --permanent
firewall-cmd --reload

# 验证
curl 127.0.0.1:3000
```
- 服务状态 `pm2 list`
- 服务停止 `pm2 stop yapi`
- 启动服务 `pm2 start yapi`

---
# 遇到的问题
## 执行`yum install -y devtoolset-7-gcc devtoolset-7-gcc-c++`报错
- 修改`/etc/yum.repos.d/CentOS-SCLo-scl.repo`中的`[centos-sclo-sclo]`
```bash
[centos-sclo-sclo]
name=CentOS-7 - SCLo sclo
baseurl=http://mirrors.aliyun.com/centos/7/sclo/$basearch/sclo/
# baseurl=http://mirror.centos.org/centos/7/sclo/$basearch/sclo/
# mirrorlist=http://mirrorlist.centos.org?arch=$basearch&release=7&repo=sclo-sclo
gpgcheck=1
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-SCLo
```
- 替换`/etc/yum.repos.d/CentOS-SCLo-scl-rh.repo`中的`[centos-sclo-rh]`
```bash
[centos-sclo-rh]
name=CentOS-7 - SCLo rh
baseurl=http://mirrors.aliyun.com/centos/7/sclo/$basearch/rh/
gpgcheck=1
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-SCLo
```