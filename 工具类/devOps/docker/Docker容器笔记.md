# docker 容器笔记

|深度|期望|达成|时间|
|---|---|---|---|
|了解|<input type="checkbox" checked/>|<input type="checkbox" />|2022-10-23|
|demo|<input type="checkbox"  checked/>|<input type="checkbox" />|2022-11-03|
|总结|<input type="checkbox"/>|<input type="checkbox" />|

--- 
## 参考书籍
- 拉钩课程: 由浅入深吃透Docker

### docker 是啥? 
- 容器技术的一种

---
### 容器技术使用的linux底层技术
- Namespace: 命名空间技术, 主要5种
  - pid namespace 隔离景晨
  - net namespace 隔离网络接口
  - mnt namespace 隔离文件系统挂载点
  - ipc namespace 隔离信号量,消息队列,共享内存
  - uts namespace 隔离主机和域名
- Cgroup: Linux内核功能, 可以限制和隔离进程的资源使用(CPU/内存/磁盘IO/网络等)
- UnionFS: 联合文件系统, 通过创建文件层进程操作的文件系统, 用于镜像构建和容器运行环境时文件共享
  - docker使用联合文件系统为容器提供联合文件层, 使得容器可以写时复制,以及镜像的分层构建和存储
  - 常用的联合文件系统有: AUFS, Overlay, Devicemapper
- 备注: 以上的底层技术, 由早起的chroot技术演化而来, chroot: 容器的雏形, 一种以隔离方式运行进程的机制.

---
### 镜像&容器&仓库
- 容器的本质是进程
- 镜像与容器, 有点类似java的类和对象, 镜像比作类, 容器比作对象, 
- 镜像仓库 
  - 介绍: 用于管理镜像的仓库, 参考代码仓库(docker官方提供了仓库, 也可以自己搭建私服), 可以通过docker命令进行推送和拉取
  - 选择:
    - [个人推荐]公共镜像仓库: 一般是Docker官方或者其他第三方组织（阿里云，腾讯云，网易云等）提供的，允许所有人注册和使用的镜像仓库。
      - docker官网的仓库地址: docker hub
      - Distribution: docker官方提供的私服镜像, 自行部署即可
    - Harbor
------


---
### docker架构图
> 以前整个docker是一个整体, 后续标准化之后, 拆分成不同组件
![Docker架构图.png](img/Docker架构图.png)
#### Docker相关的组件
- docker: docker命令, docker客户端[参考](https://blog.csdn.net/davidliuzkw/article/details/83987595)
- dockerd: dockerd是Docker服务端的后台常驻进程，用来接收客户端发送的请求， 类似于控制层。
- docker-init: 在业务书过程没有进程回收功能时十分有用（可通过--init作为1号进程接管docker进程，默认是sh）
  - `docker run -it --init --name=test busybox sh`
- docker-proxy: 用于映射主机与容器的端口
  - 例如, 将主机的9988端口映射给容器的80端口 `docker run --name=nginx -d -p 9988:80 nginx`
#### Containerd 相关的组件：
- containerd: 是一个工业级标准的容器运行时环境[参考](https://zhuanlan.zhihu.com/p/361325982)
- containerd-shim: 
  - shim垫片的意思, 用于containerd与runC解耦, 这里的解耦不是接口层面的意思, 比如物理层面上将containerd进程与runC进程(业务进程)解耦
  - shim是真正容器进程的父进程, 因此containerd重启不会影响runC的运行
- ctr: 实际上是containerd-ctr, 是containerd的客户端, 用于调试containerd, 在没有dockerd的环境中, 可以充当docker客户端的角色直接和containerd通讯
#### 容器运行时相关的组件
- runc: 是一个标准的(OCI)容器运行时的实现, 用来运行容器的

---

#### 关于Dockerfile
- dockerfile文件的demo示例
  - 根据busybox构建一个镜像,并且构建过程中会创建一个目录叫做`tmpdir`
  ```dockerfile
  FROM busybox
  RUN mkdir /tmpdir
  ```
- 每行命令由一层文件系统构成,且上层为下层提供依赖[详情参考联合文件系统原理]
  - 
  - [`docker run`]() 命令执行过程(构建容器过程) `docker run -it --name=busybox busybox` 
    - Docker会检查朩地是否-busybox镜像，如果像不-则从DockerHub拉取busybox镜像；
    - 使用busybox境像创建并启动一个容器，
    - 分配文件充，并目在涓像只读层外创建一个读写层
    - 从DockerIP池中分配一个IP给容器
    - 执行用户的启动命令运行像。
- [dockerfile指令](https://www.runoob.com/docker/docker-dockerfile.html)
  - FROM: 构建镜像基于哪个镜像, 必须要以`FROM`开头
  - MAINTAINER: 镜像维护者姓名或邮箱地址
  - RUN: 构建镜像时运行的指令
  - CMD: 运行容器时执行的shell环境
  - VOLUME: 指定容器挂载点到宿主机自动生成的目录或其他容器
  - USER: 为RUN、CMD、和 ENTRYPOINT 执行命令指定运行用户
  - WORKDIR: 为 RUN、CMD、ENTRYPOINT、COPY 和 ADD 设置工作目录，就是切换目录
  - HEALTHCHECH: 健康检查
  - ARG: 构建时指定的一些参数
  - EXPOSE: 声明容器的服务端口（仅仅是声明）
  - ENV: 设置容器环境变量
  - ADD: 拷贝文件或目录到容器中，如果是URL或压缩包便会自动下载或自动解压
  - COPY: 拷贝文件或目录到容器中，跟ADD类似，但不具备自动下载或解压的功能
  - ENTRYPOINT: 运行容器时执行的shell命令
- [docker build命令](https://www.runoob.com/docker/docker-build-command.html) `docker build [OPTIONS] PATH | URL | -`
  - `-t` :指定打包的镜像名称与tag, 如果名称不指定则为`<none>`届时需要通过id来操作, 如果tag不指定则默认为`latest`
    - 示例 `docker build -t image_name:tag_name .`
  - `-f` :指定dockerfile的路径, 如果不指定则使用当前目录下名称为`dockerfile`或`Dockerfile`的文件来构建
    - 示例 `docker build -t test_image:tag -f /root/file_name .`
  - `.` : 后面的`.`是指镜像构建时打包上传到Docker引擎中的文件的目录,不是本机目录, 比如COPY命令(还没懂, 等下次解释)
---
### 关于Docker网络, 为容器插上网线
- 两种标准
  - CNM(Container Network Model): docker定义的容器网络标准, docker容器默认
    - CNM的官方实现(Libnetwork) 
  - CNI: Kubernetes/Google/CoreOS 等大佬发起的事实容器网络标准, 
- Libnetwork 常见的网络模式
  - null空网络模式：可以帮助我们构建一个没有网络接入的容器环境，以保数据安全
  - bridge桥接模式`[Libnetwork默认]`：可以打通容器与容器间网络通信的需求, docker主机内, 主机与容器,容器与容器互通. 跨docker主机需要编排工具配置
  - host主网络模式：可以让容器内的进程共享主机网络，从而监听或修改主机网络。 与主机使用一样网络配置, (与端口??)
  - container网络模式：可以将两个容器放在同一个网络命名空间内，让两个业务通过localhost即可实现访问。 相当于两个容器独享一个网络,与主机无通讯

---
### 关于Docker的卷, 用于容器间的数据共享
- 卷(Volume): 本质是文件或者目录, 通常存放在`/var/lib/docker/volumes`目录下
- docker目前仅支持创建local模式的卷, 若需要网络访问, 需要其他工具配合
- 问: 和-v主机共享目录有什么区别?
  - 主机共享目录可以挂载任意主机目录, 而卷是主机目录中的特殊目录
  - volumes是一个存储概念的封装, 不仅支持local,还支持nfs等网络存储, 相当于抽象层,默认使用local实现
### 关于联合文件系统
- AUFS: AUFS是最早使用的联合文件系统,并未被合并到Linux内核主线，只有ubuntu和Debian等少数操作系统支持
- Devicemapper: CentOS系统中使用的联合文件系统
- OverlayFS[推荐]: Overlay2, docker版本>17.06.02, 内核版本高于4.0
  - overlay2将镜像层和容器层都放在单独的目录，并且有唯一ID，每一层仅存储发生变化的文件，
  - 最终使用联合挂载技术将容器层和镜像层的所有文件统一挂载到容器中，使得容器中看到完整的系统文件
- 其他:
  - 在生产环境中，推荐挂载/var/lib/docker目录到单独的磁盘或者磁盘分区
  - 挂载配置中推荐开启pquota，写入到/etc/fstab中的内容  $UUID/var/lib/docker xfs defaults,pquota 0 0

---
### 关于编排工具
- DockerCompose: 适用于docker开发测试, 命令行, 单机多容器管理系统, 通过解析用户编写的yaml文件，调用DockerAPI实现动态的创建和管理多个容器
- DockerSwarm: docker官方推出的容器集群化编排工具, 命令行, 内置在docker中
- Kubernetes[推荐]:
  - 事实的编排界标准, 简称K8S
- Rancher

---
### daemon.json文件说明
```
{
  "log-level":        "info",     // 日志级别, 默认info
  "storage-driver":   "overlay2",    
  "registry-mirrors": [         
      "https://docker.mirrors.tuna.tsinghua.edu.cn",
      "https://mirror.ccs.tencentyun.com",
      "http://hub-mirror.c.163.com",
      "https://registry.cn-hangzhou.aliyuncs.com",
      "https://docker.m.daocloud.io",
      "https://docker.1panel.live"
  ]
}
```

---
## 常用命令
### 安装docker[参考1]: https://github.com/tech-shrimp/docker_installer
- 安装
```
# 首选
sudo curl -fsSL https://github.com/tech-shrimp/docker_installer/releases/download/latest/linux.sh| bash -s docker --mirror Aliyun
# 备选
# sudo curl -fsSL https://gitee.com/tech-shrimp/docker_installer/releases/download/latest/linux.sh| bash -s docker --mirror Aliyun
```
- 配置daemon.json
```
{ 
  "registry-mirrors": [         
      "https://docker.mirrors.tuna.tsinghua.edu.cn",
      "https://mirror.ccs.tencentyun.com",
      "http://hub-mirror.c.163.com",
      "https://registry.cn-hangzhou.aliyuncs.com",
      "https://docker.m.daocloud.io",
      "https://docker.1panel.live"
  ]
}
```
- 增加用户到用户组, 比如:将`leon`加入到`docker`用户组, `sudo usermod -aG docker leon`, 注意需要重新登录后生效`su - leon`
- 重启docker `sudo service docker restart`
-  [参考1]: https://github.com/tech-shrimp/docker_installer
  - 首选:`sudo curl -fsSL https://github.com/tech-shrimp/docker_installer/releases/download/latest/linux.sh| bash -s docker --mirror Aliyun`
  - 备选:`sudo curl -fsSL https://gitee.com/tech-shrimp/docker_installer/releases/download/latest/linux.sh| bash -s docker --mirror Aliyun`
- 备注: 现在国外的docker仓库已经屏蔽国内用户, 且国内的大部分镜像都对匿名用户限流
### 卸载docker
```
# 停止服务
sudo systemctl stop docker
# 禁用服务
sudo systemctl disable docker
# 删除安装包
sudo apt-get purge -y docker-ce docker-ce-cli containerd.io
# 删除数据目录
sudo rm -rf /var/lib/docker
sudo rm -rf /var/lib/containerd
# 删除配置文件目录
sudo rm -rf /etc/docker
# 删除用户组
sudo groupdel docker
# 更新apt
sudo apt-get update
```

### 镜像相关命令
```shell
# 查看本地仓库镜像
docker images

# 拉取镜像, 从远程仓库拉取busybox镜像到本地仓库
docker pull busybox

# 重命名, 将 busybox:latest 镜像的名称和tag修改为 mybusybox:latest (必须为小写,否则报错) 
docker tag busybox:latest mybusybox:latest 

# 删除镜像, mybusybox:latest 镜像
docker rmi mybusybox:latest

# 构建镜像 - 通过容器构建镜像(容器进行中)
# 将 busybox 容器, 打包为新的镜像(新名称busybox:hello), 并且提交到本地仓库
docker commit busybox busybox:hello

# 构建镜像 - 通过Dockerfile容器构建镜像
# 此命令会根据`file_name`文件,构建一个名叫`test_image`的镜像
docker build -t test_image:tag -f /root/file_name .
# -f 命令可选, 如果不指定文件名称的话, build默认会在当前目录使用名称为`dockerfile`或`Dockerfile`的文件来构建
docker build -t test_image .
# 可以通过网络,比如github来生成镜像, 需要安装git, 
#docker build github.com/creack/docker-firefox #未验证通过

```  
### 容器相关命令
```shell
# 查看容器列表
docker ps [-a 查看已停止容器]

# 创建容器
# 根据busybox镜像(若没有则从远程仓库拉取), 创建一个名为mybusybox的容器
docker create -it --name=mybusybox busybox

# 启动容器
docker start mybusybox

# 重启启动容器
docker restart mybusybox

# 创建并启动容器, 相当于create 与 start命令组合, (d参数表示后台运行)
docker run -itd --name=mybusybox busybox sh
# 创建并启动容器, 运行指定tag的镜像
docker run -itd --name=mybusybox busybox:latest sh
# 创建并启动容器, 指定主机8080端口与容器80端口映射
docker run -itd --name=mybusybox -p 8080:80 busybox sh
# 创建并启动容器, 指定主机8080端口与容器80端口映射, 以及主机/data/dir1目录与容器/data目录映射
docker run -itd --name=mybusybox -p 8080:80 -v /data/dir1:/data busybox sh

# 进入容器[推荐]
docker exec -it mybusybox sh
# 进入容器[不推荐], 单线程, 所有以此命令进入的都在同一个线程, 且退出后会停止容器. 
#docker attach mybusybox

# 退出容器
exit;

# 终止容器
docker stop mybusybox

# 删除容器
docker rm mybusybox

# 导出容器, 将容器导出到文件, 用于迁移(如果有私有库, 那么直接push会更简单)
docker export mybusybox > mybusybox.tar

# 导入容器(恢复), 将导出的容器文件, 恢复成本地镜像
docker import mybusybox.tar mybusybox:latest

# 容器重命名 mybusybox -> newbusbox
docker rename mybusybox newbusbox

```
### 卷的操作命令
```shell
# 创建卷 (volumeName是自定义的名称)
docker volume create volumeName
# 在创建容器时,创建匿名卷,并且与容器中的目录绑定,以下命令中设定的是容器/data目录
docker run -d --name=testvolume -v /data busybox

# 查看所有卷
docker volume ls
# 查看某个卷的详情(volumeName是自定义的名称)
docker volume inspect volumeName

# 使用卷, 创建并启动 test10 容器, 将volumeName这个卷绑定到 test10容器的 /data目录
docker run -d --name=test10 --mount source=volumeName,target=/data busybox

# 删除卷 (使用中的卷无法删除, 需要先`删除`关联的容器)
docker volume rm volumeName

# 容器间共享卷, 不知道这个有什么用, 这个consumer有什么特殊定义吗?和多个容器共享一个目录有什么区别?
docker volume create log-vol
docker run --mount source=log-vol,target=/tmp/log --name=log-producer -it busybox 
docker run -it --name consumer --volumes-from log-producer busybox

```
### 其他
```shell
# 查看容器所有配置信息(参数:container_name容器名称)
docker inspect container_name

# 查看容器与主机目录映射情况(参数:container_name容器名称)
docker inspect r-uat-uat-ccs-web-1-8758c084 |grep Mounts -A50

# 查看端口映射
# 查看主机与容器的所有端口映射
docker ps
# 查看主机与容器的所有端口映射
ps aux|grep docker-proxy
# 查看主机与容器的所有端口映射,以及容器ip情况(拉到下面, Chain DOCKER)
iptables -L -nv -t nat

# 查看容器分配的IP地址(参数:container_name容器名称)
docker inspect --format '{{.NetworkSettings.IPAddress }}' container_name
```
---

### 仓库相关命令
#### 腾讯云私服仓库相关操作
- 先补充一下, 私服仓库并不是说所有的镜像都丢到一个仓库中, 
- 从腾讯的分类来看, 每个镜像都建一个仓库, 然后在仓库里面用tag区分开
- 注意: 由于我的demo大部分用来试验的, 因此我只建了一个demo仓库,并且用tag来表示镜像的名称和版本
```shell
# 任何操作之前先登录私服(100021823412是我的账号)
docker login ccr.ccs.tencentyun.com --username=100021823412
# 从私服获取镜像 docker pull ccr.ccs.tencentyun.com/longfeng.site/demo:[tag], 这里的tag就是对应仓库里的tag

docker pull ccr.ccs.tencentyun.com/longfeng.site/demo:gitlab_13.3.8

# 将本地镜像推送到私服(分两步)
# 第一步 打包, 将本地镜像打包成tag, : docker tag [imageId] ccr.ccs.tencentyun.com/longfeng.site/demo:[tag], imageId通过docker images获得
# 描述, 将3e4c950f748c这个镜像, 打成名称为ccr.ccs.tencentyun.com/longfeng.site/demo:gitlab_13.3.8的tag, 并保存在本地仓库
docker tag 3e4c950f748c ccr.ccs.tencentyun.com/longfeng.site/demo:gitlab_13.3.8
# 第二步 将本地仓库的tag(镜像)推送到私服
docker push ccr.ccs.tencentyun.com/longfeng.site/demo:gitlab_13.3.8

```

---
## 问题
- 有什么好的办法以docker作为基础, 快速的搭建学习(image)环境?
  - 思路: 通过镜像快速部署容器, 并且做好初始化, 如果容器有修改, 则将容器打包成新的镜像放入私服, 然后再将data数据提交到git项目中(或者通过网盘api放到网盘中),
- 写时复制是什么概念
  - 你知道为什么容器的文件系统要设计成写时复制而不是每一个容器都单独拷贝一份镜像文件吗?
    - 每个容器单考贝一份像文件会占用较多的磁空间。
    - 假设我有3个程序都用到了Jdk镜像，那该Jdk镜像得复制3分。如果改写时复制的话，只需要一份Jdk镜像，新的内容写在页层即可。
    - 比如用Docker images查看像的时候，把所有境像的SIZE加起来可能有十几个G，但是到目录下查看，实际上只有几个G，
    - 这是因为很多镜像复用了同一个底层的镜像，起到了节省磁尉空间的作用。
- 怎么主机映射了哪些端口给容器
  - 查看proxy进程暴露了哪些端口: `ps aux|grep docker-proxy`
  - 查看iptables `iptables -L -nv -t nat`
- docker run -itd 中的itd起什么作用?
  - it两个参数, 表示以交互模式运行容器, 并且分配一个伪终端, 如果run时不加此参数,容器会快速从runing->stop 
  - d参数是保持终端在后台运行
  - 保持https://blog.csdn.net/qq_19381989/article/details/102781663
- 怎么查看镜像的所有版本
  - 登录docker仓库官网(国内的仓库好像都不提供网页版):https://hub.docker.com/
  - 搜索具体的镜像名称, 然后进入(注意不要选择错了, 比如`gitlab/gitlab-ce`就有一个`drud/gitlab-ce`他们完全不一样)
  - 切换到tag标签页
- 镜像与虚拟机快照的区别?
  - 镜像是一堆指令的集合, 而虚拟机快照是系统装状态文件的集合
  - 举个栗子: 通过nginx镜像创建的容器, 在进入容器后stop nginx后, 重新打包成镜像, 然后再次使用新镜像运行容器, 此时nginx是运行状态的.
  - 举个栗子: 通过gitlab镜像创建的容器, 在配置配置文件端口后, 重新打包成镜像, 然后再次使用新镜像运行容器, 此时配置文件被覆盖还原了.
- 如何在docker中运行其他软件? 
  - 先执行: apt-get update
  - 安装ping `apt-get install iputils-ping`
- 镜像下载不了怎么办? 
  - 参考 腾讯云私服仓库相关操作 在腾讯云下载镜像, 然后将镜像推送到私服, 然后再下载到本地
--------

# 达成
- [x] 怎么获取镜像
- [x] 怎么生成镜像 通过现有镜像打包成新镜像
- [x] 怎么生成镜像 通过DockerFile构建进项
- [x] 容器生命周期 start/stop
- [x] 容器迁移export/import
- [x] 容器配置-用户 (暂时用不上)
- [x] 容器配置-网络 默认桥接(暂时用不上)
- [x] 容器配置-端口 -p 主机端口:容器端口
- [x] 容器配置-卷(磁盘映射) -v 主机目录:容器目录
- [ ] 容器编排系统(太复杂,需要另花时间学习整理)
- 扩展
  - [x] rancher
  - [x] gitlab
  - [ ] jenkins
  - [ ] maven私服