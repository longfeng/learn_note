# 使用Docker部署rancher



----------------------
## 部署rancher:2.5.5
- [参考](https://cloud.tencent.com/developer/article/1796332)
- [参考](https://www.cnblogs.com/wellful/p/14368038.html)
- 注意: 4G内存吃不消, 至少8G内存
### pull镜像 (1.12GB)
```shell
docker docker pull rancher/rancher:v2.5.16
```  
### 配置端口并刷新
```shell
firewall-cmd --zone=public --add-port=2211/tcp --permanent
firewall-cmd --zone=public --add-port=2212/tcp --permanent
firewall-cmd --reload
```
### 运行容器
```shell
docker run \
-d \
-p 2211:80 -p 2212:443 \
-v /data/rancher/:/var/lib/rancher/ \
--restart=unless-stopped \
--privileged=true \
--name ruancher \
rancher/rancher:v2.5.16

```
- 命令参数解释(这是一条完整的命令 通过\ 连接)
  - `docker run` 运行容器命令
  - `-d ` 让容器后台运行
  - `-p 2211:80` 这里是指将主机的 2211 端口映射到容器中的 80 端口上
  - `-v /data/gitlab/etc:/etc/gitlab` 这里是指将主机的`/etc/gitlab`目录映射到容器中`/data/gitlab/etc`目录上, 其他的-v参数也是一样的
  - `--restart always` 表示在容器退出时总是重启容器，但是不考虑在Docker守护进程启动时就已经停止了的容器
  - `--privileged=true` 使用该参数，启用特权模式安装, 让container内的root拥有真正的root权限。
  - `--name ruancher`  表示构建的容器名称叫做gitlab
  - `rancher/rancher:v2.5.16` 表示使用哪个docker镜像启动容器

### 进入管理页面
- 访问https://192.168.0.12:2212,进入确认密码页面
- admin/Longfeng.site.0
- 设置语言(首页进去, 右下角有个语言选择)
### 添加集群 
- 左上角菜单栏(全局) -> 添加集群 -> 创建新的 Kubernetes 集群(自定义) 
  - 输入集群名称
  - 选择kubernetes版本, 选一样的就好
  - 点击下一步, 此步骤之后, 集群配置已经弄好了, 只需要忘集群里面添加机器就好
- 给集群添加机器
  - 左上角菜单栏(全局) -> 在集群列表中, 选择需要操作的集群(右边的`三个点`菜单) -> Registration Cmd
    - 如果是单机, 则同时勾选`Etcd` `Control` `Worker`
    - 如果是集群, 则根据情况配置
  - 注意:
    - docker run 命令中需要加一个`-e CATTLE_NODE_NAME=node_name`参数,而且必须不能唯一, 否则可能出现注册失败的情况
      - `docker run -d --privileged -e CATTLE_NODE_NAME=node_name --restart=unless-stopped --net=host -v /etc/kubernetes:/etc/kubernetes -v /var/run:/var/run  rancher/rancher-agent:v2.5.16 --server https://192.168.0.12:2212 --token k2fxkllt29pbw6wj9zw74mrwhfhjdv2tqwf4zmfl6hjmwt6vzvcwbl --ca-checksum 9d3a03d21eae29e3bd046ab646a42e8b50a2c2df2dce2725a63e26e675c5f3fa --etcd --controlplane --worker` 
    - 添加好之后可能中途会出现报错或者情况请耐心等待
### 未完
- 如何配置私服??
- 如何完成自动部署??
- 如何通过api调用命令??
### 参考文章
> [rancher-1：使用rancher-2.5.5部署单节点kubernetes集群](https://cloud.tencent.com/developer/article/1796332)
> [Rancher v2.5.9添加节点主机的方法](https://zhuanlan.zhihu.com/p/412970150)
----------------------

## ?问题