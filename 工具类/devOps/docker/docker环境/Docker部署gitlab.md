# 使用Docker部署gitlab-ce

## 简介: 本文一共提供三种部署方式
- 自制的demo部署
- gitlab-ce:13.3.8-ce.0 
  - [推荐](理由, 依赖较少, 内存和io占用少)
  - 最低配置: 2C3G
- gitlab-ce:15.5.1
  - 最低配置: 2C4G, 磁盘IO: 100MB+

## 部署自制的demo(基于gitlab-ce:13.3.8-ce.0)
```shell
# 从10.0.8.8服务器scp
scp /home/data/docker_data/gitlab_data.gz root@10.0.8.16:/data
# 然后登录
ssh 10.0.8.16
# 解压
tar -zxvf gitlab_data.gz
# 修改配置文件(如果在10.0.0.16上就无需修改了)
vi /data/gitlab/etc/gitlab.rb
# 修改主机目录(假设当前机器外部ip地址为175.178.1.1)
# external_url 'http://175.178.1.1:2201/'
# 设置ssh主机ip (关键词:gitlab_ssh_host)修改为下面的
#gitlab_rails['gitlab_ssh_host'] = '175.178.1.1'
# 设置ssh连接端口 (关键词:gitlab_shell_ssh_port)修改为下面的
#gitlab_rails['gitlab_shell_ssh_port'] = 2202

# 登录腾讯云私服
docker login ccr.ccs.tencentyun.com --username=100021823412

# 拉取镜像
docker pull ccr.ccs.tencentyun.com/longfeng.site/demo:gitlab_13.3.8

# 运行
docker run \
-itd \
-p 2201:2201 -p 2202:2202 \
-v /data/gitlab/etc:/etc/gitlab \
-v /data/gitlab/log:/var/log/gitlab \
-v /data/gitlab/opt:/var/opt/gitlab \
-v /etc/localtime:/etc/localtime:ro \
--restart always \
--privileged=true \
--name gitlab \
ccr.ccs.tencentyun.com/longfeng.site/demo:gitlab_13.3.8

# 访问 gitlab
curl http://127.0.0.1:2201

# 用户名/密码 root/Longfeng.site.0

```

----------------------
## 部署gitlab-ce:13.3.8-ce.0
### pull镜像 (2.76GB)
```shell
docker pull gitlab/gitlab-ce:13.3.8-ce.0
```  
### 配置端口并刷新
```shell
firewall-cmd --zone=public --add-port=2201/tcp --permanent
firewall-cmd --zone=public --add-port=2202/tcp --permanent
firewall-cmd --reload
```
### 运行容器
```shell
docker run \
-itd \
-p 2201:2201 -p 2202:2202 \
-v /data/gitlab/etc:/etc/gitlab \
-v /data/gitlab/log:/var/log/gitlab \
-v /data/gitlab/opt:/var/opt/gitlab \
-v /etc/localtime:/etc/localtime:ro \
--restart always \
--privileged=true \
--name gitlab \
gitlab/gitlab-ce:13.3.8-ce.0

docker exec -it gitlab sh
# 进入容器后执行应该是得到以下的结果
gitlab-ctl status
# gitlab-ctl status
#run: alertmanager: (pid 2043) 134s; run: log: (pid 1037) 427s
#run: gitaly: (pid 2051) 133s; run: log: (pid 543) 528s
#run: gitlab-exporter: (pid 2064) 133s; run: log: (pid 859) 447s
#run: gitlab-workhorse: (pid 2067) 133s; run: log: (pid 785) 465s
#run: grafana: (pid 2075) 132s; run: log: (pid 1117) 392s
#run: logrotate: (pid 2086) 132s; run: log: (pid 840) 453s
#run: nginx: (pid 2105) 131s; run: log: (pid 814) 459s
#run: postgres-exporter: (pid 2111) 131s; run: log: (pid 1058) 423s
#run: postgresql: (pid 2122) 130s; run: log: (pid 633) 522s
#run: prometheus: (pid 2131) 130s; run: log: (pid 919) 433s
#run: puma: (pid 2143) 129s; run: log: (pid 737) 477s
#run: redis: (pid 2148) 129s; run: log: (pid 436) 535s
#run: redis-exporter: (pid 2153) 129s; run: log: (pid 881) 439s
#run: sidekiq: (pid 2237) 126s; run: log: (pid 755) 471s
#run: sshd: (pid 2502) 17s; run: log: (pid 33) 553s

# 修改sshd端口22->2202, 否则2202端口并不能作为ssh的登录地址, [参考](https://blog.csdn.net/chihaihai/article/details/122709431)
vi /assets/sshd_config
# Port 2202
# 重启sshd服务
service ssh restart

# 配置gitlab的访问信息
vi /etc/gitlab/gitlab.rb
# 设置gitlab访问地址, 此地址决定http clone地址, 注意中间没有=号 (假设当前机器外部ip地址为175.178.1.1)
# external_url 'GENERATED_EXTERNAL_URL' (关键词:GENERATED_EXTERNAL_URL)修改为下面的
# external_url 'http://175.178.1.1:2201/'
# 设置ssh主机ip (关键词:gitlab_ssh_host)修改为下面的
#gitlab_rails['gitlab_ssh_host'] = '175.178.1.1'
# 设置ssh连接端口 (关键词:gitlab_shell_ssh_port)修改为下面的
#gitlab_rails['gitlab_shell_ssh_port'] = 2202

# 让配置生效
gitlab-ctl reconfigure
# 重新启动gitlab
gitlab-ctl restart
```
- 命令参数解释(这是一条完整的命令 通过\ 连接)
  - `docker run` 运行容器命令
  - `-itd `: `it`以交互式方式运行容器,`d`在后台运行
  - `-p 2201:2201` 这里是指将主机的 2201 端口映射到容器中的2201端口上
    - 注意, 这里如果将容器的22端口不做修改直接映射的话, 可能会出现项目中的ssh地址错误, 无法copy的情况
    - 所以最好与主机保持一样的端口, 只是容器22端口变更还需要在容器中修改sshd的配置
  - `-v /data/gitlab/etc:/etc/gitlab` 这里是指将主机的`/etc/gitlab`目录映射到容器中`/data/gitlab/etc`目录上, 其他的-v参数也是一样的
  - `--restart always` 表示重启docker时自动重启gitlab容器
  - `--privileged=true` 使用该参数，启用特权模式安装, 让container内的root拥有真正的root权限。
  - `--name gitlabe`  表示构建的容器名称叫做gitlab
  - `gitlab/gitlab-ce` 表示使用哪个docker镜像启动容器

----------------------
## 部署gitlab-ce:15.5.1
### pull镜像 (2.76GB)
```shell
docker pull gitlab/gitlab-ce:15.3.5-ce.0
```  
### 配置端口并刷新
```shell
firewall-cmd --zone=public --add-port=2201/tcp --permanent
firewall-cmd --zone=public --add-port=2202/tcp --permanent
firewall-cmd --reload
```
### 运行容器
```shell
docker run \
-itd \
-p 2201:2201 -p 2202:2202 \
-v /data/gitlab/etc:/etc/gitlab \
-v /data/gitlab/log:/var/log/gitlab \
-v /data/gitlab/opt:/var/opt/gitlab \
-v /etc/localtime:/etc/localtime:ro \
--restart always \
--privileged=true \
--name gitlab \
gitlab/gitlab-ce:15.3.5-ce.0

docker exec -it gitlab sh
# 进入容器后执行应该是得到以下的结果
gitlab-ctl status
# run: alertmanager: (pid 671) 23s; run: log: (pid 681) 22s
# run: gitaly: (pid 299) 84s; run: log: (pid 315) 83s
# run: gitlab-exporter: (pid 611) 42s; run: log: (pid 624) 39s
# run: gitlab-kas: (pid 505) 72s; run: log: (pid 517) 69s
# run: gitlab-workhorse: (pid 553) 54s; run: log: (pid 571) 50s
# run: logrotate: (pid 270) 96s; run: log: (pid 278) 95s
# run: nginx: (pid 582) 48s; run: log: (pid 594) 45s
# run: postgres-exporter: (pid 692) 17s; run: log: (pid 700) 16s
# run: postgresql: (pid 329) 78s; run: log: (pid 502) 75s
# run: prometheus: (pid 655) 30s; run: log: (pid 668) 27s
# run: puma: (pid 520) 66s; run: log: (pid 527) 65s
# run: redis: (pid 282) 90s; run: log: (pid 296) 87s
# run: redis-exporter: (pid 642) 36s; run: log: (pid 651) 33s
# run: sidekiq: (pid 532) 60s; run: log: (pid 549) 56s
# run: sshd: (pid 30) 106s; run: log: (pid 29) 106s

# 修改sshd端口22->2202, 否则2202端口并不能作为ssh的登录地址, [参考](https://blog.csdn.net/chihaihai/article/details/122709431)
vi /assets/sshd_config
# Port 2202
# 重启sshd服务
service ssh restart

# 配置gitlab的访问信息
vi /etc/gitlab/gitlab.rb
# 设置gitlab访问地址, 此地址决定http clone地址, 注意中间没有=号
# external_url 'GENERATED_EXTERNAL_URL' (关键词:GENERATED_EXTERNAL_URL)修改为下面的
# external_url 'http://175.178.1.1:2201/'
# 设置ssh主机ip
#gitlab_rails['gitlab_ssh_host'] = '175.178.1.1'
# 设置ssh连接端口
#gitlab_rails['gitlab_shell_ssh_port'] = 2202

# 让配置生效
gitlab-ctl reconfigure
# 重新启动gitlab
gitlab-ctl restart

```
- 命令参数解释(这是一条完整的命令 通过\ 连接)
  - `docker run` 运行容器命令
  - `-p 2201:2201` 这里是指将主机的 2201 端口映射到容器中的2201端口上
    - 注意, 这里如果将容器的22端口不做修改直接映射的话, 可能会出现项目中的ssh地址错误, 无法copy的情况
    - 所以最好与主机保持一样的端口, 只是容器22端口变更还需要在容器中修改sshd的配置
  - `-v /data/gitlab/etc:/etc/gitlab` 这里是指将主机的`/etc/gitlab`目录映射到容器中`/data/gitlab/etc`目录上, 其他的-v参数也是一样的
  - `--restart always` 表示重启docker时自动重启gitlab容器
  - `--privileged=true` 使用该参数，container内的root拥有真正的root权限。
  - `--name gitlabe`  表示构建的容器名称叫做gitlab
  - `gitlab/gitlab-ce` 表示使用哪个docker镜像启动容器

----------

# 使用Docker部署gitlab-ce:13.3.8-ce.0
## 步骤
### pull镜像 (大概1.26GB)
```shell
docker pull gitlab/gitlab-ce
```  
### 配置端口并刷新
```shell
firewall-cmd --zone=public --add-port=2201/tcp --permanent
firewall-cmd --zone=public --add-port=2202/tcp --permanent
firewall-cmd --reload
```
### 运行容器
```shell
docker run \
-itd \
-p 2201:2201 -p 2202:2202 \
-v /data/gitlab/etc:/etc/gitlab \
-v /data/gitlab/log:/var/log/gitlab \
-v /data/gitlab/opt:/var/opt/gitlab \
-v /etc/localtime:/etc/localtime:ro \
--restart always \
--privileged=true \
--name gitlab \
gitlab/gitlab-ce

docker exec -it gitlab sh
# 进入容器后执行应该是得到以下的结果
gitlab-ctl status
# run: alertmanager: (pid 671) 23s; run: log: (pid 681) 22s
# run: gitaly: (pid 299) 84s; run: log: (pid 315) 83s
# run: gitlab-exporter: (pid 611) 42s; run: log: (pid 624) 39s
# run: gitlab-kas: (pid 505) 72s; run: log: (pid 517) 69s
# run: gitlab-workhorse: (pid 553) 54s; run: log: (pid 571) 50s
# run: logrotate: (pid 270) 96s; run: log: (pid 278) 95s
# run: nginx: (pid 582) 48s; run: log: (pid 594) 45s
# run: postgres-exporter: (pid 692) 17s; run: log: (pid 700) 16s
# run: postgresql: (pid 329) 78s; run: log: (pid 502) 75s
# run: prometheus: (pid 655) 30s; run: log: (pid 668) 27s
# run: puma: (pid 520) 66s; run: log: (pid 527) 65s
# run: redis: (pid 282) 90s; run: log: (pid 296) 87s
# run: redis-exporter: (pid 642) 36s; run: log: (pid 651) 33s
# run: sidekiq: (pid 532) 60s; run: log: (pid 549) 56s
# run: sshd: (pid 30) 106s; run: log: (pid 29) 106s

# 修改sshd端口22->2202, 否则2202端口并不能作为ssh的登录地址, [参考](https://blog.csdn.net/chihaihai/article/details/122709431)
vi /assets/sshd_config
# Port 2202
# 重启sshd服务
service ssh restart

# 配置gitlab的访问信息
vi /etc/gitlab/gitlab.rb
# 设置gitlab访问地址, 此地址决定http clone地址, 注意中间没有=号
# external_url 'GENERATED_EXTERNAL_URL' (关键词:GENERATED_EXTERNAL_URL)修改为下面的
# external_url 'http://175.178.1.1:2201/'
# 设置ssh主机ip
#gitlab_rails['gitlab_ssh_host'] = '175.178.1.1'
# 设置ssh连接端口
#gitlab_rails['gitlab_shell_ssh_port'] = 2202

# 让配置生效
gitlab-ctl reconfigure
# 重新启动gitlab
gitlab-ctl restart

```
- 命令参数解释(这是一条完整的命令 通过\ 连接)
  - `docker run` 运行容器命令
  - `-p 2201:2201` 这里是指将主机的 2201 端口映射到容器中的2201端口上
    - 注意, 这里如果将容器的22端口不做修改直接映射的话, 可能会出现项目中的ssh地址错误, 无法copy的情况
    - 所以最好与主机保持一样的端口, 只是容器22端口变更还需要在容器中修改sshd的配置
  - `-v /data/gitlab/etc:/etc/gitlab` 这里是指将主机的`/etc/gitlab`目录映射到容器中`/data/gitlab/etc`目录上, 其他的-v参数也是一样的
  - `--restart always` 表示重启docker时自动重启gitlab容器
  - `--privileged=true` 使用该参数，container内的root拥有真正的root权限。
  - `--name gitlabe`  表示构建的容器名称叫做gitlab
  - `gitlab/gitlab-ce` 表示使用哪个docker镜像启动容器



## ?问题
### ?gitlab如何重启查看状态
- 关闭：gitlab-ctl stop
- 开启：gitlab-ctl start
- 重启：gitlab-ctl restart
- 查看状态：gitlab-ctl status

### ?重装镜像时run命令失败
```
Error response from daemon: driver failed programming external connectivity on endpoint gitlab  (014f161050223ae967081743baafa5499fed95c3b1e7f0efd1a9a9504545309f):
(iptables failed: iptables --wait -t nat -A DOCKER -p tcp -d 0/0 --dport 10443 -j DNAT --to-destination 172.17.0.2:443 ! -i docker0: iptables: No chain/target/match by that name.
```
- 原因是 重启了防火墙导致docker的iptables重置, 
- 所以解决办法是重启docker:`service docker restart` 
-  (这个命令没起作用不知道为啥)`systemctl start docker`

### ?gitlab首次打开没有弹出确认修改默认密码界面(如何修改默认用户root密码)
- [参考](https://blog.csdn.net/stronglyh/article/details/101358425)
- 进入容器 `docker exec -it gitlab sh`
  - 如果不是在容器中部署,此处略过
  - 部分容器因为机器的配置太低,导致此命令执行时间非常长, 请耐心等待(预计3分钟左右), 中间可以用回车来试探
- 进入gitlab命令行: `gitlab-rails console`
- 获取用户: `user = User.where(id:1).first`
- 修改密码: `user.password='Pa@123456!'`, 必须要8位数
- 保存修改: `user.save`
- 使用新的账号密码进行登录root/Pa@123!

### ?有哪些系统配置
- 配置语言: User Settings(点击头像) > Preferences > Localization > Language
- 修改重置用户的密码(管理员): 左上角 Admin > Overview > Users > 找到用户并进入编辑 > Password

### ?用户首次登录进去 (Page Not Found)
- 通过首页重新登录一下

### ?验证gitlab是否可以正常工作
```shell
# 1.新建一个用户leon
# 2.新建一个项目test
# 3.更新项目到本地
git clone http://xxx:20080/leon/test.git
# 4.修改项目中的README.md文件
# 5.提交修改
git commit -m "注释"
# 6.push
git push
```

### ~~~?修改http中的host地址(使用http克隆时host不正确)~~~ 这是临时性的调整
- ~~~修改`/opt/gitlab/embedded/service/gitlab-rails/config/gitlab.yml`文件~~~
- ~~~修改路径: production > gitlab 下的`~~~
  - ~~~host: 将host修改为服务器ip`~~~
  - ~~~port: 修改为访问的端口`~~~
```yaml
production: &base
#
# 1. GitLab app settings
# ==========================

## GitLab settings
  gitlab:
  ## Web server settings (note: host is the FQDN, do not include http://)
    host: 175.178.1.1
    port: 20080
    https: false
```
