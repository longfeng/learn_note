# 安装FRP
- 官方文档: https://gofrp.org/zh-cn/docs/setup/
## 安装服务端
```shell
mkdir mkdir -p /opt/frp
# 下载
wget https://github.com/fatedier/frp/releases/download/v0.61.0/frp_0.61.0_linux_amd64.tar.gz
# 解压
tar -zxvf frp_0.61.0_linux_amd64.tar.gz
# 进入目录
cd frp_0.61.0_linux_amd64
# 创建文件(参考下面的配置文件
vi frps.ini

# 启动
nohup ./frps -c ./frps.ini &

# 防火墙开放端口
firewall-cmd --zone=public --add-port=7000/tcp --permanent
firewall-cmd --zone=public --add-port=7500/tcp --permanent
firewall-cmd --zone=public --add-port=80/tcp --permanent
firewall-cmd --zone=public --add-port=443/tcp --permanent
firewall-cmd --reload
```
- 配置文件
```frps.init
[common]
# 监听的端口
bind_port = 7000
# HTTP 端口
vhost_http_port = 80
# HTTPS 端口
vhost_https_port = 443
# Dashboard 端口
dashboard_port = 7500
# Dashboard 用户名
dashboard_user = admin
# Dashboard 密码
dashboard_pwd = admin
# 日志文件
log_file = /var/log/frps.log
log_level = info
```
### 验证 dashboard
- http://127.0.0.1:7500

## 安装客户端



如何通过frp在github的Release文件部署frp服务
2024/11/17 21:34:59
复制
删除

通过 GitHub 的 Release 文件部署 FRP 服务，需要按照以下步骤来进行操作：

步骤 1：下载 FRP 发布包
FRP（Fast Reverse Proxy）是一个高性能的反向代理应用，可以用于内网穿透。你可以从 GitHub 上获取 FRP 的最新发布版本，并下载对应的压缩包。

访问 FRP GitHub 发布页

打开 FRP 的 GitHub Release 页面。

选择合适的版本

在 Releases 页面中，选择一个稳定的版本，并下载对应平台的压缩包。以 Linux 系统为例，下载名为 frp-x.y.z-linux-amd64.tar.gz 的文件（其中 x.y.z 为版本号）。

步骤 2：上传并解压 FRP
上传到服务器

通过 scp 或者其他文件传输工具（如 rsync）将下载的文件上传到 CentOS 7 服务器的目标目录。例如，将 frp-x.y.z-linux-amd64.tar.gz 上传到 /opt/frp/。

bash
scp frp-x.y.z-linux-amd64.tar.gz user@your-server:/opt/frp/
连接到服务器并解压文件

使用 ssh 登录到服务器，然后解压文件：

bash
cd /opt/frp
tar -zxvf frp-x.y.z-linux-amd64.tar.gz
cd frp-x.y.z-linux-amd64
步骤 3：配置 FRP
FRP 包含客户端和服务器端，通常需要配置两部分：

FRP 服务器端配置（frps）
FRP 客户端配置（frpc）
1. 配置 FRP 服务器端（frps）
在服务器上，编辑 frps.ini 配置文件，根据你的需求进行修改：

bash
# frps.ini

[common]
bind_port = 7000    # 监听的端口
vhost_http_port = 80  # HTTP 端口
vhost_https_port = 443 # HTTPS 端口
dashboard_port = 7500   # Dashboard 端口
dashboard_user = admin  # Dashboard 用户名
dashboard_pwd = admin   # Dashboard 密码
log_file = /var/log/frps.log  # 日志文件
log_level = info
2. 配置 FRP 客户端（frpc）
在客户端上，编辑 frpc.ini 配置文件，指定要代理的服务：

bash
# frpc.ini

[common]
server_addr = x.x.x.x  # FRP 服务器的 IP 地址
server_port = 7000     # 服务器的端口

# 代理 HTTP 服务
[web]
type = http
local_port = 80        # 本地服务端口
custom_domains = www.example.com  # 自定义域名
步骤 4：启动 FRP 服务
启动 FRP 服务器端（frps）

在服务器端，使用以下命令启动 frps 服务：

bash
./frps -c ./frps.ini
你可以使用 nohup 或者 systemd 来让 frps 在后台运行。

bash
nohup ./frps -c ./frps.ini &
或者创建一个 systemd 服务以确保 frps 自动启动：

bash
sudo nano /etc/systemd/system/frps.service
文件内容如下：

ini
[Unit]
Description=FRP Server
After=network.target

[Service]
ExecStart=/opt/frp/frp-x.y.z-linux-amd64/frps -c /opt/frp/frp-x.y.z-linux-amd64/frps.ini
Restart=always
User=root
WorkingDirectory=/opt/frp/frp-x.y.z-linux-amd64

[Install]
WantedBy=multi-user.target
然后，重新加载 systemd 配置并启动服务：

bash
sudo systemctl daemon-reload
sudo systemctl start frps
sudo systemctl enable frps
启动 FRP 客户端（frpc）

在客户端，使用以下命令启动 frpc 服务：

bash
./frpc -c ./frpc.ini
同样，你可以将 frpc 设置为后台进程，或者使用 systemd 来管理它。

步骤 5：验证服务
验证 FRP 服务器端（frps）是否在运行

在服务器上运行 ps 或 netstat 来确保 FRP 服务器端正在监听指定的端口（例如 7000）。

bash
netstat -tulnp | grep 7000
验证 FRP 客户端是否能够连接

在客户端检查是否成功连接到 FRP 服务器，可以查看 frpc 的日志输出。

总结
从 GitHub Release 页面下载 FRP 的对应版本。
上传并解压 FRP 包。
配置并启动 FRP 服务（服务器端和客户端）。
确保 FRP 正常运行并且能够成功进行内网穿透。
这种方式通过 GitHub 的 Release 文件可以非常方便地在服务器上部署 FRP 服务。如果你希望进一步优化部署过程，可以考虑自动化脚本或者系统管理工具来管理 FRP 服务。

以上内容为 AI 生成，请核查重要信息！