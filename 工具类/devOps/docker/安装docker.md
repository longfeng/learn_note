# 安装docker与docker-compose

## centos7 安装docker CE
### 系统准备
```shell
# 更新系统包
sudo yum update -y

# 检查系统内核版本（确保内核符合 Docker 要求，版本需 ≥ 3.10）：
uname -r

# 卸载旧版本 Docker（如已安装）：
sudo yum remove -y docker docker-common docker-selinux docker-engine
```
### 安装 Docker
```shell

# 安装必要的依赖包：
sudo yum install -y yum-utils device-mapper-persistent-data lvm2

# 安装 yum-utils 包（提供 yum-config-manager 工具）：
sudo yum install -y yum-utils
# 配置 Docker 官方源(二选一)：
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
# 配置阿里云 Docker 源(二选一)：
sudo yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

# 安装 Docker CE（Community Edition）：
sudo yum install -y docker-ce docker-ce-cli containerd.io

# 验证 Docker 版本：
docker --version
```
### 启动与配置 Docker
```shell
# 启动 Docker 服务：
sudo systemctl start docker

# 设置开机自启：
sudo systemctl enable docker

# 检查 Docker 服务状态：
sudo systemctl status docker
```
### 运行测试容器(此处因为镜像不可用, 所以未验证)
```shell
# 验证 Docker 是否安装成功：
sudo docker run hello-world

# 如果输出类似以下内容，说明安装成功：
# Hello from Docker!
# This message shows that your installation appears to be working correctly.
```
---

## 安装docker-compose
```shell
# 安装 docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
# 添加执行权限
sudo chmod +x /usr/local/bin/docker-compose
# 创建软链接
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
# 测试是否安装成功
docker-compose --version
```

### 常见问题与解决

--------

