# 
cd /usr/local/src
# 下载
wget http://dlcdn.apache.org/tomcat/tomcat-8/v8.5.72/bin/apache-tomcat-8.5.72.tar.gz

# 解压到/usr/local/
tar -xf apache-tomcat-8.5.72.tar.gz -C /usr/local/
# 创建快捷方式
ln -sv /usr/local/apache-tomcat-8.5.72 /usr/local/tomcat

# 删除管理目录
# rm -rf /usr/local/tomcat/webapps
# mkdir /usr/local/tomcat/webapps

# 启动
/usr/local/tomcat/bin/startup.sh

# 访问
http://xxx:8080/



