# 设置下载源(不更新这个的话, 会使用系统自带的git版本进行安装, 可能导致与gitlab-runner不兼容)
yum -y install http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-2.noarch.rpm
# 安装
yum -y install git

### 如何保存用户名密码
#### 项目为单位保存
- 先将项目pull下来
- 修改项目根目录下`vim .git/config`文件, 注意它是个隐藏文件, 看不见,但是可以直接这样编辑
    - 在末尾加上这段配置
    ```
    [credential]
        helper = store
    ```
    - 再拉一次代码(会提示输入用户名密码),后续就不需要了
#### 以全局配置保存:
- `git config --global credential.helper store`