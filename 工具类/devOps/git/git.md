### 本地merger与在git上远程merger有什么区别?
- 经过测试没有区别
- merger原理:[递归三路合并算法](https://www.jianshu.com/p/e8932999fe1f)

### rebase是啥?
- 从来重置基线的, 通过这个命令整理出来的提交记录会变得干净整洁,
- 但是千万不要再公共分支上使用, 否则可能会导致代码丢失或者冲突等错误,
[参考](https://www.jianshu.com/p/4079284dd970)

### git提交记录中的时间是commit时间而非push时间


### 批量获取项目地址
```js
function getProjectGitPath(baseUrl, groupName){

	var hasNext = true;
	var result = "";
	for(var i=1; i<10; i++){
		if (!hasNext){
			break;
		}
		
		$.ajax({
		  "type": "GET",
		  "async": false,
		  "url": baseUrl+"/groups/"+groupName+"/-/children.json?page="+i,
		  "success": function(data){
			if(!data || data.length==0){
				hasNext = false;
				return;
			}
			for (var i in data){
				result += ("git clone "+ baseUrl+data[i]["relative_path"]+".git\r\n");
			}
		  }
		});
	}
	if(result === ""){
		throw "请在项目组根路径下执行,或者你可以手动指定项目路径"
	}
	return result;
}

// http://gitlab.xxx.com/
var baseUrl = null;
// group
var groupName = null;
if(!baseUrl||!groupName){
baseUrl = window.location.origin;
groupName = window.location.pathname.replace("/","");
}
getProjectGitPath(baseUrl,groupName);

```

### 批量更新项目代码
```xml
<dependencies>
    <dependency>
        <groupId>cn.hutool</groupId>
        <artifactId>hutool-all</artifactId>
        <version>5.5.9</version>
    </dependency>
    <dependency>
        <groupId>org.eclipse.jgit</groupId>
        <artifactId>org.eclipse.jgit</artifactId>
        <version>3.7.0.201502260915-r</version>
    </dependency>
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <version>1.18.6</version>
    </dependency>
    <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>1.7.29</version>
    </dependency>
</dependencies>
```
```java
package com.leon.learn.testdemo;

import cn.hutool.core.util.ArrayUtil;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class CmdTest {

    private static String userName = "tanlf11";
    private static String password = "Qqq@202104";
    private static String masterBranch = "master";
    // 输入目录
    private static String root = "C:\\work\\tieqilishi\\code";

    public static void main(String[] args) throws IOException, GitAPIException {

        File file = new File(root);
        if (!file.exists()){
            throw new RemoteException("文件目录不存在!");
        }
        if (ArrayUtil.isEmpty(file.listFiles())){
            throw new RemoteException("文件目录无子项目!");
        }

        for (File projectDir : Objects.requireNonNull(file.listFiles())) {
            if (isNotGitDir(projectDir)){
                System.err.println(projectDir.getName()+" is not git dir");
                continue;
            }
            Git git = Git.open(projectDir);
            System.out.println("\r\nhandler dir:"+projectDir.getName());
            if (!hasUncommittedChanges(git)){
                if (!masterBranch.equalsIgnoreCase(getCurrentBranch(git))){
                    if (getLocalBranchList(git).contains(masterBranch)){
                        // 感觉直接删除后重新更新会更方便(解决冲突)
                        // git.branchDelete().setBranchNames(masterBranch).call();
                        git.checkout().setCreateBranch(false).setName(masterBranch).call();
                        System.out.println("checkout "+masterBranch);
                    }else {
                        git.checkout().setCreateBranch(true).setName(masterBranch).call();
                        System.out.println("checkout "+masterBranch+" for new local branch!");
                    }
                    CredentialsProvider usernamePasswordCredentialsProvider = new UsernamePasswordCredentialsProvider(userName, password);
                    git.pull().setCredentialsProvider(usernamePasswordCredentialsProvider).call();
                    System.err.println("pull\r\n");
                }
            }else{
                System.err.println("该项目有未提交的文件,跳过更新");
            }
        }
    }

    private static boolean isNotGitDir(File projectDir) {
        if (projectDir.isFile()){
            return true;
        }
        if (ArrayUtil.isEmpty(projectDir.listFiles())){
            return true;
        }
        return Arrays.stream(Objects.requireNonNull(projectDir.listFiles()))
                .filter(File::isDirectory)
                .noneMatch(dir -> dir.getName().equalsIgnoreCase(".git"));
    }

    public static List<String> getLocalBranchList(Git git) throws GitAPIException {
        List<Ref> branchList = git.branchList().call();

        List<String> result = new ArrayList<>();
        for (Ref ref : branchList) {
            result.add(ref.getName().replace("refs/heads/", ""));
        }
        return result;
    }

    private static String getCurrentBranch(Git git) throws IOException {
        return git.getRepository().getBranch();
    }

    private static boolean hasUncommittedChanges(Git git) throws GitAPIException {
        return git.status().call().hasUncommittedChanges();
    }
}
```



