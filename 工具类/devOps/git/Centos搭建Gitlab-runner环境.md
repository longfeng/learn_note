# 搭建Gitlab-runner环境
|深度|期望|达成|时间|
|---|---|---|---|
|了解|<input type="checkbox" checked/>|<input type="checkbox" checked/>|2022-03-01|
|demo|<input type="checkbox" checked/>|<input type="checkbox" checked/>|2022-03-04|
|总结|<input type="checkbox" />|<input type="checkbox" />|

## 安装gitlab runner(CentOS7.6)
```shell
cd /usr/local/src

## 下载初始化脚本(初始化rpm依赖, 否则yum install 找不到gitlab-runner)
wget https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh

## 执行初始化脚本
sh script.rpm.sh

## 执行安装最新gitlab-runner版本, 若想指定版本, 可以通过yum search gitlab-runner然后再yum install
yum install -y gitlab-runner

## 验证gitlab-runner是否成功
gitlab-runner --version

```

## 将gitlab runner注册到gitlab上
### Set up a specific Runner manually方式
1. 得到gitlab注册用的token
   - 路径: gitlab -> 选择项目 -> Settings -> CI/CD -> Runners -> (查看Set up a specific Runner manually步骤)
2. 通过`gitlab-ci-multi-runner register`命令, 根据提示依次输入, 并回车
   - [必填] gitlab的地址, gitlab得到
   - [必填] token ,gitlab得到
   - [必填] description, 建议ip+描述:ip_192_168_11_228
   - [必填] tags, 无建议, 比如项目名称
   - [必填] ci执行环境, 根据情况选择, 比较多的是, shell或者docker
3. 参考下面的示例
```shell
[root@VM001 root]# gitlab-ci-multi-runner register
Runtime platform                                    arch=amd64 os=linux pid=97749 revision=775dd39d version=13.8.0
Running in system-mode.                            
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):
http://gitlab.leon.com/
Enter the registration token:
xvAyphTg4qLTBaUayM2y
Enter a description for the runner:
[VM001]: ip_192_168_12_228
Enter tags for the runner (comma-separated):
learn
Registering runner... succeeded                     runner=xvAyphTg
Enter an executor: docker, virtualbox, parallels, shell, ssh, docker+machine, docker-ssh+machine, kubernetes, custom, docker-ssh:
shell
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```
4. 注意
   - 如果选择docker executor, 那么会继续让选择一个docker的底包
   - 这个底包 可以使用`runner`

> 更多CI问题请参考[Git的CI_CD总结.md](../Git的CI_CD总结.md)