# docker and Rancher
|深度|期望|达成|时间|
|---|---|---|---|
|了解|<input type="checkbox" checked/>|<input type="checkbox" />|
|demo|<input type="checkbox" />|<input type="checkbox" />|
|总结|<input type="checkbox" />|<input type="checkbox" />|

### docker是什么?

### rancher是什么?
- https://zhuanlan.zhihu.com/p/109033560
- docker 

### docker和rancher是什么关系?

### [极简Docker和Kubernetes发展史](https://www.cnblogs.com/chenqionghe/p/11454248.html)
### [10分钟看懂Docker和kubernetes](https://cloud.tencent.com/developer/article/1450362)
- docker 是一家notcloud的公司搞出来的一个虚拟化技术,也叫作容器技术
- 相对于vmware轻量
    - 资源消耗小, 不需要虚拟整个操作系统, 只需要虚拟一个小规模的环境???
    - 占用空间小, 容器只需要MB级甚至KB级
    - 免费
- Docker三大概念: Build once，Run anywhere（一次构建，随处运行）
    - 镜像(image)
    - 容器(container)
    - 仓库(repository)
- Kubernetes 简称 K8S, 主打容器编排


### 怎样使用docker与rancher进行容器化部署