# Nacos
|深度|期望|达成|时间|
|---|---|---|---|
|了解|<input type="checkbox" checked/>|<input type="checkbox" checked />|
|demo|<input type="checkbox" checked />|<input type="checkbox" checked />|
|总结|<input type="checkbox" />|<input type="checkbox" />|
---

## windows系统快速体验nacos[quick-start](https://nacos.io/zh-cn/docs/quick-start.html)
- 下载nacos(zip):https://github.com/alibaba/nacos/releases
- 账号和密码:nacos
- 单机启动`startup.cmd -m standalone`
- 下载官方demo: https://github.com/nacos-group/nacos-examples
  - spring cloud 配置nacos https://nacos.io/zh-cn/docs/quick-start-spring-cloud.html
  - 提示: nacos-naming.1.0.0 不存在时, 使用0.8.0版本替代

---

## linux搭建nacos(单机)[quick-start](https://nacos.io/zh-cn/docs/quick-start.html)
#### 下载: https://github.com/alibaba/nacos/releases
  - 方式1(推荐): 下载已编译的安装包
    - 需要依赖JDK 1.8+
    - [2.0.3_linux安装包](https://github.com/alibaba/nacos/releases/download/2.0.3/nacos-server-2.0.3.tar.gz)
  - 方式2: 下载源码,进行编译,并安装
    - 需要依赖JDK 1.8+, git, maven
#### 安装脚本
```shell
# 切换到下载目录
cd /usr/src
# 下载文件
# wget https://github.com/alibaba/nacos/releases/download/2.0.3/nacos-server-2.0.3.tar.gz
# 解压
tar -zxvf nacos-server-2.0.3.tar.gz
# 移动到安装目录
mv nacos /usr/local/

# 开启防火墙
systemctl start firewalld
# 防火墙放行
# 配置中心端口
firewall-cmd --zone=public --add-port=47848/tcp --permanent
firewall-cmd --zone=public --add-port=48848/tcp --permanent
firewall-cmd --zone=public --add-port=49848/tcp --permanent
firewall-cmd --zone=public --add-port=49849/tcp --permanent
firewall-cmd --zone=public --add-port=60094/tcp --permanent
firewall-cmd --zone=public --add-port=52612/tcp --permanent
# 防火墙刷新
firewall-cmd --reload
# 检查端口是否正常开启, 结果是yes
firewall-cmd --permanent --query-port=47848/tcp
firewall-cmd --permanent --query-port=48848/tcp
firewall-cmd --permanent --query-port=49848/tcp
firewall-cmd --permanent --query-port=49849/tcp

```

## linux搭建nacos(集群)[quick-start](https://nacos.io/zh-cn/docs/quick-start.html)
#### 网上的例子
- https://os.51cto.com/art/202103/649179.htm
#### 3台nacos服务器或者以上
- [必选]配置mysql(参考配置mysql 替换内嵌的cmdb)
- 配置集群 `vi /usr/local/nacos/conf/cluster.conf`
```cluster
172.16.4.5:48848
172.16.4.6:48848
172.16.4.9:48848
```
#### 配置nginx


#### 配置mysql 替换内嵌的cmdb[可选]
- 在mysql中创建数据库(这里我使用了nacos_config作为库名, 自行修改)
```sql
CREATE DATABASE `nacos_config` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
```
- 修改配置
```shell
# 修改配置
vi /usr/local/nacos/conf/application.properties

### 使用/ 搜索 MySQL as datasource

######## 删除原有的内容,替换以下内容 start

### If use MySQL as datasource:
spring.datasource.platform=mysql

### Count of DB:
db.num=1

### Connect URL of DB:
db.url.0=jdbc:mysql://rm-bp12g175480ut919b.mysql.rds.aliyuncs.com/css_nacos_config?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC
db.user=nacos_root
db.password=GVRpPF&ek^

######## 配置文件内容 end
```

- 在mysql中执行脚本(https://github.com/alibaba/nacos/blob/master/distribution/conf/nacos-mysql.sql)
```mysql
-- download for https://github.com/alibaba/nacos/blob/master/distribution/conf/nacos-mysql.sql
-- doc at https://nacos.io/zh-cn/docs/cluster-mode-quick-start.html

use nacos_config;
/******************************************/
/*   表名称 = config_info   */
/******************************************/
CREATE TABLE `config_info` (
                             `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
                             `data_id` varchar(255) NOT NULL COMMENT 'data_id',
                             `group_id` varchar(255) DEFAULT NULL,
                             `content` longtext NOT NULL COMMENT 'content',
                             `md5` varchar(32) DEFAULT NULL COMMENT 'md5',
                             `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                             `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
                             `src_user` text COMMENT 'source user',
                             `src_ip` varchar(50) DEFAULT NULL COMMENT 'source ip',
                             `app_name` varchar(128) DEFAULT NULL,
                             `tenant_id` varchar(128) DEFAULT '' COMMENT '租户字段',
                             `c_desc` varchar(256) DEFAULT NULL,
                             `c_use` varchar(64) DEFAULT NULL,
                             `effect` varchar(64) DEFAULT NULL,
                             `type` varchar(64) DEFAULT NULL,
                             `c_schema` text,
                             PRIMARY KEY (`id`),
                             UNIQUE KEY `uk_configinfo_datagrouptenant` (`data_id`,`group_id`,`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_info';

/******************************************/

/*   表名称 = config_info_aggr   */
/******************************************/
CREATE TABLE `config_info_aggr` (
                                  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
                                  `data_id` varchar(255) NOT NULL COMMENT 'data_id',
                                  `group_id` varchar(255) NOT NULL COMMENT 'group_id',
                                  `datum_id` varchar(255) NOT NULL COMMENT 'datum_id',
                                  `content` longtext NOT NULL COMMENT '内容',
                                  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
                                  `app_name` varchar(128) DEFAULT NULL,
                                  `tenant_id` varchar(128) DEFAULT '' COMMENT '租户字段',
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `uk_configinfoaggr_datagrouptenantdatum` (`data_id`,`group_id`,`tenant_id`,`datum_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='增加租户字段';


/******************************************/

/*   表名称 = config_info_beta   */
/******************************************/
CREATE TABLE `config_info_beta` (
                                  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
                                  `data_id` varchar(255) NOT NULL COMMENT 'data_id',
                                  `group_id` varchar(128) NOT NULL COMMENT 'group_id',
                                  `app_name` varchar(128) DEFAULT NULL COMMENT 'app_name',
                                  `content` longtext NOT NULL COMMENT 'content',
                                  `beta_ips` varchar(1024) DEFAULT NULL COMMENT 'betaIps',
                                  `md5` varchar(32) DEFAULT NULL COMMENT 'md5',
                                  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
                                  `src_user` text COMMENT 'source user',
                                  `src_ip` varchar(50) DEFAULT NULL COMMENT 'source ip',
                                  `tenant_id` varchar(128) DEFAULT '' COMMENT '租户字段',
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `uk_configinfobeta_datagrouptenant` (`data_id`,`group_id`,`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_info_beta';

/******************************************/

/*   表名称 = config_info_tag   */
/******************************************/
CREATE TABLE `config_info_tag` (
                                 `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
                                 `data_id` varchar(255) NOT NULL COMMENT 'data_id',
                                 `group_id` varchar(128) NOT NULL COMMENT 'group_id',
                                 `tenant_id` varchar(128) DEFAULT '' COMMENT 'tenant_id',
                                 `tag_id` varchar(128) NOT NULL COMMENT 'tag_id',
                                 `app_name` varchar(128) DEFAULT NULL COMMENT 'app_name',
                                 `content` longtext NOT NULL COMMENT 'content',
                                 `md5` varchar(32) DEFAULT NULL COMMENT 'md5',
                                 `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                 `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
                                 `src_user` text COMMENT 'source user',
                                 `src_ip` varchar(50) DEFAULT NULL COMMENT 'source ip',
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `uk_configinfotag_datagrouptenanttag` (`data_id`,`group_id`,`tenant_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_info_tag';

/******************************************/

/*   表名称 = config_tags_relation   */
/******************************************/
CREATE TABLE `config_tags_relation` (
                                      `id` bigint(20) NOT NULL COMMENT 'id',
                                      `tag_name` varchar(128) NOT NULL COMMENT 'tag_name',
                                      `tag_type` varchar(64) DEFAULT NULL COMMENT 'tag_type',
                                      `data_id` varchar(255) NOT NULL COMMENT 'data_id',
                                      `group_id` varchar(128) NOT NULL COMMENT 'group_id',
                                      `tenant_id` varchar(128) DEFAULT '' COMMENT 'tenant_id',
                                      `nid` bigint(20) NOT NULL AUTO_INCREMENT,
                                      PRIMARY KEY (`nid`),
                                      UNIQUE KEY `uk_configtagrelation_configidtag` (`id`,`tag_name`,`tag_type`),
                                      KEY `idx_tenant_id` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_tag_relation';

/******************************************/

/*   表名称 = group_capacity   */
/******************************************/
CREATE TABLE `group_capacity` (
                                `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
                                `group_id` varchar(128) NOT NULL DEFAULT '' COMMENT 'Group ID，空字符表示整个集群',
                                `quota` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '配额，0表示使用默认值',
                                `usage` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '使用量',
                                `max_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
                                `max_aggr_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '聚合子配置最大个数，，0表示使用默认值',
                                `max_aggr_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
                                `max_history_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最大变更历史数量',
                                `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `uk_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='集群、各Group容量信息表';

/******************************************/

/*   表名称 = his_config_info   */
/******************************************/
CREATE TABLE `his_config_info` (
                                 `id` bigint(64) unsigned NOT NULL,
                                 `nid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                                 `data_id` varchar(255) NOT NULL,
                                 `group_id` varchar(128) NOT NULL,
                                 `app_name` varchar(128) DEFAULT NULL COMMENT 'app_name',
                                 `content` longtext NOT NULL,
                                 `md5` varchar(32) DEFAULT NULL,
                                 `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                 `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                 `src_user` text,
                                 `src_ip` varchar(50) DEFAULT NULL,
                                 `op_type` char(10) DEFAULT NULL,
                                 `tenant_id` varchar(128) DEFAULT '' COMMENT '租户字段',
                                 PRIMARY KEY (`nid`),
                                 KEY `idx_gmt_create` (`gmt_create`),
                                 KEY `idx_gmt_modified` (`gmt_modified`),
                                 KEY `idx_did` (`data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='多租户改造';


/******************************************/

/*   表名称 = tenant_capacity   */
/******************************************/
CREATE TABLE `tenant_capacity` (
                                 `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
                                 `tenant_id` varchar(128) NOT NULL DEFAULT '' COMMENT 'Tenant ID',
                                 `quota` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '配额，0表示使用默认值',
                                 `usage` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '使用量',
                                 `max_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
                                 `max_aggr_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '聚合子配置最大个数',
                                 `max_aggr_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
                                 `max_history_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最大变更历史数量',
                                 `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                 `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `uk_tenant_id` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='租户容量信息表';


CREATE TABLE `tenant_info` (
                             `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
                             `kp` varchar(128) NOT NULL COMMENT 'kp',
                             `tenant_id` varchar(128) default '' COMMENT 'tenant_id',
                             `tenant_name` varchar(128) default '' COMMENT 'tenant_name',
                             `tenant_desc` varchar(256) DEFAULT NULL COMMENT 'tenant_desc',
                             `create_source` varchar(32) DEFAULT NULL COMMENT 'create_source',
                             `gmt_create` bigint(20) NOT NULL COMMENT '创建时间',
                             `gmt_modified` bigint(20) NOT NULL COMMENT '修改时间',
                             PRIMARY KEY (`id`),
                             UNIQUE KEY `uk_tenant_info_kptenantid` (`kp`,`tenant_id`),
                             KEY `idx_tenant_id` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='tenant_info';

CREATE TABLE `users` (
                       `username` varchar(50) NOT NULL PRIMARY KEY,
                       `password` varchar(500) NOT NULL,
                       `enabled` boolean NOT NULL
);

CREATE TABLE `roles` (
                       `username` varchar(50) NOT NULL,
                       `role` varchar(50) NOT NULL,
                       UNIQUE INDEX `idx_user_role` (`username` ASC, `role` ASC) USING BTREE
);

CREATE TABLE `permissions` (
                             `role` varchar(50) NOT NULL,
                             `resource` varchar(255) NOT NULL,
                             `action` varchar(8) NOT NULL,
                             UNIQUE INDEX `uk_role_permission` (`role`,`resource`,`action`) USING BTREE
);

INSERT INTO users (username, password, enabled) VALUES ('nacos', '$2a$10$EuWPZHzz32dJN7jexM34MOeYirDdFAZm2kuWj7VEOJhhZkDrxfvUu', TRUE);

INSERT INTO roles (username, role) VALUES ('nacos', 'ROLE_ADMIN');
```
#### 单机模式(与数据源无关)
```shell
# 单机模式启动(与数据源无关)
/usr/local/nacos/bin/startup.sh -m standalone warn

# 集群模式启动(内置数据源)
/usr/local/nacos/bin/startup.sh -p embedded warn

# 集群模式启动(外置数据源)
/usr/local/nacos/bin/startup.sh warn

tail -f /usr/local/nacos/logs/start.out
```

#### 登录验证
  - 登录地址: http://127.0.0.1:8848/nacos
  - 账号和密码:nacos
---

## 问题及解决办法

#### 配置mysql 替换内嵌的cmdb
  - https://blog.csdn.net/a704397849/article/details/103605583
  - https://blog.csdn.net/qq_40523572/article/details/89364340

#### 多服务配置实践
- 假如,两个大的系统(渠道系统ccs与会员系统ms),而且每个项目有若干个服务(ccs_a, ccs_b, ms_a, ms_b), 每个服务有若干个环境(dev,uat,prod)
- 前提条件, 每个服务在每一个环境中只存在一套配置 
  - 例如CCS系统的A服务对应的dev环境只保留一个配置文件 ccs_a-dev.yaml
- 配置如下:
  - 新增ccs系统dev环境对应的namespace: ccs-dev
    - Data Id: ccs_a-dev.yaml
    - Data Id: ccs_b-dev.yaml
  - 新增ccs系统uat环境对应的namespace: ccs-uat
    - Data Id: ccs_a-uat.yaml
    - Data Id: ccs_b-uat.yaml
  - 新增ms系统dev环境对应的namespace: ms-dev
    - Data Id: ms_a-dev.yaml
    - Data Id: ms_b-dev.yaml
  - 新增ms系统uat环境对应的namespace: ms-uat
    - Data Id: ms_a-uat.yaml
    - Data Id: ms_b-uat.yaml
  - 备注:
    - 为什么用namespace来区分项目而不是环境?
      - 注册中心中通过namespace隔离项目, 而且默认分组就是DEFAULT_GROUP且不支持修改
      - nacos配置管理平台以namespace来做基本单位分配权限
    - nacos默认会加载application.name.yaml, application.name-dev.yaml 三个配置文件(例如下面的配置)
      ```yaml
      spring:
        cloud:
          nacos:
            config:
              server-addr: 127.0.0.1:8848
              namespace: ccs_afs_dev
              prefix: ${spring.application.name}
              file-extension: yaml
      ```
      - url示例 [get请求]http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=mp-css-impl-dev.yaml&tenant=ccs_afs_dev&group=DEFAULT_GROUP

#### [批导配置](https://www.jianshu.com/p/11754869dfb1)
  - 仅支持单个namespace的批量导入
  - 子账号无法做导入
  - 导入文件格式如下
    - uat_properties.zip ------ 必须zip格式的压缩包
      - DEFAULT_GROUP --------- 这是文件夹,以group的名称命名
        - ccs_a-uat.yaml ------ 这是配置文件,支持多个
        - ccs_b-uat.yaml ------
      - ms-dev ---------------- 可支持多个group
        - ms_a-uat.yaml -------
        - ms_b-uat.yaml -------
  - 冲突解决(仅支持文件级别的冲突解决,明细不判断)
    - 终止导入
      - 未存在的文件会导入成功, 已存在的文件不做修改(不明与跳过的区别,可能是bug)
    - 跳过
      - 未存在的文件会导入成功, 已存在的文件不做修改
    - 覆盖
      - 未存在的文件会导入成功, 已存在的文件做覆盖

#### 开启权限功能
  - application.properties
  - nacos.core.auth.enabled=true
  - 开启后可以基于namespace分配读写权限给账号
  - 操作比较麻烦.做不同环境或者不同团队间的权限隔离即可
  - 子账号无法做导入

#### 关闭客户端的心跳日志(调整客户端的日志级别)
```yaml
logging:
  level:
    com.alibaba.nacos: warn
```

#### 关闭客户端的心跳日志(调整客户端的日志级别)
```yaml
logging:
  level:
    com.alibaba.nacos: warn
```