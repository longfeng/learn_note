# zookeeper
|深度|期望|达成|时间|
|---|---|---|---|
|了解|<input type="checkbox" checked/>|<input type="checkbox" checked/>|
|demo|<input type="checkbox" checked/>|<input type="checkbox" checked/>|
|总结|<input type="checkbox" />|<input type="checkbox" />|

## 安装(单机)
#### 下载
- 最新版(https://zookeeper.apache.org/releases.html)
- 归档(https://archive.apache.org/dist/zookeeper/)
- 这里示例:https://archive.apache.org/dist/zookeeper/zookeeper-3.4.13/zookeeper-3.4.13.tar.gz
#### 安装
```shell
# 切换到下载目录
cd /usr/src/
# 下载文件
#wget https://archive.apache.org/dist/zookeeper/zookeeper-3.4.13/zookeeper-3.4.13.tar.gz
# 解压
tar -zxvf zookeeper-3.4.13.tar.gz
# 移动到安装目录
mv zookeeper-3.4.13 /usr/local/

# 切换到安装目录
cd /usr/local/zookeeper-3.4.13
# 复制默认配置文件
cp /usr/local/zookeeper-3.4.13/conf/zoo_sample.cfg /usr/local/zookeeper-3.4.13/conf/zoo.cfg
# 编辑配置文件
vi /usr/local/zookeeper-3.4.13/conf/zoo.cfg
```
#### zookeeper主要配置
```shell
# 数据目录 dataDir: ZooKeeper 用来存储内存数据库快照的目录, 并且除非指定其它目录, 否则数据库更新的事务日志也将会存储在该目录下。
dataDir=/usr/local/zookeeper-3.4.13/data
# 日志目录 dataLogDir: 指定 ZooKeeper 事务日志的存储目录。(非控制台日志,控制台日志是执行命令时的当前目录,文件名称是zookeeper.out)
dataLogDir=/usr/local/zookeeper-3.4.13/logs

# 端口 clientPort: 服务器监听客户端连接的端口, 也即客户端尝试连接的端口, 默认值是 2181。
clientPort=2181

# 搭建集群(https://www.cnblogs.com/ysocean/p/9860529.html)
# 集群相关 server.id=ip:通信端口:选举端口
# 注意通信端口不要和客户端连接端口重复了, 否则会莫名其妙
# service的id要与myid文件中的内容一致(在dataDir目录下，自己手动创建的一个名字叫做myid的,内容存放的是id的文件)
server.1=172.16.4.5:2182:2183
server.2=172.16.4.6:2182:2183
server.3=172.16.4.9:2182:2183

```
- 配置zookeeper集群
```shell
mkdir /usr/local/zookeeper-3.4.13/data
vi data/myid


```
#### 配置zookeeperJVM内存大小
- zk默认读取`java.env`文件来进行内存分配, 若不配置,则使用jvm默认规则进行分配内存(物理内存xms1/64, xmx:1/4)
- 通过`zkEnv.sh`可以看到读取路径是`zookeeper/conf/java.env`
```shell
## 复制下面这4句到控制台, 然后ESC->:->wq
cd /usr/local/zookeeper-3.4.13/conf
vi java.env
i#!/bin/sh
export SERVER_JVMFLAGS="-Xmx512m -Xms256m"

#esc
#:wq
# 保存完成之后重启zk
/usr/local/zookeeper-3.4.13/bin/zkServer.sh stop
/usr/local/zookeeper-3.4.13/bin/zkServer.sh start
tail -f zookeeper.out

ps -ef|grep zookeeper
# 通过zk进程ID查看zk内存分配情况`jmap -heap pid`
jmap -heap 157759
```


#### 防火墙
```shell

# 开启防火墙
# systemctl start firewalld
# systemctl status firewalld
# 防火墙放行(若集群, 的话还需要开放选举端口)
firewall-cmd --zone=public --add-port=2181/tcp --permanent
firewall-cmd --zone=public --add-port=2182/tcp --permanent
firewall-cmd --zone=public --add-port=2183/tcp --permanent
# 防火墙刷新
firewall-cmd --reload
# 检查端口是否正常开启, 结果是yes
firewall-cmd --permanent --query-port=2181/tcp
firewall-cmd --permanent --query-port=2182/tcp
firewall-cmd --permanent --query-port=2183/tcp

```

#### zookeeper启动&验证&停止
```shell
# 启动zookeeper, 
# 注意, 在哪个目录启动, zookeeper的控制台日志就会在哪个目录下生成(zookeeper.out), 要修改参照(https://www.cnblogs.com/toSeeMyDream/p/8808410.html)
./bin/zkServer.sh start

#验证服务是否可用
./bin/zkCli.sh -server 127.0.0.1:2181

#停止
./bin/zkServer.sh stop

```

