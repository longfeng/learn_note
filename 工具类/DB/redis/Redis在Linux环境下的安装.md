# linux安装redis
> 选择在Linux下安装redis，现在采用虚拟机安装的centos7 进行安装的

## 下载_编译_安装
```shell
# 检查gcc是否存在
gcc -v
# 安装gcc  redis是c语言编写的
yum install -y gcc-c++
# 如果wget不存在
yum install -y wget.x86_64
#下载redis安装包,在root目录下执行
wget http://download.redis.io/releases/redis-5.0.6.tar.gz

# 解压redis安装包
tar -zxvf redis-5.0.6.tar.gz
# 进入redis目录
cd redis-5.0.6
# 编译
make

# 安装
make PREFIX=/usr/local/redis install

# 拷贝redis.conf到安装目录
cp redis.conf /usr/local/redis
cp utils/redis_init_script /etc/init.d/redis

# 进入/usr/local/redis目录
cd /usr/local/redis/

```

## 配置redis
### 创建相关的用户与目录
```shell

# 创建目录
mkdir /usr/local/redis/data
mkdir /usr/local/redis/log

useradd elasticjob
passwd 123456

chown -R  elasticjob   /usr/local/shardingsphere-elasticjob-ui
chgrp  -R  elasticjob  /usr/local/shardingsphere-elasticjob-ui

# 创建用户[可选]
#useradd redis
#passwd redis
#chown -R  redis   /usr/local/redis
#chgrp  -R  redis  /usr/local/redis

```
### 修改配置文件 `vi /usr/local/redis/redis.conf`
```shell
#vi中可通过/ 搜索对应关键词进行快速定位

# 后台启动(默认no)
daemonize yes

# 绑定端口(默认是6379)
port 6379

# 绑定IP(默认是127.0.0.1), 需要注释, 否则只能本机访问
# bind 127.0.0.1

# 指定数据存放路径(默认./)
# 一定要改,否则会用 启动命令的时当前路径 作为数据存放路径
dir /usr/local/redis/data/

# 指定日志文件(默认"")
logfile "/usr/local/redis/log/redis.log"

# 设置密码(如果配置中搜索不到requirepass, 就新增这个配置, 且与masterauth二选一)
# requirepass与masterauth的区别 https://blog.csdn.net/damanchen/article/details/100584275
requirepass Redis!1

# 是否开启持久化(默认no, 根据项目需要调整)
appendonly no

```
### 修改redis启动脚本(建议覆盖) `vi /etc/init.d/redis`
```shell
#!/bin/sh
#
# Simple Redis init.d script conceived to work on Linux systems
# as it does use of the /proc filesystem.

### BEGIN INIT INFO
# Provides:     redis_6379
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Short-Description:    Redis data structure server
# Description:          Redis data structure server. See https://redis.io
### END INIT INFO

HOME=/usr/local/redis
REDISPORT=6379
REDISPASSWD="Redis!1"

EXEC=${HOME}/bin/redis-server
CLIEXEC=${HOME}/bin/redis-cli
CONF="${HOME}/redis.conf"

PIDFILE=/var/run/redis_${REDISPORT}.pid


case "$1" in
    start)
        if [ -f $PIDFILE ]
        then
                echo "$PIDFILE exists, process is already running or crashed"
        else
                echo "Starting Redis server..."
                $EXEC $CONF
        fi
        ;;
    stop)
        if [ ! -f $PIDFILE ]
        then
                echo "$PIDFILE does not exist, process is not running"
        else
                PID=$(cat $PIDFILE)
                echo "Stopping ..."
                $CLIEXEC -p $REDISPORT -a $REDISPASSWD shutdown
                while [ -x /proc/${PID} ]
                do
                    echo "Waiting for Redis to shutdown ..."
                    sleep 1
                done
                echo "Redis stopped"
        fi
        ;;
    *)
        echo "Please use start or stop as first argument"
        ;;
esac
```


## 启动与检查
### 增加redis系统服务, 且开机自启
```shell
chkconfig --add redis
chkconfig redis on

```
### 启动redis服务
```shell
# 以redis账号启动redis服务[可选:若没创建这个账号就直接用root用户启动]
#su redis
service redis start
# 也可以用这个启动
#./bin/redis-server ./redis.conf

# 检查进程是否存在
ps aux | grep redis

```
### 验证服务是否可用
- 进入客户端: `./bin/redis-cli -p 6379`  `./bin/redis-cli -h 127.0.0.1 -p 6379`
  - 输入密码: `auth Redis!1`
  - 测试连接: `ping` 会收到 `pong`
- 处理中文乱码问题: ./bin/redis-cli --raw
### 防火墙
```shell
# 防火墙放行
firewall-cmd --zone=public --add-port=6379/tcp --permanent
# 防火墙刷新
firewall-cmd --reload
```

### 关闭redis `service redis stop`


### 常用命令
```
# 写入
set leon:1 11
set leon:2 22
set leon:3 33

# 读取
get leon:3

# 查看"leon:*"开头的key
KEYS "leon:*"

# 统计"leon:*"开头的key数量
??

# (在shell中执行)删除"leon:*"开头的key
redis-cli -p 6380 -a GJredis#7383 KEYS "leon:*" | xargs redis-cli -p 6380 -a GJredis#7383 DEL

```

## 工具 
