# Mysql停机状态下进行主从同步(假设操作过程中都没有任何数据写入)
> 这里假设你已经清楚在停机状态下的主从同步配置了,请参阅[停机状态下主从同步](Mysql停机状态下进行主从同步.md)
> 
> 
## 前提条件
1. 主库必须已经开启了binlog,
2. 这里假设你已经清楚在停机状态下的主从同步配置了,请参阅[停机状态下主从同步](Mysql停机状态下进行主从同步.md)

## 思路
1. 通过xtrabackup备份主库
2. 将xtrabackup备份的文件恢复到从库
3. 通过从库得到备份点的binlog状态
4. 在主库中配置主从同步

## 步骤拆解


### 完成










方案一:XtraBackup
https://segmentfault.com/a/1190000003063874
https://blog.51cto.com/shineforever/1769084
https://database.51cto.com/art/202004/614334.htm
https://blog.csdn.net/weixin_33674976/article/details/85711496

XtraBackup 简介
https://baijiahao.baidu.com/s?id=1642939159275282706&wfr=spider&for=pc
XtraBackup 使用手册
https://my.oschina.net/deepblue/blog/529374

方案二: 纯手工
https://blog.csdn.net/mengruobaobao/article/details/105710954
https://blog.csdn.net/m0_37923316/article/details/108260673

mysql 下载
https://downloads.mysql.com/archives/community/
安装mysql rpm
https://blog.csdn.net/weixin_44198965/article/details/91891985
安装mysql tar
https://www.jianshu.com/p/276d59cbc529
mv mysql-5.7.29-el7-x86_64.tar.gz /usr/local/src/
cd /usr/local/src/
tar -zxvf mysql-5.7.29-el7-x86_64.tar.gz
mv mysql-5.7.29-el7-x86_64 /usr/local/mysql

groupadd mysql
useradd -r -g mysql mysql
chown -R mysql:mysql /usr/local/mysql
chmod -R 755 /usr/local/mysql

mkdir /home/mysql_data

#####
cd /usr/local/mysql/bin
./mysqld --initialize --user=mysql --datadir=/home/mysql_data --basedir=/usr/local/mysql

A temporary password is generated for root@localhost: XgBOjrVrg3;(

vim /etc/my.cnf

/usr/local/mysql/support-files/mysql.server start

#Starting MySQL.Logging to '/home/mysql_data/localhost.localdomain.err'.

#添加软连接，并重启mysql服务
ln -s /usr/local/mysql/support-files/mysql.server /etc/init.d/mysql
ln -s /usr/local/mysql/bin/mysql /usr/bin/mysql
service mysql restart

#登录mysql，修改密码(密码为步骤5生成的临时密码)
mysql -u root -p
set password for root@localhost = password('fong');
#验证密码是否修改正确
exit;
mysql -u root -p
#
开放远程连接
use mysql;
update user set user.Host='%' where user.User='root';
flush privileges;

#设置开机自动启动
1、将服务文件拷贝到init.d下，并重命名为mysql
[root@localhost /]# cp /usr/local/mysql/support-files/mysql.server /etc/init.d/mysqld
2、赋予可执行权限
[root@localhost /]# chmod +x /etc/init.d/mysqld
3、添加服务
[root@localhost /]# chkconfig --add mysqld
4、显示服务列表
[root@localhost /]# chkconfig --list

#开放防火墙端口
firewall-cmd --zone=public --add-port=3306/tcp --permanent
firewall-cmd --reload


#主从配置
https://blog.51cto.com/lizhenliang/1290431



#create user 'sync'@'192.168.10.13' identified by 'fong';
GRANT REPLICATION SLAVE ON *.* to 'sync'@'%' identified by 'fong';


flush privileges;


show master status;

change master to master_host='192.168.193.12', master_user='sync', master_password='fong', master_log_file='mysql-bin.000001', master_log_pos=1461227;

start slave;

show slave status \G




mysqld  mysqld_multi  mysqld_safe 一堆堆的

https://cloud.tencent.com/developer/article/1574085

#mysql同一个配置文件的加载顺序
1.从上至下
2.遵循分组原则,比如分组[mysqld_safe][mysqld]
3.mysql的参数分组有
[client] 客户端默认设置内容
[mysql] 使用mysql命令登录mysql数据库时的默认设置
[mysqld] 数据库本身的默认设置
4.加参数的时候一定要注意顺序和所在的分组
- MySQL配置文件的格式为集中式，通常会分成好几部分，可以为多个程序提供配置，如[client]、[mysqld]、[mysql]等等。MySQL程序通常是读取与它同名的分段部分。例如服务器mysqld通常读取[mysqld]分段下的相关配置项。如果`配置项`位置不正确，该配置是不会生效的。




