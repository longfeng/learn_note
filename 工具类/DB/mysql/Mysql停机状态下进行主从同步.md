# Mysql停机状态下进行主从同步(假设操作过程中都没有任何数据写入)
> [参考1](https://blog.51cto.com/lizhenliang/1290431)

## 假设同步back_test库, 请先在主从库中将数据库创建好
```shell
CREATE DATABASE `back_test` CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_general_ci';
CREATE TABLE `back_test`.`user`  (
  `id` bigint(16) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NULL,
  `age` int(11) NULL,
  `created` timestamp(3) NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated` timestamp(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
);
```

## 主要步骤
### 主库 开启binlog
- `vim /etc/my.cnf` 文件,在`mysqld`分组下加入以下内容
```shell

#mysql服务id,相同集群(包含主从)每个服务必须唯一,通常使用ip后几位删除点号
#默认值0,最大值4294967295
server_id = 193012

#把relay-log里的日志内容再记录到slave本地的binlog里,5.6(含)之前做主从必须开启
#log_slave_updates=1

#binlog日志的前缀,默认存放在%datadir%目录下
log_bin=binlog

#不需要记录binlog的数据库(实例)与bin_do_db互斥
binlog_ignore_db=information_schema
binlog_ignore_db=mysql

```
- 手动备份两个数据库
- 重启生效 `service mysql restart`

### 主库 增加slave账号并授权
```shell
# 登录mysql
mysql -u root -pfong
## 以下在mysql中执行
  # 在mysql中操作 GRANT REPLICATION SLAVE ON *.* to `slaveCount`@`ip` identified by `password`; 
  GRANT REPLICATION SLAVE ON *.* to 'sync'@'%' identified by 'fong'; 
  # 切换到需要同步的数据库
  use back_test;
  # 显示binlog状态
  show master status;
  # 记录File与Position
```

### 从库 配置同步信息
- `vim /etc/my.cnf` 文件,在`mysqld`分组下加入以下内容
```shell
#mysql服务id,相同集群(包含主从)每个服务必须唯一,通常使用ip后几位删除点号
#默认值0,最大值4294967295
server_id = 193013

# 不配置则表示全部同步(需要主库开启binlog
#replicate_do_db=dbname

# 跳过同步过程中的错误
slave-skip-errors = all

```
- 重启生效 `service mysql restart`

### 将主库中要同步的库表结构复制到从库中

### 从库 开启同步
```shell
# 登录mysql
mysql -u root -pfong

## 以下在mysql中执行
  # master_host表示主库ip
  # master_user表示用来访问主库的账号
  # master_password表示密码
  # 以下两个参数在主库中通过show master status 获得
  # master_log_file表示从主库的哪个binlog开始同步
  # master_log_pos表示从主库master_log_file中的哪个Position开始同步
  change master to master_host='192.168.193.12', master_user='sync', master_password='fong', master_log_file='binlog.000001', master_log_pos=12758351;
  
  #开启同步
  start slave;
  
  #检查同步状态,不要带分号
  # Slave_IO_Running与Slave_SQL_Running必须是YES
  # 如果不对的话,就排查情况,注意这个语句是在从库执行的,请优先排查是否搞错
  show slave status \G
```


### 完成

## 问题
- show master status;命令 Binlog_Ignore_DB 列没有自己要备份的数据库?
  - 没发现什么问题,可以正常使用
  
- 总是有几条数据未同步过来如何解决?
  

- 如果同步完成后从库又将数据删除了,这时候是否可以通过重新同步来实现数据恢复?
  - slave中有保存已读取的位点信息, `Read_Master_Log_Pos`, 应该可以在slave中通过重置位点来恢复数据[未验证]
  - 我尝试过 重新修改change的位点到之前,但是这行不通.
  



