# Windows_安装Mysql5.7

### 步骤
1. 下载: https://dev.mysql.com/downloads/mysql/5.7.html#downloads
    - ZIP Archive (mysql-5.7.35-winx64.zip)
    

2. 解压到安装的目录, 例如: `D:\work\tools\mysql-5.7.35-winx64`
   

3. 在安装目录下增加`my.ini`文件, 指定程序目录和data目录,注意编码格式为`ANSI`
```init
[mysqld]
#无需校验用户和密码(不知道怎么用)
#skip-grant-tables=1
port = 3306
basedir=D:/work/tools/mysql-5.7.35-winx64
datadir=E:/tmp/mysql/data
max_connections=200
character-set-server=utf8
default-storage-engine=INNODB
sql_mode=NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES
[mysql]
default-character-set=utf8 
```


4. 配置环境变量
- 新增`MYSQL_HOME`, 值为:`D:\work\tools\mysql-5.7.35-winx64`
- 追加`path`路径, 值为:`D:\work\tools\mysql-5.7.35-winx64\bin`


5. 安装mysql服务
- 以管理员身份运行cmd, 然后执行下面的命令
```cmd
#切换磁盘
D:
#进入到程序目录
cd D:\work\tools\mysql-5.7.35-winx64\bin
# 安装mysql服务 默认的服务名是mysql
mysqld -install
```
- 安装成功会提示:`Service successfully installed.`
- 若报错`MSVCR120.dll缺失`, 则需要安装` Visual C++ Redistributable Packages for Visual Studio 2013` [vc2013下载](https://www.microsoft.com/zh-CN/download/details.aspx?id=40784)
- 若报错`Install/Remove of the Service Denied!`,则需要以管理员省份运行cmd


6. 创建用户
```cmd
# 生成一个无密码的root用户
mysqld --initialize-insecure
```

7. 启动mysql服务
```cmd
net start mysql
```
- MySQL 服务正在启动 . MySQL 服务无法启动。 服务没有报告任何错误。
    - 通过`mysqld --console`命令查看服务启动过程中报了什么错


8. 更改root密码, 
- 先登录在cmd中登录`mysql -uroot -p`
- 再执行sql`update mysql.user set authentication_string=password('root') where user='root' and Host ='localhost';`
- 重启mysql服务
- 验证`mysql -uroot -proot`

9. 备注

哦了