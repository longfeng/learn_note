### Navicat Blob 字段 乱码
- 获取方式`select cast(字段名 as char) from 表名`
- 更新或者插入使用`insert`与`update`即可
  - update table set a=`xxx xx` where id=`;

---
### 如何sql语句查看当前版本号
`select version();`

---
### 如果不配置my.cnf文件mysql能用吗?

---
### mysqld是什么?
是mysql的守护进程

---

### 别人安装的mysql怎么知道怎么启动
systemctl restart mysqld

### 如何在linux上执行sql文件? 可以批量
```shell
# 先安装mysql客户端
yum install mysql
# 通过mysql客户端登录
mysql -h127.0.0.1 -uroot -p123456
# 通过source执行脚本
source /data/sql/test.sql
```
- source如何批量执行脚本?
  - 通过嵌套来实现, 例如下面两个文件
    - test.sql
    ```
    INSERT INTO table (User) VALUES ('a');
    source /data/sql/1.sql;
    ```
    - 1.sql
    ```
    INSERT INTO table (User) VALUES ('b');
    ```

### mysql 新增用户
```shell
mysql -uroot -p

#添加用户
### 创建用户,并指定用户可以通过任意ip登录数据库
create user 'leon'@'%' identified by '123456';
### 创建用户,并指定用户仅允许本机(127.0.0.1)登录数据库
create user 'leon'@localhost identified by '123456';

#用户分配权限
### 允许leon从任意服务器访问nacos_config数据库, 仅支持INSERT,SELECT,UPDATE,DELETE,CREATE,INDEX,ALTER语句权限
grant INSERT,SELECT,UPDATE,DELETE,CREATE,INDEX,ALTER on nacos_config.* to 'leon'@'%';
### 允许leon从任意服务器访问nacos_config数据库, 并且支持所有语句权限
grant all privileges on nacos_config.* to 'leon'@'%';
### 允许leon从本机(127.0.0.1)服务器访问nacos_config数据库, 并且支持所有语句权限
grant all privileges on nacos_config.* to 'leon'@localhost;
#### all privileges 有哪些权限? https://www.cnblogs.com/abclife/p/5789808.html

# 刷新权限(用户侧不需要重新登录)
# flush privileges;
# 查看权限
# show grants for 'leon'@'%';
# 移除所有授权
# revoke all privileges on *.* from 'leon'@'%';


```
### mysql 更改密码
```shell
mysql -uroot -p
# 进入mysql之后
use mysql
# 更改密码
update user set authentication_string=password('123456') where user='root';
# 刷新 权限
flush privileges;

```

### 阿里云rds如何做ssh代理登录
#### 场景: 阿里云rds不想开放外网, 同时希望本地通过堡垒机ssh过去访问数据库
  - 场景元素:
    - A: 本机
    - B: 阿里云服务器
      - 可访问阿里云的数据库, 在阿里云内网
    - C: 阿里云RDS
      - 通常会有内网和外网两个地址
  - 解决方案:
    - 修改A的数据库连接配置 
      - 使用C提供的内网访问地址进行数据库访问
      - 勾选SSH, 填写B的ip以及用户名密码

### mysql驱动 mysql.jdbc与mysql.cj.jdbc区别 
- https://www.cnblogs.com/rainbow70626/p/14466303.html
- mysql.cj.jdbc是mysql.jdbc替代版本.