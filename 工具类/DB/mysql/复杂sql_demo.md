### 获取分组中时间最小的记录
```sql
select cwb.id
from css_work_booking cwb
where exists(
  select WORK_ID, CREATION_DATE
  from (
           select WORK_ID, min(CREATION_DATE) CREATION_DATE
           from css_work_booking
           group by WORK_ID
       ) t where T.WORK_ID = cwb.WORK_ID and T.CREATION_DATE = cwb.CREATION_DATE
);
```

### 从另外一张表得到数据insert

insert into table_name_new(column1,column2…) select column1,column2… from table_name_old


### 时间转换函数
- 时间或日期转时间戳
    - UNIX_TIMESTAMP(cs.CREATION_DATE)