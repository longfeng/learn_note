# Linux CentOS7 安装 Mysql 5.7(两种方式)

---
## mysql版本选择与下载
- 主要是选择MySQL Community Server
- 版本描述: 待补充
- ![操作示意图](../../source/mysql_download.png)

---
## 方案1: tar方式安装mysql
> 参考地址:https://www.jianshu.com/p/276d59cbc529
### 安装前准备

- 卸载mariadb[可选], 查看是否安装mariadb`rpm -qa | grep mariadb`
>[如何卸载mariabdb](https://blog.csdn.net/lilygg/article/details/100123335)
> 
>[为什么要卸载mariabdb](https://blog.csdn.net/u012026446/article/details/79397953)
```shell
#卸载mariadb相关的软件
yum remove -y `rpm -aq mariadb*`
#删除数据目录和配置文件[可选,centos7.9 卸载完成后没有这两个东西了]
rm -rf /etc/my.cnf
rm -rf /var/lib/mysql

```

- 卸载mysql[可选]
```shell
#检查是否已经安装过mysql，执行命令
rpm -qa | grep mysql
# 执行删除命令
rpm -e --nodeps mysql-libs-5.1.73-5.el6_6.x86_64
#再次执行查询命令，查看是否删除
rpm -qa | grep mysql

#删除相关目录或文件
whereis mysql
find / -name mysql
rm -rf /usr/bin/mysql /usr/include/mysql /data/mysql /data/mysql/mysql

#验证是否删除完毕
whereis mysql
find / -name mysql
```

### 下载并解压文件到`/usr/local/mysql`目录
```shell
# 可选择从网上下载,或者本地上传
cd /usr/local/src/
# 下载[可选]
#wget https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-5.7.29-el7-x86_64.tar.gz
# 解压文件到/usr/local/mysql目录
tar -zxvf mysql-5.7.29-el7-x86_64.tar.gz
# 将文件移动到/usr/local/mysql目录
mv mysql-5.7.29-el7-x86_64 /usr/local/mysql


```

### 创建相关目录与mysql用户和组, 并授权
```shell
#安装前先创建mysql数据目录,需要与之后的my.cnf中配置的dataDir一致
mkdir /usr/local/mysql/data
mkdir /usr/local/mysql/logs
touch /usr/local/mysql/logs/mysql_error.log
# 仅供系统使用,不需要设置密码
groupadd mysql
useradd -r -g mysql mysql
chown -R mysql:mysql /usr/local/mysql
chmod -R 755 /usr/local/mysql


```


### 执行安装
```shell
# 
cd /usr/local/mysql/bin
#执行安装, 
#注意安装完成后,会临时生成root账号以及密码,请先记住这个密码,类似:A temporary password is generated for root@localhost: XgB;OjrVrg3s.
./mysqld --initialize --user=mysql --datadir=/usr/local/mysql/data --basedir=/usr/local/mysql

```


### 创建my.cnf文件 `vim /etc/my.cnf`
```mysql.cnf
[client]
#设置服务器默认编码格式(为了支持颜文字)
default-character-set=utf8mb4
[mysqld]
#设置服务器默认编码格式(为了支持颜文字)
character_set_server=utf8mb4
# 服务端口
port=3306
#应用目录
basedir = /usr/local/mysql
#数据目录
datadir = /usr/local/mysql/data
#错误日志路径
log_error = /usr/local/mysql/logs/mysql_error.log
#是否将每个表的数据单独存储，1表示单独存储；0表示关闭独立表空间，可以通过查看数据目录，查看文件结构的区别
innodb_file_per_table=1
#是否区分大小写，1表示存储时表名为小写，操作时不区分大小写；0表示区分大小写；不能动态设置，修改后，必须重启才能生效
lower_case_table_names=1

```

### 设置防火墙,以及开机启动
```shell
# 防火墙放行
firewall-cmd --zone=public --add-port=3306/tcp --permanent
# 防火墙刷新
firewall-cmd --reload

# 将mysql作为服
# 添加软连接(快捷方式),这样就可以使用service mysql start|stop 来启动mysql了
ln -s /usr/local/mysql/support-files/mysql.server /etc/init.d/mysql
# /use/bin目录在系统环境变量的覆盖下,这样就可以直接再shell中运行 mysql -uroot 命令了
ln -s /usr/local/mysql/bin/mysql /usr/bin/mysql

#设置开机启动
chkconfig --add mysqld
# 显示服务列表
# chkconfig --list
```

### 重启完成后 初始化账号信息
```shell
# 启动mysql
/usr/local/mysql/support-files/mysql.server start
# 登录
mysql -u root -p
# mysql中操作 
  # 设置密码
  set password for root@localhost = password('fong');
  # 使用mysql数据库
  use mysql;
  # 开放远程连接
  update user set user.Host='%' where user.User='root';
  # 刷新mysql配置
  flush privileges;
  # 退出验证
  exit;
  
# 重新登录验证
mysql -u root -pfong
exit;

# 重启mysql
# service mysql restart
```

---
## 方案2: rpm方式安装mysql(尚未验证)
> 参考地址: https://blog.csdn.net/weixin_44198965/article/details/91891985


## 问题备注
### 问题启动mysql成功了,但是使用mysql命令报错: 
- mysql: error while loading shared libraries: libncurses.so.5: cannot open shared object file: No such file or directory
- 解答: 执行以下命令安装依赖 `sudo yum install libncurses*`
### 执行开机自启命令失败
```shell
[root@VM-8-8-centos ~]# chkconfig --add mysqld
error reading information on service mysqld: No such file or directory
```
- 解决: 通过`systemctl enable mysql`来重启,