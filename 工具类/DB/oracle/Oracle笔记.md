# oracle笔记
## 版本
- [版本时间线](https://baijiahao.baidu.com/s?id=1665191256782813922&wfr=spider&for=pc)
- 关键版本 10G/11G/19C
- 版本区别[oracle 10g 11g 12c区别](https://www.cnblogs.com/xiaojianblogs/p/7446487.html)
- 版本区别[oracle 10g 11g 12c区别](https://www.zhihu.com/question/23095276)
- oracle版本号查看 `select * from v$version;`
- oracle版本对应关系
	- 12.2.0.1 -> 12C
	- 12.2.0.2 -> 18C
	- 12.2.0.3 -> 19C
	- 20.1.0   -> 20C
- 如何下载旧版本, 可能需要翻墙后打开(https://blog.csdn.net/wupanlong_1992/article/details/128171333)
---

## 环境搭建
- windows
- linux
- 关键概念
	- [Oracle “模式”和“用户”概念详解](https://blog.csdn.net/u010051036/article/details/108745280)
		- 在创建用户的时候系统会默认分配一个同名的模式
		- 模式(schema), 是一个逻辑容器, 用于管理某个用户拥有的所有对象
			- 模式不能脱离用户单独存在
	  - 对象指的是如下
      - 表（Table）, 分区（Partition）, 视图（View）, 索引（Indexe）, 包（Package）,存储过程（Procedure）,函数（Function）,触发起（Trigger）,类型（Type）, 序列（Sequence）, 同义（Synonym）等数据库对象。
		- 可以通过权限的分配,让一个用户访问其他用户(所属)模式下的对象
	  - SYS 和 SYSTEM 模式
			- 当我们安装 Oracle 的时候，系统自动帮我们创建了 SYS 和 SYSTEM 模式，它们拥有最高权限，用来管理数据库，SYSTEM 比 SYS 提供了更多的表。
--- 

## 关于用户
### oracle中的sys和system用户有什么区别
- 在Oracle数据库中，sys和system是两个重要的预定义用户，它们之间有以下区别：
	- SYS用户是Oracle数据库的超级用户，具有最高权限。它是数据库的所有者，可以对数据库进行全面的管理和配置。SYS用户可以执行所有的数据库操作，包括创建和删除数据库对象、管理用户、备份和恢复数据库等。
	- SYSTEM用户也是一个特殊的用户，但相对于SYS用户来说，它的权限要低一些。SYSTEM用户主要用于管理数据库的日常运维任务，例如创建用户、授权、执行备份和恢复操作等。SYSTEM用户通常用于管理数据库对象和配置，但没有SYS用户的全部权限。
	- SYS用户是Oracle数据库的内部用户，用于执行数据库内部操作和管理。它拥有更高的权限，并且可以访问和修改数据库的内部数据字典。在某些情况下，只有SYS用户才能执行某些特定的操作，例如修改数据库参数、执行高级的系统级别任务等。
	- SYSTEM用户是一个普通的数据库用户，可以像其他用户一样创建和管理数据库对象。它通常用于管理数据库的常规操作，例如创建表、索引、视图等。SYSTEM用户的权限范围较小，不具备修改数据库内部结构和配置的特权。
- 总结来说，SYS用户是Oracle数据库的超级用户，具有最高权限，用于执行数据库的内部操作和管理。而SYSTEM用户是一个普通的数据库用户，用于管理数据库的日常运维任务和对象管理。

## 函数 
### TRUNC（number,num_digits）[参考](https://www.cnblogs.com/zhangxiaoxia/p/10270840.html)
- 对于数字类型
  - 参数:
		- Number 需要截尾取整的数字。
	  - Num_digits 用于指定取整精度的数字。Num_digits 的默认值为 0。
	  - TRUNC()函数截取时不进行四舍五入
  - 示例:
    - select trunc(123.458) from dual --123
    - select trunc(123.458,1) from dual --123.4
- 对于日期类型
	- select trunc(sysdate) from dual --2013-01-06 今天的日期为2013-01-06
	- select trunc(sysdate, 'mm') from dual --2013-01-01 返回当月第一天.
	- select trunc(sysdate,'yy') from dual --2013-01-01 返回当年第一天
	- select trunc(sysdate,'dd') from dual --2013-01-06 返回当前年月日
	- select trunc(sysdate,'yyyy') from dual --2013-01-01 返回当年第一天

### 字符串转数字 [参考](https://www.php.cn/oracle/489135.html)
- 示例: select to_number('88877') from dual;

	
### to_date [参考](https://blog.csdn.net/weixin_42387852/article/details/80760071)
```sql
select to_date('2022-08-31','yyyy-MM-dd')
from dual;
select to_date('2022-11-11 01:02:03', 'yyyy-mm-dd hh24:mi:ss')
from dual;

select to_char(sysdate,'yyyy-MM-dd HH24:mi:ss') from dual;//mi是分钟

-- 获取当前时间前10分钟
select to_char(sysdate - 10/1440,'yyyy-MM-dd HH24:mi:ss') from dual;

select * 
from table
where CREATION_DATE >= to_date('2022-08-01','yyyy-MM-dd') -- 大于等于最后一天
and  CREATION_DATE < to_date('2022-08-31','yyyy-MM-dd')+1 -- 小于等于最后一天
```

### 将时间戳转换为时间字符串
- 毫秒
select TO_CHAR(TO_TIMESTAMP('19700101', 'YYYYMMDD') + NUMTODSINTERVAL((1698203265543+28800000)/1000, 'SECOND'), 'YYYY-MM-DD HH24:MI:SS') as data_str from dual
- 秒
select TO_CHAR(TO_TIMESTAMP('19700101', 'YYYYMMDD') + NUMTODSINTERVAL((1698203265+28800000)/1000, 'SECOND'), 'YYYY-MM-DD HH24:MI:SS') as data_str from dual
	
### decode(条件,值1,返回值1,值2,返回值2,…值n,返回值n, 缺省值) [参考](https://blog.csdn.net/sdut406/article/details/82795585)
	-  decode(t.sex, '1', '男生', '2', '女生', '其他') as sex
---

## 执行计划
### oracle 怎么查看执行计划
1. 执行`explain plan for select * from dual;`, 此时控制台还没有结果, 结果需要用另外一条sql来展示
2. 执行`SELECT * FROM TABLE(DBMS_XPLAN.DISPLAY);`
---

## 如何新建dbLink
- sql
```sql
drop database link zlsc;
CREATE DATABASE LINK zlsc
    CONNECT TO gccsdb IDENTIFIED BY "123#ccs#gj"
    USING '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=10.230.20.118)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=gujingcloud)))';
```
---

### 如何创建用户,并赋予部分权限
```
-- 创建用户
CREATE USER zlhz_link_user IDENTIFIED BY password;
-- 给用户授权
GRANT SELECT, UPDATE, DELETE ON table1 TO zlhz_link_user;
GRANT SELECT, UPDATE, DELETE ON table2 TO zlhz_link_user;
```
---

## 关于定时任务(有两种job对象)
### [推荐]DBMS_SCHEDULER (支持秒, 安全粒度细, 提供了完整的历史记录)
- 查询有哪些定时任务
```sql
-- 查询所有定时任务(包含通过DBMS_JOB.SUBMIT创建的job)
SELECT job_name, owner, job_type, job_action, start_date, repeat_interval, enabled, state FROM dba_scheduler_jobs;
```
- 查看定时任务的执行记录
```sql
SELECT log_date, status, error#, additional_info FROM dba_scheduler_job_run_details WHERE job_name = 'ARCHIVE_CCS_INV_CURRENT_INV_JOB' ORDER BY log_date DESC;
````
- 怎么创建定时任务(job_action表示job执行的存储过程名称)
```sql
BEGIN
    DBMS_SCHEDULER.CREATE_JOB (
            job_name           => 'SYNC_FOR_MINUTELY_JOB',
            job_type           => 'STORED_PROCEDURE',
            job_action         => 'ARCHIVE_CCS_INV_CURRENT_INV',
            start_date         => SYSTIMESTAMP,
            repeat_interval    => 'FREQ=MONTHLY;BYMONTHDAY=1;BYHOUR=0;BYMINUTE=0;BYSECOND=0',
            enabled            => TRUE,
            comments           => 'Monthly job to archive current inventory');
END;
/
```
- 怎么修改定时任务
```sql
BEGIN
    DBMS_SCHEDULER.SET_ATTRIBUTE(
            name               => 'SYNC_FOR_MINUTELY_JOB',
            attribute          => 'repeat_interval',
            value              => 'FREQ=MONTHLY;BYMONTHDAY=27;BYHOUR=17;BYMINUTE=08;BYSECOND=0');
END;
/
```
- 删除定时任务(请注意，删除作业将删除作业的定义以及作业运行期间生成的所有日志。如果您需要保留作业日志，请在删除作业之前将日志导出到其他位置。)
```sql
BEGIN
  DBMS_SCHEDULER.DROP_JOB(
    job_name => 'SYNC_FOR_MINUTELY_JOB'
  );
END;
/
```
- 停止定时任务
```sql
BEGIN
	DBMS_SCHEDULER.STOP_JOB(
			job_name        => 'SYNC_FOR_MINUTELY_JOB',
			force          => FALSE
		);
END;
/
```
- 查看正在执行中的定时任务 `SELECT * FROM DBA_SCHEDULER_RUNNING_JOBS WHERE JOB_NAME = 'SYNC_FOR_MINUTELY_JOB';`
	- OWNER：任务所属用户
	- `JOB_NAME`：任务名称
	- JOB_SUBNAME：任务子名称
	- JOB_STYLE：任务类型
	- DETACHED：是否为分离式任务
	- SESSION_ID：任务所在会话ID
	- SLAVE_PROCESS_ID：任务从进程ID
	- SLAVE_OS_PROCESS_ID：任务从操作系统进程ID
	- RUNNING_INSTANCE：任务运行实例
	- RESOURCE_CONSUMER_GROUP：任务所在资源消耗组
	- `ELAPSED_TIME`：任务执行时间(截止当前时间耗时)
	- CPU_USED：任务使用的CPU时间
	- DESTINATION_OWNER：任务目标所有者
	- DESTINATION：任务目标
	- CREDENTIAL_OWNER：任务凭证所有者
	- CREDENTIAL_NAME：任务凭证名称
	- LOG_ID：任务执行日志ID

### DBMS_JOB (支持到分钟, 只是一个简单的作业调度器)
- 有哪些定时任务
```sql
-- 查询所有通过DBMS_JOB.SUBMIT创建的job
SELECT * FROM dba_jobs;
-- 查询所有通过DBMS_JOB.SUBMIT创建的job(根据当前用户查询权限过滤)
SELECT * FROM all_jobs;
```
- ?修改定时任务时间, 将job任务编号为151的定时任务执行时间修改为每天凌晨01:10:00执行
```sql
-- 将任务的信息查出来, 主要是what, 和next_date
SELECT job, next_date, next_sec, broken, interval, what
FROM dba_jobs
WHERE job = 151;
-- 修改定时任务
BEGIN
	DBMS_JOB.CHANGE(
			job => 1,
			what => 'your_job_procedure;',
			next_date => TO_DATE('2023-09-26 14:00:00', 'YYYY-MM-DD HH24:MI:SS'),
			interval => 'SYSDATE +20/(60*24)');
	COMMIT;
END;
/
-- 修改完成后查看定时任务的next_date, next_sec是否相符
select job, what, LAST_DATE, LAST_SEC, next_date, NEXT_SEC, interval, BROKEN
from all_jobs
where job in (28, 49, 151);
-- 注意:
-- next_date 一定要包含日期, 因为默认日期是当天, 这样可能会导致任务永远不会被执行
-- 关于表达式
-- SYSDATE +20/(60*24) 		-- 每20分钟执行一次
-- TRUNC(SYSDATE+1) + 6/24-- 每天早上6点执行一次
```
- ?将job任务编号为151的定时任务删除
```sql
DBMS_JOB.REMOVE(job => 151);
```

---

## 关于触发器
### 创建触发器
```sql
-- MDP_SEC_SETS_OF_BOOKS新增trigger, 批量执行需要用idea,navicat报错,不知道为啥
create trigger UPDATE_MDP_SEC_SETS_OF_BOOKS_LAST_DATE
    before update
    on MDP_SEC_SETS_OF_BOOKS
    for each row
BEGIN
    :NEW.LAST_UPDATE_DATE := SYSDATE;
END;
/
```
### 删除触发器
```sql
DROP TRIGGER UPDATE_CCS_CCS_BASE_AGENT_LAST_DATE;
```
### 查看所有触发器
```sql
SELECT trigger_name, table_name, trigger_type, triggering_event
FROM user_triggers;
```

## 序列
### 创建序列
```
CREATE SEQUENCE SQ_CCS_FIN_BUD_CASH_PAYMENT			MINVALUE 1	MAXVALUE 999999999999	INCREMENT BY 1  START WITH 1 CACHE 20;
```
### 删除序列
```
DROP SEQUENCE SQ_CCS_FIN_BUD_CASH_PAYMENT;
```
### 修改序列
- 如果只是缩小范围
```
ALTER SEQUENCE SQ_CCS_FIN_BUD_CASH_PAYMENT		MINVALUE 1			MAXVALUE 100000000	INCREMENT BY 1 ;
```
- 如果不是缩小范围,则删除后再创建
```
DROP SEQUENCE SQ_CCS_FIN_BUD_CASH_PAYMENT;
CREATE SEQUENCE SQ_CCS_FIN_BUD_CASH_PAYMENT			MINVALUE 300000001	MAXVALUE 400000000	INCREMENT BY 1  START WITH 300000001 CACHE 20;
```
####
```
SELECT seq.SEQUENCE_NAME, seq.LAST_NUMBER, seq.MIN_VALUE, seq.MAX_VALUE, seq.INCREMENT_BY, seq.CACHE_SIZE, seq.*
FROM all_sequences seq
```
- SEQUENCE_OWNER：序列所属的用户。
- SEQUENCE_NAME：序列的名称。
- MIN_VALUE：序列的最小值。
- MAX_VALUE：序列的最大值。
- INCREMENT_BY：序列的增量值。
- CYCLE_FLAG：指示序列是否循环的标志。'Y'表示循环，'N'表示不循环。
- ORDER_FLAG：指示序列是否按顺序生成的标志。'Y'表示按顺序，'N'表示无序。
- CACHE_SIZE：序列的缓存大小。
- LAST_NUMBER：序列的当前值。
- PARTITION_COUNT：序列的分区数。
- SESSION_FLAG：指示序列是否与会话相关的标志。'Y'表示是，'N'表示否。
- KEEP_VALUE：序列的保留值。
- KEEP_FLAG：指示序列是否保留的标志。'Y'表示保留，'N'表示不保留。
- INCREMENT_FLAG：指示序列是否为增量的标志。'Y'表示增量，'N'表示不增量。
---


select
vsession.sid      	SessionId
, vsession.username Oracle用户名
, vsession.OSUSER   操作系统用户名
, vsession.serial#	session序列
, vprocess.spid     操作系统ID
, vsql.sql_FULLtext 正在执行的SQL
, vsession.machine  计算机名
, vsession.SQL_EXEC_START	执行时间
, vsession.PREV_EXEC_START 预执行时间
, vsql.SQL_ID				sqlID
, vsession.status		会话状态
, vsession.STATE		资源状态
, vsession.WAIT_CLASS	等待类型
, vsession.WAIT_TIME	等待时间
, vwait.EVENT			等待事件
, vwait.SECONDS_IN_WAIT	等待时间秒
from V$PROCESS vprocess
JOIN V$SESSION vsession on vprocess.ADDR = vsession.PADDR
JOIN V$SQLAREA vsql on vsession.SQL_HASH_VALUE = vsql.HASH_VALUE
JOIN V$SESSION_WAIT vwait on vwait.SID = vsession.SID
where vsession.status = 'ACTIVE'

## 性能分析相关
### V$SESSION 会话视图, 
- SPID: 		操作系统进程ID, 进程无法在oracle终止时在linux系统通过此进程ID终止?
- SID： 		会话的唯一标识符（Session ID）。
- SERIAL#： 会话的序列号，用于唯一标识会话。
- USERNAME：会话所属的(Oracle)用户名。
- OSUSER：  会话所属的(操作系统)用户名。
- MACHINE： 会话所在的客户端机器名。
- PROGRAM： 会话所使用的客户端程序名。
- SQL_ID：  当前正在执行的SQL语句的标识符。
- SQL_EXEC_ID：			当前正在执行的SQL语句的执行ID。
- SQL_CHILD_NUMBER：当前正在执行的SQL语句的子标识符。
- SQL_EXEC_START：  当前正在执行的SQL语句的开始时间。
- PREV_EXEC_START： 上一个SQL语句的开始时间。
- BLOCKING_SESSION_STATUS：如果会话被阻塞，表示阻塞会话的状态。
- BLOCKING_SESSION：		   如果会话被阻塞，表示阻塞会话的SID。
- STATUS: 当前会话的状态, 值如下:
	- INACTIVE：非活动状态, 会话的初始状态。
	- ACTIVE：  活动状态, 执行操作时,状态会从INACTIVE变为ACTIVE
	- KILLED：  终止状态, 如果会话被终止或取消或发生错误，状态将变为KILLED
	- SNIPED：  在某些情况下，会话可能会在执行完操作后一段时间内保持ACTIVE状态，然后自动变为SNIPED状态。这种情况通常发生在会话执行了较长时间的操作，但没有再次执行新的操作。
- STATE:  当前会话的资源状态
	- WAITING：表示会话当前正在等待某个资源，如等待锁、等待I/O完成等。
	- ON CPU： 表示会话当前正在执行CPU密集型操作，如执行SQL语句。
	- WAITED UNKNOWN TIME：表示会话正在等待资源，但无法确定等待时间。
- WAIT_CLASS：表示会话当前正在等待的事件
	- Application: 表示会话正在等待应用程序相关的事件，如网络通信、客户端请求等。
	- Concurrency: 表示会话正在等待并发控制相关的事件，如行级锁、表级锁等。
	- Commit: 表示会话正在等待提交相关的事件，如等待事务提交完成。
	- Idle: 表示会话当前处于空闲状态，没有正在等待的事件。
	- Network: 表示会话正在等待网络相关的事件，如数据传输、网络连接等。
	- System I/O: 表示会话正在等待系统I/O相关的事件，如磁盘读写操作。
	- User I/O: 表示会话正在等待用户I/O相关的事件，如表空间读写操作。
	- Scheduler: 表示会话正在等待调度器相关的事件，如等待调度器分配资源。
	- Other: 表示会话正在等待其他类型的事件，无法归类到以上类别中。
- WAIT_TIME:  会话在等待事件上已经等待的时间


- 语法
  
- 书籍
  
- sql备忘
