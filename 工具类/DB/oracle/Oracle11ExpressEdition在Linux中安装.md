##官方文档
- https://docs.oracle.com/cd/E17781_01/install.112/e18802/toc.htm#XEINL123

### 安装命令
```shell
# 检查环境依赖
# glibc 应该大于或等于 2.3.4-2.41
# make 应该大于或等于 3.80
# binutils 应该大于或等于 2.16.91.0.5
# gcc 应该大于或等于 4.1.2
# libaio 应该大于或等于 0.3.104

rpm -qa | grep glibc
rpm -qa | grep make
rpm -qa | grep binutils
rpm -qa | grep gcc
rpm -qa | grep libaio

# 安装
rpm -i oracle-xe-11.2.0-1.0.x86_64.rpm

# 配置
#http端口
#数据库端口
#sys和system账号的密码 123456
#是否开机启动
# INTERNAL和和ADMINOracle Application Express用户帐户 的密码最初与SYS和SYSTEM用户帐户的密码相同。

/etc/init.d/oracle-xe configure

#数据库手动启动
/etc/init.d/oracle-xe start
/etc/init.d/oracle-xe stop

# 放开防火墙
firewall-cmd --zone=public --add-port=1521/tcp --permanent
firewall-cmd --reload
```

### navicat 连接
```
连接类型:TNS
网络服务名:192.168.193.11
用户名:system
密码:123456
```
