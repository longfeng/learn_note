## 自增
- https://www.cnblogs.com/lgx5/p/11508649.html

## 分页
```sql
-- limit 2, 10
-- 从第二条开始,查询10条记录
SELECT
	* 
FROM ( 
    SELECT ROWNUM no, t.* FROM table t  WHERE ROWNUM <= 10
) WHERE
	no >= 2;
```

## 开窗函数
- 先分组,然后按照时间排序取第一条[参考](https://www.cnblogs.com/anningkang/p/10931728.html)
```sql
-- demo1
select (
    select 
        a.*,
        row_number() over(partition by 需要分组的字段 order by 更新时间 desc) rw
    from 表名称 a
) t where t.rw = 1

-- demo2
select 
 dl.AMOUNT   
,dal.AUDIT_AMOUNT                FIRST_AMOUNT -- 第一次下发的金额
from CCS_FIN_BUD_DRAFT_LINE dl
left join (
  select bdal.* , row_number() over (partition by bdal.DRAFT_LINE_ID order by bdah.CREATION_DATE) rw
  from CCS_FIN_BUD_DRAFT_AUDIT_LINE bdal
  left join CCS_FIN_BUD_DRAFT_AUDIT_HEAD bdah on bdah.ID = bdal.DRAFT_AUDIT_HEAD_ID
  where bdah.STAT = 5
) dal on dal.rw = 1 and dal.DRAFT_LINE_ID = dl.DRAFT_LINE_ID
```
