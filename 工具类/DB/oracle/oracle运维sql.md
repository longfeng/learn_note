### Oracle中常用的系统表
- [参考](https://www.cnblogs.com/kdy11/p/6898070.html)

### 表空间
```sql
-- 表空间名称及大小
SELECT
	a.TABLESPACE_NAME
	, a.bytes                   total
	, b.bytes                   used
	, c.bytes                   free
	, (b.bytes * 100) / a.bytes "% USED"
	, (c.bytes * 100) / a.bytes "% FREE"
from sys.sm$ts_avail a,
	sys.sm$ts_used b,
	sys.sm$ts_free c
WHERE
	a.tablespace_name = b.tablespace_name
	AND a.tablespace_name = c.tablespace_name;

-- 表空间物理存储信息
SELECT *
FROM dba_tablespaces t, dba_data_files d
WHERE t.tablespace_name = d.tablespace_name
	and t.TABLESPACE_NAME = $(spaceName);

-- 表空间物理存储信息-大小统计(MB)
SELECT t.tablespace_name 表空间, file_name as 文件路径, round(SUM(bytes / (1024 * 1024)), 0) size_mb
FROM dba_tablespaces t, dba_data_files d
WHERE t.tablespace_name = d.tablespace_name
GROUP BY t.tablespace_name;

-- 查看表空间是否开启自动扩容
select d.AUTOEXTENSIBLE
FROM dba_tablespaces dt, dba_data_files d
where dt.tablespace_name = d.tablespace_name
	and dt.tablespace_name = 'GJTMINDEX'

-- 查看索引所在的表空间
select
	a.segment_name
	, a.segment_type
	, a.tablespace_name
	, b.table_name
	, a.bytes / 1024 / 1024 mbytes
	, a.blocks
from user_segments a, user_indexes b
where
	a.segment_name = b.index_name
	-- 索引名称like(区分大小写)
--     and a.segment_name like '%'||$(likeIndexName)||'%'
	-- 表空间类型, INDEX:索引,LOBINDEX:未知
--     and a.segment_type = 'INDEX'
	-- 表空间名称
--     and a.tablespace_name=$(indexName)
	-- 索引所在表
--     and b.table_name like '%'||$(tabName)||'%'
order by table_name, a.bytes / 1024 / 1024 desc
```

### 死锁检测
```sql
-- 检测死锁
select
    vsession.sid       sessionId
    , vsession.serial# session序列
    , vlockobj.oracle_username
    , vlockobj.os_user_name
    , vlockobj.locked_mode
    , obj.object_name
from v$session vsession
join v$locked_object vlockobj on vlockobj.session_id = vsession.sid
join dba_objects obj on obj.object_id = vlockobj.object_id
;

-- 查看正在执行的sql
select
    vsession.sid      	sessionId
    , vsession.username Oracle用户名
    , vsession.OSUSER   操作系统用户名
    , vsession.serial#	session序列
    , vprocess.spid     操作系统ID
    , vsql.sql_FULLtext 正在执行的SQL
    , vsession.machine  计算机名
    , vsession.SQL_EXEC_START	执行时间
    , vsession.PREV_EXEC_START 预执行时间
    , vsql.SQL_ID				sqlID
    , vsession.status		会话状态
    , vsession.STATE		资源状态
    , vsession.WAIT_CLASS	等待类型
    , vsession.WAIT_TIME	等待时间
    , vwait.EVENT			等待事件
    , vwait.SECONDS_IN_WAIT	等待时间秒
    , 'alter system kill session ''' || vsession.sid ||',' || vsession.serial# ||  '''      ;      ' killSql
from V$PROCESS vprocess
JOIN V$SESSION vsession on vprocess.ADDR = vsession.PADDR
JOIN V$SQLAREA vsql on vsession.SQL_HASH_VALUE = vsql.HASH_VALUE
JOIN V$SESSION_WAIT vwait on vwait.SID = vsession.SID
where vsession.status = 'ACTIVE'
-- and vsession.serial# = 
order by vsession.SQL_EXEC_START asc;

-- 10分钟内最占cpu资源的SQL:
select * from
    (SELECT SQL_ID, COUNT(*), ROUND(COUNT(*) / SUM(COUNT(*)) OVER(), 2) PCTLOAD
     FROM V$ACTIVE_SESSION_HISTORY
     WHERE SAMPLE_TIME > SYSDATE - 10 / (24 * 60)
         AND SESSION_TYPE <> 'BACKGROUND'
         AND SESSION_STATE = 'ON CPU'
     GROUP BY SQL_ID
     ORDER BY COUNT(*) DESC )
where rownum <=10;

-- 查看实际执行计划
-- 参数 SQL_ID
SELECT * FROM TABLE(DBMS_XPLAN.DISPLAY_CURSOR('${SQL_ID}', null, 'ALLSTATS LAST'));

-- 生成清空执行计划
-- 参数 SQL_ID
SELECT 'BEGIN ' FROM DUAL
UNION ALL
SELECT DISTINCT 'SYS.DBMS_SHARED_POOL.PURGE(''' || ADDRESS || ','||HASH_VALUE || ''',''C'');'
FROM V$SQL WHERE SQL_ID = '${SQL_ID}'
UNION ALL SELECT 'END;' FROM DUAL
;
-- 它如果没有起到想要的效果, 参考一个思路, 执行表分析


-- 杀死进程
-- 参数 sid
-- 参数 serial#
alter system kill session '${sid}, ${serial#}';


```

### 查询慢sql
-- 简易版, 不准确, 会受到oracle资源繁忙程度的影响
```
-- 慢sql
SELECT sql_id, FIRST_LOAD_TIME, SQL_FULLTEXT, executions, elapsed_time/1000000 AS elapsed_seconds
-- SELECT *
FROM v$sql
WHERE elapsed_time/1000000 > 5
    and FIRST_LOAD_TIME > '2023-07-12/07:00:00'
ORDER BY elapsed_time DESC;
```
-- 复杂版, 比较准确(和awr报告中的接近)
```
SELECT 'Top_cput' top_type
      ,sql_id
      ,planhashvalue
      ,"Elapsed(s)"
       ,"%elap"
       ,execs
       ,"Elap/Exec"
       ,"%dbcpu"
       ,"cpu%"
       ,sql_module
       ,sql_text
       ,"CPU/IO/CLT/APP/CONC%"
FROM   (SELECT rtrim(to_char(nvl((sqt.elap / 1000000), to_number(NULL)), 'fm999999990'), '.') AS "Elapsed(s)"
               ,rtrim(to_char(sqt.elap / (SELECT SUM(elapsed_time_delta)
                                         FROM   dba_hist_sqlstat
                                         WHERE  instance_number LIKE '%%') * 100,
                             'fm999999990.99'),
                     '.') || '%' AS "%elap"
               ,rtrim(to_char(decode(sqt.exec, 0, to_number(NULL), (sqt.elap / sqt.exec / 1000000)),
                             'fm999999990.999'),
                     '.') AS "Elap/Exec"
               ,sqt.exec AS "EXECS"
               ,
               --RTRIM(to_char(nvl((sqt.cput / 1000000), to_number(null)),'fm999999990'),'.') AS "CPU Time(s)",
               rtrim(to_char(sqt.cput / (SELECT SUM(cpu_time_delta)
                                         FROM   dba_hist_sqlstat
                                         WHERE  instance_number LIKE '%%') * 100,
                             'fm999999990.99'),
                     '.') || '%' "%dbcpu"
               ,rtrim(to_char(nvl((sqt.cput / 1000000), to_number(NULL)) /
                             (SELECT SUM(elapsed_time)
                              FROM   (SELECT extract(DAY FROM
                                                     e.end_interval_time - b.end_interval_time) * 1440 +
                                             extract(hour FROM
                                                     e.end_interval_time - b.end_interval_time) * 60 * 60 +
                                             extract(minute FROM
                                                     e.end_interval_time - b.end_interval_time) * 60 +
                                             extract(SECOND FROM
                                                     e.end_interval_time - b.end_interval_time) AS elapsed_time
                                      FROM   dba_hist_snapshot b
                                            ,dba_hist_snapshot e
                                      WHERE  b.instance_number LIKE '%%'
                                      AND    e.instance_number LIKE '%%'
                                      AND    b.startup_time = e.startup_time
                                      AND    b.end_interval_time < e.end_interval_time) t) /
                             (SELECT VALUE / 4 FROM v$parameter WHERE NAME = 'cpu_count') * 100,
                             'fm999999990.9'),
                     '.') || '%' "cpu%"
               ,sqt.bget AS "Buffer Gets"
               ,rtrim(to_char(sqt.bget / (SELECT SUM(buffer_gets_delta)
                                         FROM   dba_hist_sqlstat
                                         WHERE  instance_number LIKE '%%') * 100,
                             'fm999999990.99'),
                     '.') || '%' AS "%buffer"
               ,rtrim(to_char(decode(sqt.exec, 0, to_number(NULL), (sqt.bget / sqt.exec)),
                             'fm999999990'),
                     '.') AS "Gets/Exec"
               ,sqt.dskr AS "Phys Reads"
               ,rtrim(to_char(sqt.dskr / (SELECT SUM(disk_reads_delta)
                                         FROM   dba_hist_sqlstat
                                         WHERE  instance_number LIKE '%%') * 100,
                             'fm999999990.99'),
                     '.') || '%' AS "%diskread"
               ,rtrim(to_char(decode(sqt.exec, 0, to_number(NULL), (sqt.dskr / sqt.exec)),
                             'fm999999990'),
                     '.') AS "Reads/Exec"
               ,rtrim(to_char(nvl((sqt.iowait / 1000000), to_number(NULL)), 'fm999999990.9'), '.') || ', ' ||
               rtrim(to_char(sqt.iowait / (SELECT SUM(iowait_delta)
                                           FROM   dba_hist_sqlstat
                                           WHERE  instance_number LIKE '%%') * 100,
                             'fm999999990.99'),
                     '.') || '%' AS "I/O(s)_%iowait"
               ,rtrim(to_char(decode(sqt.exec, 0, to_number(NULL), (sqt.iowait / sqt.exec / 1000000)),
                             'fm999999990.999'),
                     '.') AS "iowait/exec"
               ,rtrim(to_char(decode(sqt.elap, 0, to_number(NULL), (sqt.cput / sqt.elap * 100)),
                             'fm999999990'),
                     '.') || '%' || ',' ||
               rtrim(to_char(decode(sqt.elap, 0, to_number(NULL), (sqt.iowait / sqt.elap * 100)),
                             'fm999999990'),
                     '.') || '%' || ',' ||
               rtrim(to_char(decode(sqt.elap, 0, to_number(NULL), (sqt.clwait / sqt.elap * 100)),
                             'fm999999990'),
                     '.') || '%' || ',' ||
               rtrim(to_char(decode(sqt.elap, 0, to_number(NULL), (sqt.appwait / sqt.elap * 100)),
                             'fm999999990'),
                     '.') || '%' || ',' ||
               rtrim(to_char(decode(sqt.elap, 0, to_number(NULL), (sqt.ccwait / sqt.elap * 100)),
                             'fm999999990'),
                     '.') || '%' AS "CPU/IO/CLT/APP/CONC%"
               ,sqt.sql_id
               ,sqt.plan_hash_value planhashvalue
               ,decode(sqt.module, NULL, NULL, sqt.module) sql_module
               ,nvl(st.sql_text, to_clob('** SQL Text Not Available **')) AS clob_sql_text
               ,dbms_lob.substr(st.sql_text, 3600) AS sql_text
        FROM   (SELECT sql_id
                      ,plan_hash_value
                      ,substr(MAX(module), 1, 50) module
                      ,SUM(elapsed_time_delta) elap
                      ,SUM(cpu_time_delta) cput
                      ,SUM(buffer_gets_delta) bget
                      ,SUM(disk_reads_delta) dskr
                      ,SUM(executions_delta) exec
                      ,SUM(iowait_delta) iowait
                      ,SUM(clwait_delta) clwait
                      ,SUM(apwait_delta) appwait
                      ,SUM(ccwait_delta) ccwait
                FROM   dba_hist_sqlstat
                WHERE  nvl(parsing_schema_name, 0) LIKE '%%'
                      --AND    nvl(module, 0) LIKE '%:cp:cux%'
                AND    plan_hash_value != 0
                AND    instance_number LIKE '%%'
                GROUP  BY sql_id
                         ,plan_hash_value) sqt
              ,dba_hist_sqltext st
        WHERE  st.sql_id(+) = sqt.sql_id
        AND    sqt.exec >= decode('', NULL, 10, '')
        AND    decode(sqt.exec, 0, sqt.elap, sqt.elap / sqt.exec / 1000000) >=
               decode('', NULL, 0, '')
        ORDER  BY nvl(sqt.cput, -1) DESC)
WHERE  rownum <= decode('', NULL, 10, '')
UNION
SELECT 'Top_elap'
      ,sql_id
      ,planhashvalue
      ,"Elapsed(s)"
       ,"%elap"
       ,execs
       ,"Elap/Exec"
       ,"%dbcpu"
       ,"cpu%"
       ,sql_module
       ,sql_text
       ,"CPU/IO/CLT/APP/CONC%"
FROM   (SELECT rtrim(to_char(nvl((sqt.elap / 1000000), to_number(NULL)), 'fm999999990'), '.') AS "Elapsed(s)"
               ,rtrim(to_char(sqt.elap / (SELECT SUM(elapsed_time_delta)
                                         FROM   dba_hist_sqlstat
                                         WHERE  instance_number LIKE '%%') * 100,
                             'fm999999990.99'),
                     '.') || '%' AS "%elap"
               ,rtrim(to_char(decode(sqt.exec, 0, to_number(NULL), (sqt.elap / sqt.exec / 1000000)),
                             'fm999999990.999'),
                     '.') AS "Elap/Exec"
               ,sqt.exec AS "EXECS"
               ,
               --RTRIM(to_char(nvl((sqt.cput / 1000000), to_number(null)),'fm999999990'),'.') AS "CPU Time(s)",
               rtrim(to_char(sqt.cput / (SELECT SUM(cpu_time_delta)
                                         FROM   dba_hist_sqlstat
                                         WHERE  instance_number LIKE '%%') * 100,
                             'fm999999990.99'),
                     '.') || '%' "%dbcpu"
               ,rtrim(to_char(nvl((sqt.cput / 1000000), to_number(NULL)) /
                             (SELECT SUM(elapsed_time)
                              FROM   (SELECT extract(DAY FROM
                                                     e.end_interval_time - b.end_interval_time) * 1440 +
                                             extract(hour FROM
                                                     e.end_interval_time - b.end_interval_time) * 60 * 60 +
                                             extract(minute FROM
                                                     e.end_interval_time - b.end_interval_time) * 60 +
                                             extract(SECOND FROM
                                                     e.end_interval_time - b.end_interval_time) AS elapsed_time
                                      FROM   dba_hist_snapshot b
                                            ,dba_hist_snapshot e
                                      WHERE  b.instance_number LIKE '%%'
                                      AND    e.instance_number LIKE '%%'
                                      AND    b.startup_time = e.startup_time
                                      AND    b.end_interval_time < e.end_interval_time) t) /
                             (SELECT VALUE / 4 FROM v$parameter WHERE NAME = 'cpu_count') * 100,
                             'fm999999990.9'),
                     '.') || '%' "cpu%"
               ,sqt.bget AS "Buffer Gets"
               ,rtrim(to_char(sqt.bget / (SELECT SUM(buffer_gets_delta)
                                         FROM   dba_hist_sqlstat
                                         WHERE  instance_number LIKE '%%') * 100,
                             'fm999999990.99'),
                     '.') || '%' AS "%buffer"
               ,rtrim(to_char(decode(sqt.exec, 0, to_number(NULL), (sqt.bget / sqt.exec)),
                             'fm999999990'),
                     '.') AS "Gets/Exec"
               ,sqt.dskr AS "Phys Reads"
               ,rtrim(to_char(sqt.dskr / (SELECT SUM(disk_reads_delta)
                                         FROM   dba_hist_sqlstat
                                         WHERE  instance_number LIKE '%%') * 100,
                             'fm999999990.99'),
                     '.') || '%' AS "%diskread"
               ,rtrim(to_char(decode(sqt.exec, 0, to_number(NULL), (sqt.dskr / sqt.exec)),
                             'fm999999990'),
                     '.') AS "Reads/Exec"
               ,rtrim(to_char(nvl((sqt.iowait / 1000000), to_number(NULL)), 'fm999999990.9'), '.') || ', ' ||
               rtrim(to_char(sqt.iowait / (SELECT SUM(iowait_delta)
                                           FROM   dba_hist_sqlstat
                                           WHERE  instance_number LIKE '%%') * 100,
                             'fm999999990.99'),
                     '.') || '%' AS "I/O(s)_%iowait"
               ,rtrim(to_char(decode(sqt.exec, 0, to_number(NULL), (sqt.iowait / sqt.exec / 1000000)),
                             'fm999999990.999'),
                     '.') AS "iowait/exec"
               ,rtrim(to_char(decode(sqt.elap, 0, to_number(NULL), (sqt.cput / sqt.elap * 100)),
                             'fm999999990'),
                     '.') || '%' || ',' ||
               rtrim(to_char(decode(sqt.elap, 0, to_number(NULL), (sqt.iowait / sqt.elap * 100)),
                             'fm999999990'),
                     '.') || '%' || ',' ||
               rtrim(to_char(decode(sqt.elap, 0, to_number(NULL), (sqt.clwait / sqt.elap * 100)),
                             'fm999999990'),
                     '.') || '%' || ',' ||
               rtrim(to_char(decode(sqt.elap, 0, to_number(NULL), (sqt.appwait / sqt.elap * 100)),
                             'fm999999990'),
                     '.') || '%' || ',' ||
               rtrim(to_char(decode(sqt.elap, 0, to_number(NULL), (sqt.ccwait / sqt.elap * 100)),
                             'fm999999990'),
                     '.') || '%' AS "CPU/IO/CLT/APP/CONC%"
               ,sqt.sql_id
               ,sqt.plan_hash_value planhashvalue
               ,decode(sqt.module, NULL, NULL, sqt.module) sql_module
               ,nvl(st.sql_text, to_clob('** SQL Text Not Available **')) AS clob_sql_text
               ,dbms_lob.substr(st.sql_text, 3600) AS sql_text
        FROM   (SELECT sql_id
                      ,plan_hash_value
                      ,substr(MAX(module), 1, 50) module
                      ,SUM(elapsed_time_delta) elap
                      ,SUM(cpu_time_delta) cput
                      ,SUM(buffer_gets_delta) bget
                      ,SUM(disk_reads_delta) dskr
                      ,SUM(executions_delta) exec
                      ,SUM(iowait_delta) iowait
                      ,SUM(clwait_delta) clwait
                      ,SUM(apwait_delta) appwait
                      ,SUM(ccwait_delta) ccwait
                FROM   dba_hist_sqlstat
                WHERE  nvl(parsing_schema_name, 0) LIKE '%%'
                      --AND    nvl(module, 0) LIKE '%:cp:cux%'
                AND    plan_hash_value != 0
                AND    instance_number LIKE '%%'
                GROUP  BY sql_id
                         ,plan_hash_value) sqt
              ,dba_hist_sqltext st
        WHERE  st.sql_id(+) = sqt.sql_id
        AND    sqt.exec >= decode('', NULL, 10, '')
        AND    decode(sqt.exec, 0, sqt.elap, sqt.elap / sqt.exec / 1000000) >=
               decode('', NULL, 0, '')
        ORDER  BY nvl(sqt.elap, -1) DESC)
WHERE  rownum <= decode('', NULL, 10, '')
UNION
SELECT 'Top_exec'
      ,sql_id
      ,planhashvalue
      ,"Elapsed(s)"
       ,"%elap"
       ,execs
       ,"Elap/Exec"
       ,"%dbcpu"
       ,"cpu%"
       ,sql_module
       ,sql_text
       ,"CPU/IO/CLT/APP/CONC%"
FROM   (SELECT rtrim(to_char(nvl((sqt.elap / 1000000), to_number(NULL)), 'fm999999990'), '.') AS "Elapsed(s)"
               ,rtrim(to_char(sqt.elap / (SELECT SUM(elapsed_time_delta)
                                         FROM   dba_hist_sqlstat
                                         WHERE  instance_number LIKE '%%') * 100,
                             'fm999999990.99'),
                     '.') || '%' AS "%elap"
               ,rtrim(to_char(decode(sqt.exec, 0, to_number(NULL), (sqt.elap / sqt.exec / 1000000)),
                             'fm999999990.999'),
                     '.') AS "Elap/Exec"
               ,sqt.exec AS "EXECS"
               ,
               --RTRIM(to_char(nvl((sqt.cput / 1000000), to_number(null)),'fm999999990'),'.') AS "CPU Time(s)",
               rtrim(to_char(sqt.cput / (SELECT SUM(cpu_time_delta)
                                         FROM   dba_hist_sqlstat
                                         WHERE  instance_number LIKE '%%') * 100,
                             'fm999999990.99'),
                     '.') || '%' "%dbcpu"
               ,rtrim(to_char(nvl((sqt.cput / 1000000), to_number(NULL)) /
                             (SELECT SUM(elapsed_time)
                              FROM   (SELECT extract(DAY FROM
                                                     e.end_interval_time - b.end_interval_time) * 1440 +
                                             extract(hour FROM
                                                     e.end_interval_time - b.end_interval_time) * 60 * 60 +
                                             extract(minute FROM
                                                     e.end_interval_time - b.end_interval_time) * 60 +
                                             extract(SECOND FROM
                                                     e.end_interval_time - b.end_interval_time) AS elapsed_time
                                      FROM   dba_hist_snapshot b
                                            ,dba_hist_snapshot e
                                      WHERE  b.instance_number LIKE '%%'
                                      AND    e.instance_number LIKE '%%'
                                      AND    b.startup_time = e.startup_time
                                      AND    b.end_interval_time < e.end_interval_time) t) /
                             (SELECT VALUE / 4 FROM v$parameter WHERE NAME = 'cpu_count') * 100,
                             'fm999999990.9'),
                     '.') || '%' "cpu%"
               ,sqt.bget AS "Buffer Gets"
               ,rtrim(to_char(sqt.bget / (SELECT SUM(buffer_gets_delta)
                                         FROM   dba_hist_sqlstat
                                         WHERE  instance_number LIKE '%%') * 100,
                             'fm999999990.99'),
                     '.') || '%' AS "%buffer"
               ,rtrim(to_char(decode(sqt.exec, 0, to_number(NULL), (sqt.bget / sqt.exec)),
                             'fm999999990'),
                     '.') AS "Gets/Exec"
               ,sqt.dskr AS "Phys Reads"
               ,rtrim(to_char(sqt.dskr / (SELECT SUM(disk_reads_delta)
                                         FROM   dba_hist_sqlstat
                                         WHERE  instance_number LIKE '%%') * 100,
                             'fm999999990.99'),
                     '.') || '%' AS "%diskread"
               ,rtrim(to_char(decode(sqt.exec, 0, to_number(NULL), (sqt.dskr / sqt.exec)),
                             'fm999999990'),
                     '.') AS "Reads/Exec"
               ,rtrim(to_char(nvl((sqt.iowait / 1000000), to_number(NULL)), 'fm999999990.9'), '.') || ', ' ||
               rtrim(to_char(sqt.iowait / (SELECT SUM(iowait_delta)
                                           FROM   dba_hist_sqlstat
                                           WHERE  instance_number LIKE '%%') * 100,
                             'fm999999990.99'),
                     '.') || '%' AS "I/O(s)_%iowait"
               ,rtrim(to_char(decode(sqt.exec, 0, to_number(NULL), (sqt.iowait / sqt.exec / 1000000)),
                             'fm999999990.999'),
                     '.') AS "iowait/exec"
               ,rtrim(to_char(decode(sqt.elap, 0, to_number(NULL), (sqt.cput / sqt.elap * 100)),
                             'fm999999990'),
                     '.') || '%' || ',' ||
               rtrim(to_char(decode(sqt.elap, 0, to_number(NULL), (sqt.iowait / sqt.elap * 100)),
                             'fm999999990'),
                     '.') || '%' || ',' ||
               rtrim(to_char(decode(sqt.elap, 0, to_number(NULL), (sqt.clwait / sqt.elap * 100)),
                             'fm999999990'),
                     '.') || '%' || ',' ||
               rtrim(to_char(decode(sqt.elap, 0, to_number(NULL), (sqt.appwait / sqt.elap * 100)),
                             'fm999999990'),
                     '.') || '%' || ',' ||
               rtrim(to_char(decode(sqt.elap, 0, to_number(NULL), (sqt.ccwait / sqt.elap * 100)),
                             'fm999999990'),
                     '.') || '%' AS "CPU/IO/CLT/APP/CONC%"
               ,sqt.sql_id
               ,sqt.plan_hash_value planhashvalue
               ,decode(sqt.module, NULL, NULL, sqt.module) sql_module
               ,nvl(st.sql_text, to_clob('** SQL Text Not Available **')) AS clob_sql_text
               ,dbms_lob.substr(st.sql_text, 3600) AS sql_text
        FROM   (SELECT sql_id
                      ,plan_hash_value
                      ,substr(MAX(module), 1, 50) module
                      ,SUM(elapsed_time_delta) elap
                      ,SUM(cpu_time_delta) cput
                      ,SUM(buffer_gets_delta) bget
                      ,SUM(disk_reads_delta) dskr
                      ,SUM(executions_delta) exec
                      ,SUM(iowait_delta) iowait
                      ,SUM(clwait_delta) clwait
                      ,SUM(apwait_delta) appwait
                      ,SUM(ccwait_delta) ccwait
                FROM   dba_hist_sqlstat
                WHERE  172984 < snap_id
                AND    nvl(parsing_schema_name, 0) LIKE '%%'
                      --AND    nvl(module, 0) LIKE '%:cp:cux%'
                AND    plan_hash_value != 0
                AND    instance_number LIKE '%%'
                GROUP  BY sql_id
                         ,plan_hash_value) sqt
              ,dba_hist_sqltext st
        WHERE  st.sql_id(+) = sqt.sql_id
        AND    sqt.exec >= decode('', NULL, 10, '')
        AND    decode(sqt.exec, 0, sqt.elap, sqt.elap / sqt.exec / 1000000) >=
               decode('', NULL, 0.1, '')
        ORDER  BY nvl(sqt.exec, -1) DESC)
WHERE  rownum <= decode('', NULL, 10, '')
;
```

### 强制使用索引
```sql
-- BCS_BARCODE是表名
-- BCS_BARCODE_BARCODE_IDX_CUST_CODE_BARCODE_ID是索引
-- 注意他可能不生效,需要查看真实执行计划( SELECT * FROM TABLE(DBMS_XPLAN.DISPLAY_CURSOR(sql_id,null,null)) )
select /*+index(BCS_BARCODE BCS_BARCODE_BARCODE_IDX_CUST_CODE_BARCODE_ID)*/
*
from BCS_BARCODE
```

### 清理索引
- 找到对应的sql_id
```sql
-- 方式1: 根据所有历史sql
SELECT * FROM V$SQL WHERE sql_fulltext LIKE '%IS_SYNC%' ORDER BY last_load_time DESC;
-- 方式2: 根据正在执行的sql信息
SELECT b.sid oracleID,
    b.username 登录Oracle用户名,
    b.serial#,
    spid 操作系统ID,
    paddr,
    sql_FULLtext 正在执行的SQL,
    b.machine 计算机名
    ,b.SQL_EXEC_START
    ,b.PREV_EXEC_START
    ,c.SQL_ID
FROM v$process a, v$session b, v$sqlarea c
WHERE a.addr = b.paddr
    AND b.sql_hash_value = c.hash_value 
```
- 查询对应sql_id的真实执行计划
```sql
SELECT * FROM TABLE(DBMS_XPLAN.DISPLAY_CURSOR('3agpxd0hwvrhr',null,null));
```
- 生成清理执行计划的sql语句
```sql
SELECT 'BEGIN ' FROM DUAL
UNION ALL
SELECT DISTINCT 'SYS.DBMS_SHARED_POOL.PURGE(''' || ADDRESS || ','||HASH_VALUE || ''',''C'');'
FROM V$SQL WHERE SQL_ID = '3agpxd0hwvrhr'
UNION ALL SELECT 'END;' FROM DUAL;
```
- 执行上面生成的sql, 主要是SYS.DBMS_SHARED_POOL.PURGE

### 服务器负载情况
```sql
-- 服务器负载情况(按照小时统计)
SELECT *
FROM ( SELECT --A.INSTANCE_NUMBER,A.SNAP_ID,
     B.BEGIN_INTERVAL_TIME + 0 BEGIN_TIME,B.END_INTERVAL_TIME + 0 END_TIME
     ,ROUND(VALUE - LAG( VALUE, 1 , '0') OVER(ORDER BY A.INSTANCE_NUMBER, A.SNAP_ID)) "DB TIME"
     FROM (SELECT B.SNAP_ID,INSTANCE_NUMBER,SUM(VALUE ) / 1000000 / 60 VALUE
               FROM DBA_HIST_SYS_TIME_MODEL B
              WHERE B.DBID = (SELECT DBID FROM V$DATABASE) AND UPPER (B.STAT_NAME) IN UPPER(('DB TIME' ))
              GROUP BY B.SNAP_ID, INSTANCE_NUMBER) A,DBA_HIST_SNAPSHOT B
     WHERE A.SNAP_ID = B.SNAP_ID
     AND B.DBID = (SELECT DBID FROM V$DATABASE)
     AND B.INSTANCE_NUMBER = A.INSTANCE_NUMBER
)
WHERE TO_CHAR(BEGIN_TIME, 'YYYY-MM-DD') <= TO_CHAR(SYSDATE , 'YYYY-MM-DD')
ORDER BY BEGIN_TIME;
```

### 服务器运行的所有job
```sql
select * from all_jobs
```

### 删除表
```sql
drop table table_name purge; 
```

### 创建索引(指定表空间)
```sql
-- 在线创建索引，参考这种写法
create index index_name on table_name (field_name)  tablespace gccsdbindex  nologging parallel online;
```

### 索引不生效(可以尝试重建索引)
1. 检查索引是否存在, 以及索引状态,比如key的重复性
```
SELECT index_name, -- 索引名
       table_name, -- 表名
       num_rows, -- 数据行数
       DISTINCT_KEYS -- key数
FROM user_indexes
WHERE table_name = 'IRREGULAR_ORDER_INIT_LINE';
```
2. 尝试指定索引执行都不生效
```
select /*+ INDEX(t IDX_IRR_ORDER_INIT_LINE_N1) */ * from IRREGULAR_ORDER_INIT_LINE where INIT_KEY = '31b0e8b914bd4c7dbf32211abaaca943'
```
3. 更新统计信息(重建索引)
```sql
begin
DBMS_STATS.GATHER_TABLE_STATS('GCCSDB', 'IRREGULAR_ORDER_INIT_LINE');
end;
/
```

### 备份表(指定表空间)
```sql
-- 备份表
create table GCCSDB.CCS_BASE_ITEM_SYNC_SWAP_ZLHZ_FUYANG tablespace GCCSDBBAK as
select *
from CCS_BASE_ITEM cbi
where cbi.ITEM_CODE = '1000090'

```

### 如何生成AWR报告(服务器操作方式)
```shell
# 登录进入服务器

# 切换至oracle用户 
su - oracle

# 以dba身份登陆数据库：
sqlplus / as sysdba

# 执行awrrpt.sql文件
@?/rdbms/admin/awrrpt.sql

# 之后根据提示进行输入

# 输入报告文件类型 
# 示例: Enter value for report_type: html


# 输入报告范围(天),从当前日期倒数. 画外音:查询多少天的报告
# 示例: Enter value for num_days: 1
# 系统会显示这个范围里面的报告片段(默认1小时一个片段)
 
# 输入起始范围编码
# 示例: Enter value for begin_snap: 9510
# 输入结束范围编码
# 示例: Enter value for end_snap: 9532

# 输入报告文件名称:(假设这里是取2023年0916日08点-12点的报告
# Enter value for report_name: AWR_230916_08-12.html

# 报告输出目录(默认)
ll home/oracle

```

### update慢示例
#### 示例1
```
-- 表7E数据, OUT_BOTTLE_CODE, IN_BOTTLE_CODE, PACKET_CODE均有索引
-- 需要跟新的数据 1217,6578
UPDATE BCS_BARCODE
   SET IN_BOTTLE_CODE = 'XNN' || PACKET_CODE
 WHERE IN_BOTTLE_CODE IS NULL
   AND OUT_BOTTLE_CODE = 'XN' || PACKET_CODE
```
- 步骤:
1. 创建临时表(如果有从库,则使用导入导出)
```
create table BCS_BARCODE_BK as
SELECT 
	'XNN' || PACKET_CODE AS IN_BOTTLE_CODE,
	'XN' || PACKET_CODE AS OUT_BOTTLE_CODE
FROM BCS_BARCODE
 WHERE IN_BOTTLE_CODE IS NULL
   AND OUT_BOTTLE_CODE = 'XN' || PACKET_CODE;
```
2. 优化执行计划
```sql
-- plan ok? 可以尝试使用in
SELECT
    bb.*,
    (
        SELECT
            IN_BOTTLE_CODE  
        FROM BCS_BARCODE_BK bbb
        WHERE bbb.OUT_BOTTLE_CODE =  bb.OUT_BOTTLE_CODE
    ) AS IN_BOTTLE_CODE
FROM BCS_BARCODE bb
WHERE EXISTS (
    SELECT
        1 
    FROM BCS_BARCODE_BK bbb
    WHERE bbb.OUT_BOTTLE_CODE =  bb.OUT_BOTTLE_CODE
)

-- plan ok?
SELECT /*+ INDEX(bb IDX_OUT_BOTTLE_CODE) */ 
    bb.*,
    (
        SELECT /*+index(bbb IDX_OUT_BOTTLE_CODE)*/
            IN_BOTTLE_CODE  
        FROM BCS_BARCODE_BK bbb
        WHERE bbb.OUT_BOTTLE_CODE =  bb.OUT_BOTTLE_CODE
    ) AS IN_BOTTLE_CODE
FROM BCS_BARCODE bb
WHERE EXISTS (
    SELECT /*+index(bbb IDX_OUT_BOTTLE_CODE)*/
        1 
    FROM BCS_BARCODE_BK bbb
    WHERE bbb.OUT_BOTTLE_CODE =  bb.OUT_BOTTLE_CODE
)
```
3. 调优之后速度还是很慢
   - 将临时表拆分成多个表
4. 最终进行更新(根据之前的执行计划和查询结果快慢来决定 是否需要强制索引)
```sql
update /*+ INDEX(bb IDX_OUT_BOTTLE_CODE) */
BCS_BARCODE bb
set bb.IN_BOTTLE_CODE = (
        SELECT /*+index(bbb IDX_OUT_BOTTLE_CODE)*/
            IN_BOTTLE_CODE  
        FROM BCS_BARCODE_BK bbb
        WHERE bbb.OUT_BOTTLE_CODE =  bb.OUT_BOTTLE_CODE
    )
WHERE EXISTS (
    SELECT /*+index(bbb IDX_OUT_BOTTLE_CODE)*/
        1 
    FROM BCS_BARCODE_BK bbb
    WHERE bbb.OUT_BOTTLE_CODE =  bb.OUT_BOTTLE_CODE
)
```

### oracle 查询条件

## 未整理
```

Abc:
活动会话数:
Select count(*) from gv$session where status='ACTIVE' and type='USER';

等待事件：
Select event,count(*) from gv$session where wait_class <>'Idle';

并根据sql_id kill掉原来执行异常的会话：
select 'alter system kill session '''||sid||','||serial#||''' immediate;' from v$session where sql_id='&sql_id'

Abc:
SELECT l.session_id sid,
s.serial#,
l.locked_mode 锁模式,
l.oracle_username 登录用户,
l.os_user_name 登录机器用户名,
s.machine 机器名,
s.terminal 终端用户名,
o.object_name 被锁对象名,
s.logon_time 登录数据库时间
FROM v$locked_object l, all_objects o, v$session s
WHERE l.object_id = o.object_id
AND l.session_id = s.sid
ORDER BY sid, s.serial#;

Abc:
查询DML造成的锁
select sid,
blocking_session,
LOGON_TIME,
sql_id,
status,
event,
seconds_in_wait,
state,
BLOCKING_SESSION_STATUS
from v$session
where event like 'enq%'
and state = 'WAITING'
and BLOCKING_SESSION_STATUS = 'VALID';

```



