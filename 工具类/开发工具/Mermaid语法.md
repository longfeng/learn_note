#官方中文版手册:
https://mermaid.nodejs.cn/intro/

## 功能
- sequenceDiagram
- flowchart

## 颜色和边框
```mermaid
flowchart LR
    黄色 --> 红色 --> 绿色 --> 灰色
    style 黄色 fill:#060
    style 红色 fill:#600
    style 绿色 fill:#660
    style 灰色 fill:#666
    黄边框 --> 红边框 --> 绿边框 --> 灰边框
    style 黄边框 stroke:#060
    style 红边框 stroke:#600
    style 绿边框 stroke:#660
    style 灰边框 stroke:#666

```
