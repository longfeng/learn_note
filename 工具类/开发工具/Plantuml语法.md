#官方中文版手册:
https://plantuml.com/zh



### 修改节点的形状
- 示例: 定义节点1使用actor形状 `actor 节点1`
- 内置形状有
  - actor（角色）
  - boundary（边界）
  - control（控制）
  - entity（实体）
  - database（数据库）
  - collections（集合）
  - queue（队列）

### 加注释
- 语法 note (left|right) [of 节点名称] [#FFFFFF] 
```puml
note left
未指定节点名称,语法通过,但是不会显示
end note

note left of test #00FF00
这段注释会显示在左边
end note

note right of test #00FF00
这段注释会显示在右边
end note
```

### 箭头颜色
-[#0000FF]>

### 去掉序号
删除 autonumber

### 增加标题
header Page Header

### 示例
```puml
@startuml
'https://plantuml.com/sequence-diagram

title 分支管理_冲突处理(远程功能分支A合并到dev冲突)

control "本地\n功能分支A" as 功能分支A
participant "远程功能分支A" as 远程功能分支A
participant "远程功能分支A2" as 远程功能分支A2
participant "sit分支\nsit环境" as sit分支

autonumber


功能分支A -[#Green]> 远程功能分支A: 自测完成,将分支发布(push)到远程

远程功能分支A2 -[#Green]> sit分支: sit环境提测,发起合并(merger)请求

远程功能分支A -[#RED]>x sit分支: sit环境提测,发起合并(merger)请求\n与功能分支A2冲突

远程功能分支A2 -[#Green]> 功能分支A: 将远程功能分支A2合并到本地分支, 并处理冲突
note right 功能分支A #YELLOW
注意:两个合并的功能分支在发生产的时候需要一起发版
end note

功能分支A -[#Green]> 远程功能分支A: 自测完成,将分支发布(push)到远程

远程功能分支A -[#Green]> sit分支: sit环境提测,发起合并(merger)请求

@enduml

``` 