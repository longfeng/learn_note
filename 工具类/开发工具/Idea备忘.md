# Idea备忘
## [idea配置](../source/idea_settings_for_leon.zip)

##激活2021.1.3(无限试用,包括插件)
1. 先设置试用
	- 如果以前激活过，无法试用了
		- 用[idea无限试用.7z](../../source/idea无限试用.7z)中的reset_jetbrains_eval_windows.vbs进行重置试用
		- 重启IDEA， 设置试用30天
2. 安装插件
   - 用[idea无限试用.7z](../../source/idea无限试用.7z)里面的插件或者去[下载最新插件](https://gitee.com/pengzhile/ide-eval-resetter)
     - 此插件也支持无限试用（MyBatisCodeHelperPro (Marketplace Edition)）
3. 设置插件中的
    - 打开任意项目
    - `[点击]Help` -> `[点击]Eval Reset` -> `[勾选]Auto reset before per restart` -> `[点击]Reload`
##激活2023
1. [idea全家桶2023-Win系统本地激活.zip](../../source/idea全家桶2023-Win系统本地激活.zip)
    - 解压后执行vbs命令
    
## 常用插件
- CamelCase
    驼峰转换 Ctrl+Alt+Shift+U
- Chinese language
    - 汉化包
- GenerateAllSetter
    - 一键gettersettter
- CodeGlance
    - 右侧代码地图
- `MyBatisCodeHelperPro (Marketplace Edition)[收费]`
    - Mapper.xml映射(收费的插件, 这款可以支持无限试用)
    - 一共有两款,功能相同,
    - ***另外一款是`MyBatisCodeHelperPro`~~不支持无限试用.***
- MyBatis Log plugin
    - 解析sql日志利器(收费的插件, 这款可以支持无限试用)
- MybatisX
    - Mapper.xml映射
- PlantUML
    - 时序图
- junitgenerator
    - 自动生成单元测试
- Key Promoter X
    - IDEA快捷键提示
- grep console
    - 快速找到自己想要的类型日志，从settings进入，点击 other settings
- GsonFormat
    - 自动帮你把类转化为JSON对象，一大利器
- String Manipulation
    - 字符串操作
- AceJump
    - （键盘流）快速定位工具,键盘流 快捷键 ctrl+;
- Maven Helper
    - maven依赖分析
- Translation
    - 翻译
- Restful Fast Request[收费,不贵][推荐使用作为api测试工具]
  - restfull接口测试工具(自动识别swagger注解), 可以配置类似于postman一样的环境变量
  - 在java代码的左侧栏有一个像火箭一样的东西, 点击之后会自动在右边栏生成请求参数和地址
  - bug挺多的
- EasyYapi
  - yapi导出工具, api测试工具
- Mybatis log plugin[收费]
  - 安装插件后, 在底部栏会有一个mybatis Log的工作栏, 进入开启后会监听日志中的mybatis生成的sql, 并且自动填充参数
  - 可以手动解析日志中的sql, 并填充参数
- grep console
  - 自动将控制台不同level的日志显示不同颜色
- Rainbow Brackets 收费
  - 彩虹括号
  

## 快捷键
  - 进入方法实现类 
    - 光标定位到方法名称上 `Ctrl+Alt+B` 反之进入方法父类 `Ctrl+B`
    - `Ctrl+Alt+鼠标左键`
  - 查看当前方法结构图 `Ctrl+H`
  - 查看当前方法调用链 `Ctrl+Alt+H`
  - 标签
    - 打标签 `Ctrl+F11`
    - 查看标签 `Shift+F11`
    - 快速进入标签 `Ctrl+标签对应的数字或者字母`
  - 当前Tab全屏或恢复 `Ctrl+Shift+F12`
  - 返回到上次修改处 `Ctrl+Shift+Backspace`
  - 折叠方法 `Ctrl+加号`, 反之 `Ctrl+减号`
  - 折叠类的所有方法 `Ctrl+Shift+加号`, 反之 `Ctrl+Shift+减号`
  - 快速定位文本 `Ctrl+;`,需要配合Acejump插件

## 粘贴xml时保持源格式
    setting-》Editor(编辑器)-》Code Style(代码样式)->XML
    在右边的面板中，单击第二个 “Other” 的页签，勾选“Keep white spaces(保留里面的空格)”，重启idea。
## 粘贴java文件时取消代码自动缩进
    settings-》Eidtor(编辑器)-》General(常规)-》Smart keys(智能按键)
    在右边的面板中, 找到`Reformat on paste`(粘贴时重新格式化)选项, 选择`None`, 它默认是`ident each line`
## 取消idea在保存时的自动格式化(这里没必要了by.24.06.20)
    Settings>>Editor>>General
    找到On Save将下面的勾勾都去掉

## 配置Linux服务器(SSH)
- 配置入口: File -> Settings -> ssh Configurations -> add
    - Visible only for this project勾选后只在当前项目中可见, 否则所有项目中可见
- 使用入口: Tool -> Start SSH Session

## 如何通过搜索快速定位到方法与行
- 双击shift
    - 定位到行
      - 搜索PoHeadServiceImpl.java:137
    - 定位到方法
      - 搜索PoHeadServiceImpl.getPoPrintData

## 如何将mybatis中的#{}占位符作为sql执行中的占位符
- 设置->工具->数据库->执行查询->用户形参
- `新增`一种`形参模式`
	- 模式:`#\{\s*(\w+).*?\}`
	- `勾选`在脚本中
	- `删除勾选`在字面量中
      - 点击`所有语言`, 只保留`sql`
- 执行验证sql
```oracle
select
    #{itemId, jdbcType=BIGINT} as a,
    #{ itemCode ,jdbcType=VARCHAR} as b
from dual
;
```
- 执行后输入`'x'`,如果执行不报错则表示完成
- 注意: 字符串类型时需要手动增加''

## 启动tomcat乱码
- 参考: https://blog.csdn.net/Bernie_7/article/details/121742274

## idea每次打开新的maven项目都需要重新配置
1. 用户设置文件, 勾选重写
2. 取消勾选 使用.mvn/maven.config中的设置

## idea设置自定义的额todo
格式如下:`\bltodo\b.*`