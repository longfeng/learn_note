# 通过yum安装或卸载openJDK
1. 安装：
```shell
#查询可用的jdk安装包
yum search jdk
#根据包名安装jdk
yum -y install java-1.8.0-openjdk-devel.x86_64
# 11
# yum -y install java-11-openjdk-devel.x86_64

```
2. 卸载：
```shellv
#查询已经安装的jdk
yum list installed |grep jdk
#卸载已安装的jdk
yum -y remove java-1.8.0-openjdk*
```
3. 注意：
- yum方式安装jdk的默认目录/usr/lib/jvm
- yum方式安装jdk为java的安装目录做了快捷方式指向/usr/bin/java，所以不需要配置JAVA_HOME
- openjdk相对oracle版本区别：https://www.jianshu.com/p/441aa1a976b3


# 通过官方直接下载tar安装jdk
1. 安装：
  - [下载地址](rjavase-jdk8-downloads.html)
  - 推荐（注意64版本, 不要下成ARM版本了 ）：Linux x64 Compressed Archive jdk-8u301-linux-x64.tar.gz
2. 配置环境变量：
```shell
cd /usr/src
#解压
tar -zxvf jdk-8u301-linux-x64.tar.gz
# mv或者 cp -r jdk1.8.0_301/ /usr/local/lib/
mv jdk1.8.0_301/ /usr/local/lib/

#配置环境变量
vi /etc/profile
/export PATH
i

#在最后的export配置后面追加以下配置-start
export JAVA_HOME=/usr/local/lib/jdk1.8.0_301
export CLASSPATH=$:CLASSPATH:$JAVA_HOME/lib/
export PATH=$PATH:$JAVA_HOME/bin
#在最后的export配置后面追加以下配置-end

#更新环境变量配置
source /etc/profile

#验证 
java -version

```
- 另附官方推荐的jdk配置方式(https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/)

# 如何查找java安装目录
```shell
which java
ll /usr/bin/java
ll /etc/alternatives/java
```

# 如何查看jvm内存分配情况
```shell
jmap -heap <PID>
```


### doc规范
```java
/**
 * 这是一个示例 {@link #getField(String)} } {@linkplain #fun2 link的描述}
 * @author leon
 * @version 1.0
 * @since 标记从什么(jdk)版本开始支持
 * @deprecated 描述过期原因
 * @see <a href="https://www.oracle.com/cn/technical-resources/articles/java/javadoc-tool.html">官方文档</a>
 * @see <a href="https://blog.csdn.net/huangsiqian/article/details/82818211">详细的个人总结</a>
 */
public class Doc {

    /** 这是一个常量 {@value} */
    private static int FINAL_VALUE = 1;

    /** asdfasdf */
    private String field;

    /**
     * 第一个方法 {@link #fun2} {@linkplain #fun2 link的描述}
     * @author leon
     * @since 标记从什么(jdk)版本开始支持
     * @deprecated 描述过期原因
     * @see Doc  可以参考这里
     * @param p 描述参数
     * @return 描述返回值
     */
    public String getField(String p) throws RuntimeException {
        return field;
    }

    /**
     * {{@docRoot asdfasdf}}
     * {@literal Doc#field}
     * <pre>
     *     第二个方法
     *     自由样式
     *     {@value #FINAL_VALUE}
     * </pre>
     *
     */
    public void fun2() {
        // 这条注释
    }
}
```

### stream示例:
```java
// list -> map, key 是field , value 是obj, 注意, key不能重复,否则异常
Map<String, Obj> map = source.stream().collect(Collectors.toMap(Obj::getField, entity -> entity));
// list -> map, key 是field , value 是obj, obj根据xx字段倒序(且xx!=null优先级高于=null), 取第一条记录
Map<String, Obj> map = source.stream().collect(
        Collectors.toMap(
            Obj::getField,
            Function.identity(),
            (o1, o2) -> 
                    (o1.getXx() == null||o2.getXx() == null)
                    || o1.getXx().compareTo(o2.getXx()) > 0 ? o1 : o2
        )
);


// list -> mapList,key 是field, value是List<Obj>
Map<String, List<Obj>> map = source.stream().collect(Collectors.groupingBy(Obj::getField));
Map<String, List<Obj>> map2 = source.stream().collect(Collectors.groupingBy(dto -> dto.getOrgCode()+"#"+dto.getSupCustCode()));


// sort排序, 根据getBookingTime升序
CollUtil.sort(list, Comparator.comparing(CssWorkBooking::getBookingTime));


// list拆分
public static void main(String[] args) {
    List<String> list = new ArrayList<>();
    list.add("1");
    list.add("2");
    list.add("1");
    list.add("4");
    list.add("5");
    list.add("6");

    int pageSize = 2;
    Stream.iterate(0, n -> n + 1)
            .limit((list.size() + pageSize - 1) / pageSize)
            .forEach(pageNo -> {
                List splitList = list.parallelStream().skip(pageNo * pageSize).limit(pageSize).collect(Collectors.toList());
                System.err.println(pageNo + "->" + splitList);
            });
}
```

----------------

# JMockit使用[参考文档](http://jmockit.cn/index.htm)

## 两个注意点:
1. 这种方式只能mock实现类,无法mock接口
2. SaleOrderQueryBaseServiceImpl这个类的其他方法正常使用(代理了被mock的方法)
```java
private void mock1(Map<String, OrderSale> orderMap) {
    new MockUp<SaleOrderQueryBaseServiceImpl>(SaleOrderQueryBaseServiceImpl.class) {
        @Mock
        public OrderSale selectById(String orderId) {
            return orderMap.get(orderId);
        }
        @Mock
        public List<OrderSale> selectByIds(Collection<String> orderIds, OrderChannel orderChannel){
            List<OrderSale> result = Lists.newArrayList();
            orderIds.forEach(orderId-> result.add(orderMap.get(orderId)));
            return result;
        }
    };
}
```

## 有另外一种mock工具 名字: PowerMock