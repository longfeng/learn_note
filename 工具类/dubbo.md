#
## 调用指定消费者
- 语法 在`<dubbo>`标签中增加属性`url="dubbo://服务所在服务器IP:dubbo服务提供者定义的服务端口"`
  - url="dubbo://10.12.194.110:59900"
  - url="dubbo://172.28.144.36:20886"
```xml 
<dubbo:reference id="serviceA" interface="com.XXX.sdk.ServiceA"  url="dubbo://10.12.194.110:59900" />
```

## idea中使用@Autowired注解对dubbo服务注入提示错误(编译和运行无问题) `Could not autowire. No beans of 'xxx' type found.`
- 原因: idea对dubbo支持的不够
- 方法1: (不推介)直接将提示去掉,容易出现在运行时才能发现的服务无法注入的bug
- 方法2:
  - 使用`@Reference`替代`@Autowired`
  - 在使用`<dubbo:annotation package="com.xxx"/>`扫描`@Reference`所在的类
  - 会引发新的报警: the private field 'xxx' is never assigned(可用alt+回车可以消除,不影响注入检测)

## dubbo配置文件的的xsd
```xml
<beans
  xmlns="http://www.springframework.org/schema/beans"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
  xsi:schemaLocation="http://www.springframework.org/schema/beans
    http://www.springframework.org/schema/beans/spring-beans.xsd
    http://dubbo.apache.org/schema/dubbo
    http://dubbo.apache.org/schema/dubbo/dubbo.xsd"
>
```
## [Dubbo 外部化配置](https://mercyblitz.github.io/2018/01/18/Dubbo-%E5%A4%96%E9%83%A8%E5%8C%96%E9%85%8D%E7%BD%AE/)

## dubbo admin
- 2.5.x(https://blog.csdn.net/fly910905/article/details/86212400)
  - 下载地址:https://github.com/apache/dubbo/tree/2.5.x
  - 下载完成后, 修改zookeeper地址(dubbo.properties文件), 再编译, 最后丢到tomcat中运行, 
  - 访问地址:http://localhost:8080/dubbo-admin-2.5.10/, 用户名密码:root/root
- 3.x 看官网地址(http://dubbo.incubator.apache.org/zh/docs/v2.7/admin/install/admin-console/)