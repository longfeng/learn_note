# elastic-job-lite-console-3.0.0安装
|深度|期望|达成|时间|
|---|---|---|---|
|了解|<input type="checkbox" checked/>|<input type="checkbox" checked/>|2022-03-01|
|demo|<input type="checkbox" checked/>|<input type="checkbox" checked/>|2022-03-01|
|总结|<input type="checkbox" />|<input type="checkbox" />||

# 下载到/apps目录下
cd /usr/local/src/
wget http://cmms3.meicloud.com/file/install_package/elastic-job-lite-console-3.0.0.M1-SNAPSHOT.tar.gz

# 安装
tar -xvf elastic-job-lite-console-3.0.0.M1-SNAPSHOT.tar.gz

mv elastic-job-lite-console-3.0.0.M1-SNAPSHOT /usr/local/elastic-job-lite-console-3.0.0.M1

cd /usr/local/elastic-job-lite-console-3.0.0.M1
# 启动
nohup /usr/local/elastic-job-lite-console-3.0.0.M1/bin/start.sh &

firewall-cmd --zone=public --add-port=8899/tcp --permanent
firewall-cmd --reload


# 注意
# 登录账号密码默认是 root/root, 可以在 ./conf/auth.properties 下修改
# 添加连接的时候, 要记得填写命名空间, 命名空间就是配置文件里的 elaticjob.zookeeper.namespace的配置, 我们系统一般是 elastc-job
