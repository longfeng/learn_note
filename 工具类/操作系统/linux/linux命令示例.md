#常用场景与示例

## 下载linux应用的网站
https://pkgs.org

## 查看centos版本号
`cat  /etc/redhat-release`

## 关于x86、x86_64/x64、amd64和arm64/aarch64
https://blog.csdn.net/newbeixue/article/details/112211012

## 新增一个用户并给用户分配某些目录的访问权限与某些文件的执行权限
### 步骤
- 系统默认的权限是775, [参考权限说明](https://www.cnblogs.com/geekdc/p/5497919.html)
```shell
相关
# 创建一个用户组
groupadd css_read
# 创建一个新用户:css_root,并添加到root用户组(已存在的会报错)
useradd -g root 
# 将old_user(已存在的用户)加入到用户组
usermod -G root old_user
# 查看root用户组下的所有用户
grep 'root' /etc/group
# 为新用户设置密码, 中间有两次是输入密码的过程
# 最终会提示 all authentication tokens updated successfully.
passwd css_read

# 修改/home/logs所有目录与子目录的 775权限
chmod 775 -R /home/logs

# 删除用户
userdel -r css_root
```
- 如果你是打算为生产环境做权限管理,请参考下文
```shell
# 创建一个普通用户
useradd css_read
# 设置密码
passwd css_read

# 将生产的目录权限设置为754
# 拥有者可写,用户组可执行,其他可读
chmod 754 -R /home/
```
---

## 如何配置linux免密登录,跳板机
### 步骤
- 假设A 192.168.200.100, 假设B 192.168.200.101, 需要在A机器上免密登录到B
- 在A机器上执行`ssh-keygen -t rsa`(提示key时连续回车就可以了)
- 在A机器上执行`ssh-copy-id -i ~/.ssh/id_rsa.pub 192.168.200.101`
    - 中途会需要输入B机器的密码
- 原理:A要免密码登录到B，B首先要拥有A的公钥，然后B要做一次加密验证
- [参考资料](https://blog.csdn.net/u013415591/article/details/81943189)
- 问题1: -bash: ssh-copy-id: 未找到命令
  - 依次尝试 [参考资料](https://www.jianshu.com/p/799ad5a6c571) 中的步骤 
---

## 开启防火墙(FirewallD is not running)
```shell
systemctl start firewalld
# systemctl status firewalld
# firewall-cmd --zone=public --add-port=80/tcp --permanent
# firewall-cmd --reload
```

## 批量删除文件
```shell
# 删除当前目录下的所有logs子目录
find */logs -exec rm -rf {} +
# 删除当前目录下的所有jar包
find */*.jar -exec rm -rf {} +

```

## 解压和解压缩
```shell
#### tar
#tar zcf [压缩后的文件] [被压缩的文件]
tar -zcf test.gz test.txt
# 压缩ecu-pk目录, 排除掉所有.log文件, 排除掉logs目录(不管logs在哪个层级)
tar -zcf test.gz test --exclude=*.log --exclude=logs
# 压缩test目录, 排除掉所有.log文件, 排除掉test目录下的logs目录(仅排除一个目录)
tar -zcf test.gz test --exclude=*.log --exclude=test/logs
#解压到当前文件夹
tar [-zxvf] 压缩文件 [-C 解压目录]

#### gz
# 解压.gz文件
gzip -dk demo.gz

#### zip
#压缩zip
#解压zip [文件路径] [-d] [解压路径]
unzip -n test.zip -d /tmp

```

## 下载文件[wget](https://www.cnblogs.com/sx66/p/11887022.html)
```shell
wget https://www.test.com/tset.jar
# 需要用户名密码鉴权
wget --http-user=user --http-passwd=123465 https://www.test.com/tset.jar
# 覆盖同名文件(默认不覆盖)
wget -N https://www.test.com/tset.jar
# 屏蔽下载进度,但是保留错误信息
wget -nv https://www.test.com/tset.jar
```

## 修改密码
`passwd` 根据提示输入新密码,回车(重复两次)

## 设置开机启动
### 方式1(已过时,不推荐): 在/etc/rc.d/rc.local文件中增加执行脚本(/etc/rc.d/rc.local的软连接指向/etc/rc.local)
- 步骤:
  - chmod +x /etc/rc.d/rc.local
    > centos7开始此文件默认开机不加载, 所以需要先将此文件设置为可执行文件
  - 在/etc/rc.d/rc.local文件末尾追加开机启动命令
    - 建议将命令的启动日志写入到文件中, 免得要调试很久`nohup /home/service/x.sh >>/home/start.log 2>&1 &
- 脚本demo
```shell
#!/bin/bash
# THIS FILE IS ADDED FOR COMPATIBILITY PURPOSES
#
# It is highly advisable to create own systemd services or udev rules
# to run scripts during boot instead of using this file.
#
# In contrast to previous versions due to parallel execution during boot
# this script will NOT be run after all other services.
#
# Please note that you must run 'chmod +x /etc/rc.d/rc.local' to ensure
# that this script will be executed during boot.

touch /var/lock/subsys/local

# 加载所有环境变量, 不清楚副作用
source /etc/profile

# 启动任务
# /home/service/A.sh

# 建议后台启动, 这样不会因为某个任务挂起导致系统启动失败
nohup /home/service/B.sh >/dev/null 2>&1 &
```184
### 方式2(不推荐, 会受source命令的影响): 将脚本copy到/etc/profile.d/目录下
- 原理: 因为/etc/profile文件的最后一段循环执行了profile.d目录下的所有脚本
- 步骤: 将脚本copy或者创建软连接到profile.d目录下
  - 创建软连 `ln -s source.sh /etc/profile.d/target.sh`
- 疑问: source /etc/profile时脚本是否会被执行?
  - 答案: 会
### 方式3: 以chkconfig的方式管理
- 备注: chkconfig主要还是基于init.d的方式管理服务
- 步骤:
  - 脚本必须携带如下文件头, 否则chkconfig不支持.
    - `#!/bin/sh` 声明系统使用的shell
    - `#chkconfig: 35 20 80`  默认用这个就好
      - 第1个参数表示:等级代号(level)
      - 第2个参数表示:启动优先权
      - 第3个参数表示:关闭优先权
    - `#description: desc`  描述
  - 将`脚本copy或者创建软连`接到`/etc/init.d/或者/etc/rc.d/init.d/`目录下。
  - 为脚本增加可执行权限 `chmod +x /etc/rc.d/init.d/test.sh`
  - 将脚本添加到服务 `chkconfig --add /etc/rc.d/init.d/test.sh`
  - 设置脚本为开机启动 `chkconfig test.sh on`
  - 查看服务是否添加成功 `chkconfig --list`
  - 取消脚本为开机启动 `chkconfig test.sh off`
- 脚本demo:
```shell
#!/bin/sh
#chkconfig: 35 20 80
#description: test
date >>/home/logs/startlog/start.log 2>&1 &
```
### 方式4(推荐): 以systemctl的方式管理, 
- 备注: systemctl可以理解成(chkconfig和service)的合集
- 步骤:
  - 在`/usr/lib/systemd/system`目录下增加服务描述文件, 通常约定 xxx.service, 分为三部分
    -`Unit` 服务的说明
      - Description:描述服务
      - After:描述服务类别
    -`Service` 服务运行参数的设置[参考](https://www.jianshu.com/p/3dd6b57a16bf)
      - Type=forking      进程的运行方式(forking表示以后台方式运行)
      - ExecStart         服务启动命令(绝对路径)
      - ExecReload        服务重启命令(绝对路径)
      - ExecStop          服务停止命令(绝对路径)
    -`Install`  用来定义如何启动，以及是否开机启动
      - WantedBy=multi-user.target 
  - 设置开机启动 `systemctl enable xxx.service`
  - 取消开机启动 `systemctl disable xxx.service`
  - 查看是否开机启动 `systemctl is-enabled xxx.service`
  - 启动服务 `systemctl start xxx.service`
  - 停止服务 `systemctl start xxx.service`
  - 重启服务 `systemctl restart xxx.service`
  - 查看服务状态 `systemctl status xxx.service`
  - 查看所有服务 `systemctl list-units --type=service`
- 服务描述文件demo
```shell
[Unit]
Description=pk
[Service]
Type=forking
ExecStart=/home/service/ecu-pk/deploy-ecu-pk-impl.sh start
ExecReload=/home/service/ecu-pk/deploy-ecu-pk-impl.sh restart
ExecStop=/home/service/ecu-pk/deploy-ecu-pk-impl.sh stop
[Install]
WantedBy=multi-user.target
```
- 问题:
  - 提示找不到java命令?或者其他命令(https://serverfault.com/questions/927367/systemctl-java-not-found-when-using-systemctl)
    - 分析:  systemctl只会加载`/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`这些路径的环境, 因此需要将报错的命令通过软连接的方式加入到这些目录中, 建议加入到/usr/bin目录下
    - `ln -s $JAVA_HOME/bin/java /usr/bin/java`

## 如何配置ssh代理
### 配置方式
- 阿里云rds的ssh代理只需配置本机mysql连使用内网地址以及加上ssh, 不需要在堡垒机上配置
- 适合机器之间的代理, 示例:(https://www.cnblogs.com/toughlife/p/5653669.html)

## 找到线程所在的目录
```shell
ll /proc/17681/cwd

pwdx 17681

which nginx

whereis nginx
```

## 查找内容
```shell
# 查找内容
grep "要找查找的文本" xxx.log
# 批量查找内容
grep -rn "mobile_business_servers" ./
# 批量替换
sed -i "s/要找查找的文本/替换后的文本/g" grep -rl "要找查找的文本" ./
sed -i "s/901414/22334e/g" grep -rl "901414" ./
sed -i "s/#A82020/#215db0/g" grep -rl "#A82020" /usr/tomcat/webapps/ccs-web/resource/static/css/frame/fr


sed -i "s/#901414/#22334e/g" `grep "#901414" -rl /usr/tomcat/webapps/ccs-web/resource/static/css/frame`
sed -i "s/#A82020/#215DB0/g" `grep "#A82020" -rl /usr/tomcat/webapps/ccs-web/resource/static/css/frame`
sed -i "s/#951F1F/#22334e/g" `grep "#951F1F" -rl /usr/tomcat/webapps/ccs-web/resource/static/css/frame`


sed -i "s/#901414/#22334e/g" `grep "#901414" -rl /usr/tomcat/webapps/ccs-web/resource/static/ligerUI/lib/ligerUI/skins/Aqua/css`
sed -i "s/#a82020/#215db0/g" `grep "#a82020" -rl /usr/tomcat/webapps/ccs-web/resource/static/ligerUI/lib/ligerUI/skins/Aqua/css`


sed -i "s/旧内容/新内容/g" `grep 旧内容 -rl 文件夹路径`
#901414 -> #22334e
#A82020 -> #215db0

```


## 磁盘相关
```shell
# 查看所有磁盘占用情况
df -h
# 查看当前目录占用情况
du -h 
# 查看某个目录下最大的5个文件夹
du -h /home | sort -h | tail -n 5
# 查看某个目录下最大的5个文件
find /home/logs -type f -printf '%s %p\n' | sort -nr | head -10
# 删除某个目录下`framework_lib`开头的文件
find /home/logs/ -type f -name 'framework_lib*' -exec rm {} \;

```

du -h / | sort -h | tail -n 5
du -h /data | sort -h | tail -n 20


du -h /home/data | sort -h | tail -n 5