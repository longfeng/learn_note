# 安装Ubuntu
## 前置条件
- [清华大学镜像站](https://mirrors.tuna.tsinghua.edu.cn/ubuntu-releases/HEADER.html)
  - 进入目录索引后, 点击HEADER.html就会得到上面的页面
- 关于版本
  - LTS版本: 长期支持版本
  - 20.xx/21.xx/22.xx 前面的数字表示年份, 后面的表示版本
  - 桌面版: 提供GUI能力
  - 服务器版: 针对服务器的一些特性优化, 服务器建议选择此版本

## 使用U盘安装
### 步骤:
- U盘安装[Ventoy](https://www.ventoy.net/), 会格式化U盘
- 将ISO镜像复制到u盘中
- 选择从u盘启动
- 进入安装
- 选择语言
- ...
- 选择硬盘(此处拔掉upan?)
- 选择 install openSSH server (否则无法ssh登录)
- 输入用户名(root用户默认禁止,如果需要使用到root权限,通过sudo)
- 安装完成后
### 系统配置
- 配置网卡(网卡默认开启DHCP)
  - 网卡配置如下, 通过命令`sudo nano /etc/netplan/01-netcfg.yaml` 其中`01-netcfg.yaml`这个名称是随机的
  ```yaml
  network:
    version: 2
    ethernets:
      eth0:
        dhcp4: no
        addresses:
          - 192.168.1.10/24    # 设置您的IP地址和子网掩码
        gateway4: 192.168.1.1  # 设置网关IP地址
        nameservers:
          addresses:
            - 8.8.8.8          # 设置主DNS服务器
            - 8.8.4.4          # 设置备用DNS服务器
  ```
  - 配置完成后使用`sudo netplan try`检查配置是否生效, 如果语法错误,则可能导致网卡无法启动(物理灯都灭了)
  - 最后使用`sudo netplan apply`应用新的网络配置
  - 检查网络是否生效`ping www.baidu.com`

## 在虚拟机安装(略过...)


## 其他问题

