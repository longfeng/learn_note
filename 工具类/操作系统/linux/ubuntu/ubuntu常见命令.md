## 磁盘相关
### 查看磁盘: `lsblk`
- 名词解释: 磁盘? 分区? LVM? PV? VG? LV?
  - 磁盘：物理存储设备，可以包含一个或多个分区或被整体用作物理卷。
  - 分区：磁盘上的逻辑划分，可以被传统文件系统或LVM使用。
  - LVM: 是逻辑卷管理工具, 安装完成后, 可以将分区设置为逻辑卷(可动态调整), 画外音: `为了支持动态调整, 加了两层扩展(物理卷和卷组)` 
  - 物理卷（PV）：LVM管理的基本存储单元，可以是磁盘或分区。      画外音:`就是被LVM管理的分区`
  - 卷组（VG）：由一个或多个物理卷组成的存储池，提供给逻辑卷使用。 画外音:`可以将多个物理卷组合成卷组, 这样可以打破硬盘的物理界限, 使用者当做一块完整的硬盘`
  - 逻辑卷（LV）：在卷组上创建的虚拟分区，用于存储数据。         画外音:`有了物理卷和卷组的铺垫, 就可以在他们的基础上动态调整分区了, 他就类似windows系统的逻辑分区`
```
plaintext
NAME                      MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
loop0                       7:0    0  74.2M  1 loop /snap/core22/1380
loop1                       7:1    0 130.1M  1 loop /snap/docker/2915
loop2                       7:2    0  38.8M  1 loop /snap/snapd/21759
nvme0n1                   259:0    0 953.9G  0 disk
├─nvme0n1p1               259:1    0     1G  0 part /boot/efi
├─nvme0n1p2               259:2    0     2G  0 part /boot
└─nvme0n1p3               259:3    0 950.8G  0 part
  └─ubuntu--vg-ubuntu--lv  252:0    0   100G  0 lvm  /
分析内容
loop设备:

loop0，loop1 和 loop2 是 loopback 设备，通常用于挂载镜像文件或者 Snap 包。
loop0 挂载在 /snap/core22/1380，大小为 74.2M。
loop1 挂载在 /snap/docker/2915，大小为 130.1M。
loop2 挂载在 /snap/snapd/21759，大小为 38.8M。
NVMe SSD (nvme0n1):

nvme0n1 是一块 NVMe 固态硬盘，大小为 953.9G。
nvme0n1p1 是这个硬盘的第一个分区，大小为 1G，挂载在 /boot/efi，通常是 EFI 系统分区，用于存储启动加载器和启动相关文件。
nvme0n1p2 是第二个分区，大小为 2G，挂载在 /boot，用于存储内核和其他启动文件。
nvme0n1p3 是第三个分区，大小为 950.8G，没有直接挂载点。这个分区被进一步分成了一个逻辑卷管理器 (LVM) 分区 ubuntu--vg-ubuntu--lv。
LVM 分区 (ubuntu--vg-ubuntu--lv):

ubuntu--vg-ubuntu--lv 是在 nvme0n1p3 上创建的逻辑卷，大小为 100G，挂载在根目录 /。LVM 提供了更灵活的磁盘管理方式，可以动态调整分区大小等。

总结:
有一块硬盘:nvme0n1, 容量为:953.9G, 它有三个(主)分区
    分区1: nvme0n1p1, 类型为 EFI 系统分区，大小为 1G
    分区2: nvme0n1p2, 类型为 启动分区，大小为 2G
    分区3: nvme0n1p3, 类型为 普通分区, 大小为 950.8G, 它的分配如下
        LVM分区1: ubuntu--vg-ubuntu--lv, 类型为LVM, 挂载点为/, 大小为 100G
```
### LVM相关
#### 物理卷（Physical Volume, PV)
- 查看物理卷
```
leon@leon-server:~$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/nvme0n1p3              // PV的名称
  VG Name               ubuntu-vg                   // 所属的VG
  PV Size               <950.82 GiB / not usable 0  // PV大小, 其中 "not usable 0" 表示没有不可用的空间
  Allocatable           yes         // 表示这个物理卷是可分配的（即可以从这个物理卷中分配物理扩展）
  PE Size               4.00 MiB    // 物理扩展（Physical Extent, PE）的大小是 4.00 MiB。物理卷是由多个PE组成的，每个PE的大小都是相同的。 画外音:可以理解成物理卷中的区块大小
  Total PE              243409      // PE总数
  Free PE               217809      // 可用PE数
  Allocated PE          25600       // 已分配PE数
  PV UUID               HqltKH-BzSH-zAsn-DM3V-RtB0-MMpD-Y73tbm\   // PV的ID
```
- 创建物理卷(待补充)
#### 卷组（Volume Group, VG）
- 查看卷组
```
leon@leon-server:~$ sudo vgdisplay
  --- Volume group ---
  VG Name               ubuntu-vg     // VG的名称
  System ID             
  Format                lvm2          // 格式
  Metadata Areas        1             // 元数据区域
  Metadata Sequence No  2             // 元数据序列
  VG Access             read/write    // VG的(读/写)权限
  VG Status             resizable     // VG的状态(resizable:可调整大小)
  MAX LV                0             // 最大LV数量(0表示不限制)
  Cur LV                1             // 当前LV数量
  Open LV               1             // 打开的LV数量, 画外音:可理解为使用中的LV数量
  Max PV                0             // 最大PV数量, 画外音:可以让多个物理卷组合成一个卷组
  Cur PV                1             // 当前PV数量
  Act PV                1             // 活动的PV数量, 画外音:可以理解为可用的PV数量
  VG Size               <950.82 GiB   // 当前卷组大小
  PE Size               4.00 MiB      // PE空间（Physical Extent, PE）, 画外音:可以理解成物理卷中的区块大小
  Total PE              243409        // 总PE大小
  Alloc PE / Size       25600 / 100.00 GiB   // 已分配PE大小
  Free  PE / Size       217809 / <850.82 GiB // 可分配PE大小
  VG UUID               L1KV6v-Uktb-x7mb-d7r4-zGfJ-8V4i-HQgZgC // VG的ID
```
- 创建卷组(待补充)
#### 逻辑卷（Logical Volume, LV）
- 查看逻辑卷
```
leon@leon-server:~$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/ubuntu-vg/ubuntu-lv                 
  LV Name                ubuntu-lv                                // LV的名称
  VG Name                ubuntu-vg                                // VG的名称
  LV UUID                LYq43C-vAr7-UYqH-6MlN-rmnD-bUol-l00uu1   // LV的ID
  LV Write Access        read/write                               // LV的(读/写)权限
  LV Creation host, time ubuntu-server, 2024-06-17 14:05:28 +0000 // LV的创建(时间等)信息
  LV Status              available                                // LV的状态(available:可用的)
  # open                 1                                        // 打开的逻辑卷数量(?不知道啥意思)
  LV Size                100.00 GiB                               // LV的大小, 一个PE=4MB, 当前LV有25600个LE, 因此LV大小为25600*4=102400MB=100GB
  Current LE             25600                                    // 当前LE（Logical Extent, LE）, 画外音:可以理解成逻辑卷中的区块大小, 每个LE对应一个PE
  Segments               1                                        // 逻辑卷的段（Segment）数量为1。每个段表示逻辑卷的一个连续区域, 画外音:当前LV属于连续的物理空间
  Allocation             inherit                                  // 逻辑卷的分配策略是继承（inherit），表示从卷组继承分配策略
  Read ahead sectors     auto                                     // 逻辑卷的预读扇区设置为自动（auto）
  - currently set to     256                                      // 当前的预读扇区数设置为256
  Block device           252:0                                    // 逻辑卷对应的块设备号是 252:0
```
- 创建逻辑卷: 在`ubuntu-vg`这个卷组下创建一个名称为`v001`逻辑卷, 且大小为`200G`, 注意还需要挂载才能使用
```
# 创建逻辑卷
sudo lvcreate -L 200G -n v001 ubuntu-vg
## --- 接下来挂载到目录/data/package ---
# 先格式化逻辑卷
sudo mkfs.ext4 /dev/ubuntu-vg/v001
# 创建/data/package
sudo mkdir -p /data/package
# 永久挂载-编辑 fstab 增加一行 /dev/ubuntu-vg/v001 /data/package ext4 defaults 0 2
sudo cp /etc/fstab /etc/fstab.bak
sudo vim /etc/fstab
# 验证是否挂载成功, 可能会提示你: mount: (hint) your fstab has been modified, but systemd still uses the old version; use 'systemctl daemon-reload' to reload.
sudo mount -a
# 重启后验证
lsblk
```
- 删除逻辑卷: 删除上面创建的`v001`的逻辑卷, 注意需要先删除挂载点
```
# 查看逻辑卷信息,得到逻辑卷的名称
sudo lvdisplay|grep v001
# 查看挂载点
lsblk
# 卸载挂载点
sudo umount /data/package
# 如果是永久挂载(则还需要删除fstab中的挂载信息)
sudo cp /etc/fstab /etc/fstab.bak
sudo vim /etc/fstab
# 删除逻辑卷
sudo lvremove /dev/ubuntu-vg/v001
```

## 系统设置
### 配置网卡(网卡默认开启DHCP)
- 网卡配置如下, 通过命令`sudo nano /etc/netplan/01-netcfg.yaml` 其中`01-netcfg.yaml`这个名称是随机的
```yaml
network:
  version: 2
  ethernets:
    eth0:
      dhcp4: no
      addresses:
        - 192.168.1.10/24    # 设置您的IP地址和子网掩码
      gateway4: 192.168.1.1  # 设置网关IP地址
      nameservers:
        addresses:
          - 8.8.8.8          # 设置主DNS服务器
          - 8.8.4.4          # 设置备用DNS服务器
```
- 配置完成后使用`sudo netplan try`检查配置是否生效, 如果语法错误,则可能导致网卡无法启动(物理灯都灭了)
- 最后使用`sudo netplan apply`应用新的网络配置
- 检查网络是否生效`ping www.baidu.com`
### 设置时区
```
# 编辑文件
sudo vi /etc/locale.gen
# 取消注释 en_DK.UTF-8 UTF-8
sudo locale-gen
# 更新时区
sudo update-locale LC_TIME="en_DK.UTF-8"
# 重启系统时间
sudo systemctl restart systemd-timedated
```


