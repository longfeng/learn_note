shell
### 文件开头的约定,告诉系统使用哪个执行器(Shell)来解释这个脚本
- `#!/bin/bash`

- 可以直接在shell脚本中运行命令
  - 例如 运行`ls`命令
  ```shell
  #!/bin/bash
  ls
  ```

### 注释
  - [推荐]单行注释 `#`
  - 多行注释 `:<<EOF`开头 `EOF`结束
  
### 作用域
  - 局部变量 当前shell中定义的变量作用域为当前脚本
  - 环境变量 可以在脚本中引用系统环境变量, 当然也可以设置系统环境变量(固定语法)
  - 不需要也可以使用;作为执行结束
  
### 变量
- 思路的转变
  - 没有类型的概念
  - 关键字不可设置为变量,可用help命令查看保留关键字

- 变量赋值 (***=号两边不能有空格***)
  - 常规
    - `intVar=123`
    - `string="value"`
  - 隐式赋值
    - 下目录的文件名循环出来: `for file in $(ls /etc)`f
  - 只读变量
    - `readonly intVar=111`
- 使用变量
  - [推荐]`${intVar}`
  - `$intVar`
  
### 字符串
  - 定义
    - [推荐]双引号
      - 可以表示一段字符串,且可以嵌入变量
    - 单引号
      - 可以表示一段字符串
      - 不可转义单引号
  - 拼接
    - `echo "这句话是连" "起来读的!"`
    - 结果:这句话是连起来读的!
  - 获取长度
    ```shell
    string="abc"
    echo ${#string}
    #结果:4
    ```
  - 提取字符串(第2个字符开始截取3个字符)
    ```shell
    string = "abcde"
    echo ${string:1:3}
    ```
  - 获取字符所属位置(类似indexOf)
    ```shell
    string = "abcd"
    # 注意用的是`
    echo "长度是:"`expr index "${string}" "bc"`
    # 结果:长度是:2
    ```
### 数组
```shell
# 使用逗号或者空格分割, 可以分多行
arr=("str",123,456)

# 获取数组第1个元素,
echo ${arr[1]}

# 获取数组所有元素,也可以用@替代*
echo ${arr[*]}

# 获取数组的长度
echo ${#arr[*]}

# 获取数组第1个元素的长度
echo ${#arr[0]}

```
### 数组 shell不定参数数组接收
```shell
warNameArr=()
count=0
while [ "$#" -ge "1" ];do
  warNameArr[$count]=$1
  let count=count+1
  shift
done

for warName in ${warNameArr[@]}
do
  echo "参数:${warName}"
done
```

### 输入 参数
- 在脚本外输入参数的方式 `sh demo.sh param1 param2`

```shell

# 脚本内获取输入的第1个参数: $下标
echo $0

# 参数个数
echo $#

# 所有参数, 以字符串的方式拼接
echo $*

# 运行当前脚本的进程PID
echo $$

# 判断当前脚本中上一次执行的命令是否正确
ls

# 判断上面这个ls是否成功, 0 表示成功
echo $?
```
### [运算符](https://www.runoob.com/linux/linux-shell-basic-operators.html)

- 算数运算符
```shell
#### 符号两端必须要有空格!!!!
# + 加法
`expr $a + $b`
# - 减法
`expr $a - $b`
# * 乘法
`expr $a \* $b`
# /	除法
`expr $b / $a`
# % 取余
`expr $b % $a`
# == 相等
[ $a == $b ]
# != 不相等
[ $a != $b ]
#### 符号两端必须要有空格!!!!
```
```shell
a=10
b=20

val=`expr $a + $b`
echo "a + b : $val"

val=`expr $a - $b`
echo "a - b : $val"

val=`expr $a \* $b`
echo "a * b : $val"

val=`expr $b / $a`
echo "b / a : $val"

val=`expr $b % $a`
echo "b % a : $val"

if [ $a == $b ]
then
   echo "a 等于 b"
fi
if [ $a != $b ]
then
   echo "a 不等于 b"
fi
```
- 关系运算符
```shell
a=10
b=20

if [ $a -eq $b ]
then
   echo "$a -eq $b : a 等于 b"
else
   echo "$a -eq $b: a 不等于 b"
fi
if [ $a -ne $b ]
then
   echo "$a -ne $b: a 不等于 b"
else
   echo "$a -ne $b : a 等于 b"
fi
if [ $a -gt $b ]
then
   echo "$a -gt $b: a 大于 b"
else
   echo "$a -gt $b: a 不大于 b"
fi
if [ $a -lt $b ]
then
   echo "$a -lt $b: a 小于 b"
else
   echo "$a -lt $b: a 不小于 b"
fi
if [ $a -ge $b ]
then
   echo "$a -ge $b: a 大于或等于 b"
else
   echo "$a -ge $b: a 小于 b"
fi
if [ $a -le $b ]
then
   echo "$a -le $b: a 小于或等于 b"
else
   echo "$a -le $b: a 大于 b"
fi
```
- 布尔运算符
```shell
a=10
b=20

if [ $a != $b ]
then
echo "$a != $b : a 不等于 b"
else
echo "$a == $b: a 等于 b"
fi
if [ $a -lt 100 -a $b -gt 15 ]
then
echo "$a 小于 100 且 $b 大于 15 : 返回 true"
else
echo "$a 小于 100 且 $b 大于 15 : 返回 false"
fi
if [ $a -lt 100 -o $b -gt 100 ]
then
echo "$a 小于 100 或 $b 大于 100 : 返回 true"
else
echo "$a 小于 100 或 $b 大于 100 : 返回 false"
fi
if [ $a -lt 5 -o $b -gt 100 ]
then
echo "$a 小于 5 或 $b 大于 100 : 返回 true"
else
echo "$a 小于 5 或 $b 大于 100 : 返回 false"
fi
```
- 逻辑运算符
```shell
a=10
b=20

if [[ $a -lt 100 && $b -gt 100 ]]
then
   echo "返回 true"
else
   echo "返回 false"
fi

if [[ $a -lt 100 || $b -gt 100 ]]
then
   echo "返回 true"
else
   echo "返回 false"
fi
```

- 字符串运算符
```shell
a="abc"
b="efg"

if [ $a = $b ]
then
   echo "$a = $b : a 等于 b"
else
   echo "$a = $b: a 不等于 b"
fi
if [ $a != $b ]
then
   echo "$a != $b : a 不等于 b"
else
   echo "$a != $b: a 等于 b"
fi
if [ -z $a ]
then
   echo "-z $a : 字符串长度为 0"
else
   echo "-z $a : 字符串长度不为 0"
fi
if [ -n "$a" ]
then
   echo "-n $a : 字符串长度不为 0"
else
   echo "-n $a : 字符串长度为 0"
fi
if [ $a ]
then
   echo "$a : 字符串不为空"
else
   echo "$a : 字符串为空"
fi
```
 
- 文件测试运算符
```shell
file="/var/www/runoob/test.sh"
if [ -r $file ]
then
   echo "文件可读"
else
   echo "文件不可读"
fi
if [ -w $file ]
then
   echo "文件可写"
else
   echo "文件不可写"
fi
if [ -x $file ]
then
   echo "文件可执行"
else
   echo "文件不可执行"
fi
if [ -f $file ]
then
   echo "文件为普通文件"
else
   echo "文件为特殊文件"
fi
if [ -d $file ]
then
   echo "文件是个目录"
else
   echo "文件不是个目录"
fi
if [ -s $file ]
then
   echo "文件不为空"
else
   echo "文件为空"
fi
if [ -e $file ]
then
   echo "文件存在"
else
   echo "文件不存在"
fi
```

- 输出 linux
  
- 分支
```shell
a=10
b=20
if [ $a == $b ]
then
   echo "a 等于 b"
elif [ $a -gt $b ]
then
   echo "a 大于 b"
elif [ $a -lt $b ]
then
   echo "a 小于 b"
else
   echo "没有符合的条件"
fi
```  
- 循环
```shell
#for
for loop in 1 2 3 4 5
do
    echo "The value is: $loop"
done
#while
i=1 
while (( $i <= 10 )) 
do
  echo $i 
  (( i++ )) 
done


#switch
site="runoob"
case "$site" in
   "runoob") echo "菜鸟教程"
   ;;
   "google") echo "Google 搜索"
   ;;
   "taobao") echo "淘宝网"
   ;;
   *) echo "默认"     
   ;;
esac
```

### Shell 函数
- 参数不需要定义,直接传就好
- 返回值只能是int,且限定范围(0-255), 约定0表示执行成功
- 函数可以看成一种临时命令,调用方式和获取返回值方式是一样的
- 返回值的取值 在调用完成后 使用$?
- 将命令的结果赋值给变量$(command)
```
[ function ] funname [()]
{
    action;
    [return int;]
}
```
```shell
var1=$1
echo "外部传入参数:"$var1

function demo(){
  echo "参数:"$1
  p1=`expr $1 + 1`
  p2=$p1","$2
  echo "预计返回值:"$p2  
  return $(($p2))
}

demo 1 "param2"
echo "实际返回值:"$?

```

### 输出到文件
- 追加到文件 `echo >> f.txt`
- 写入到文件(写入之前会先删除文件的所有内容) `echo > f.txt`

### 从文件输入(按行读取)
```shell
#for循环的方式[推荐]
for line in `cat x.tmp`
do
echo $line
done

echo -------------------------1

#while方式
while read line
do
echo $line
done < x.tmp

echo -------------------------2
#while方式
cat x.tmp | while read line2
do
echo $line2
done

```

### Shell 文件包含
```shell
#使用 . 号来引用test1.sh 文件
. ./test1.sh

# 或者使用以下包含文件代码
source ./test1.sh
```

---
# 其他
### set 用来设置shell的执行方式
  - `set` 不带参数时,用来显示环境变量
  - `set -e` 当命令出错时,退出shell, (默认是只有命令返回值不等于0时才会退出)
    - 注：set +e 表示关闭-e选项，set -e 表示重新打开-e选项。
    - 例如下面这段脚本`test.sh`
      ```shell
      #!/bin/bash
      set -e
      abcefg
      echo "123"
      ```
      - 如果没有-e参数, 执行`sh .test.sh`会报错`test.sh: line 5: test3: command not found\n123`
      - 执行`sh .test.sh` 会输出 `test.sh: line 5: test3: command not found`
  - `set -u` 当命令的参数不存在时则报错, (默认命令参数不存在会继续执行)
    - 例如下面这段脚本`test.sh`
      ```shell
      #!/bin/bash
      set -u
      echo $1
      echo "123"
      ```
      - 执行`sh .test.sh a` 会输出 `a\n123`
      - 执行`sh .test.sh`会报错`$1: unbound variable`
  
# 脚本成果
[项目部署脚本](shell_demo/deploy.sh)
