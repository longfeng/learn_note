
# 查看某批文件, 根据时间倒序取5条记录
ls -lht ccs_supplychain_rpc_sql* | head -n 5

#grep 查看日志
grep "xxx" -[i|B] /data/logs/xxx.log
# -i 不区分大小写
# -B 50 Before 50行
# -A 50 After 50行

# zgrep 从压缩文件中检索日志
zgrep "内容" abc.tar.gz


# 查看当前目录下包含`/api`这个内容的文件, 并且将这个文件的上下5行作为展示
grep -r -C 5 "/api" *
