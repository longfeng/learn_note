## 安装CentOS7 
- 安装时的软件选择
  - 基本环境
    - [建议]基础设施服务器,(不建议最小安装,连ifconfig都没有)
  - 已选环境的附加选项
    - 此选项仅仅是在基本环境的基础上的可选内容,
    - 建议留空,比如java装的是jdk1.7
- 配置阿里云镜像 [阿里云官方镜像站](https://developer.aliyun.com/mirror/)
```shell
# 备份原镜像配置: 
cp /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.bk
# 下载阿里云镜像配置: 备注:因为mirrors.aliyuncs.com不可用,导致大量重试,因此建议`将Centos-7.repo中包含mirrors.aliyuncs.com的行删除`(by 2021-03-04)
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
# 清除缓存:
yum clean all
# 生成缓存:
yum makecache
# 执行命令: - 可能会因为网络原因出现Error,重新执行一次`yum -y update`命令即可
yum -y update
# 安装编译工具及库文件
yum -y install make zlib zlib-devel gcc-c++ libtool  openssl openssl-devel

echo "rmmod pcspkr" >> /etc/rc.d/rc.local
chmod +x /etc/rc.d/rc.local
reboot
  
```


- 关闭蜂鸣器
```shell
echo "rmmod pcspkr" >> /etc/rc.d/rc.local
chmod +x /etc/rc.d/rc.local
reboot
```
- 设置网络,参考常用命令
```shell
cd /etc/sysconfig/network-scripts/

```
- 设置中文man手册
  https://www.jianshu.com/p/185fe0a4f426
  
### 问题
- vm增加了网卡, 但是`/etc/sysconfig/network-scripts/`目录下没有看到对应的网卡
  - 先通过`ifconfig -a`找到缺少的网卡名称,通常是`ensXX` `XX`表示数字
  - 假设这块不识别的网卡名称是`ens34`
  - 在`/etc/sysconfig/network-scripts/`目录下新建一个`ifcfg-ens34`的文件
  ```ifcfg-ensxxx
  TYPE=Ethernet
  PROXY_METHOD=none
  BROWSER_ONLY=no
  DEFROUTE=yes
  IPV4_FAILURE_FATAL=no
  IPV6INIT=no
  ONBOOT=yes
  NAME=ens32
  DEVICE=ens32
  HWADDR=00:0C:29:17:40:C2
  BOOTPROTO=static
  IPADDR=192.168.200.100
  NETMASK=255.255.255.0
  GATEWAY=192.168.200.254
  #DNS1=114.114.114.114
  #DNS2=8.8.8.8
  ```
  - 重启网络服务: `service network restart`
    00:0C:29:17:40:C2
    00:0C:29:17:40:CC

----
### 附件
- ![VMware安装图](../../../source/VMware安装Centos7.png)