#!/bin/bash

# start
function start(){
        echo "starting..."
        i=1
        while (($i > 0));do
          ping oss-cn-hangzhou-internal.aliyuncs.com -c1 >/dev/null
          ping xlm-crm-after-sale-prod.oss-cn-hangzhou-internal.aliyuncs.com -c1 >/dev/null
          sleep 4s
        done
}


# stop
function stop(){
        echo "stopping..."
        pid=`ps -ef|grep refresh_oss_dns |grep start |awk '{print $2}'`
        if [ ! -n "${pid}" ]
                then
                        echo "while_ping.sh not run!"
                else
                        echo "kill ${pid}"
                        kill ${pid}
        fi
        echo "stopped"
}

#进程是否停止
function isStop(){
    pid=$1
    result=`ps -ax | awk '{ print $1 }' | grep -e "^${pid}$"`

    if [ ! -n "${result}" ]
                then
            return 1
            else
                return 0
        fi
}

# 状态
function status(){
  pid=`ps -ef|grep refresh_oss_dns |grep start |awk '{print $2}'`
  if [ ! -n "${pid}" ]
  then
          echo "while_ping.sh status: not run!"
  else
      echo "while_ping.sh status: running, pid:${pid}";
  fi
}


#脚本入口
function main(){
        cmd=$1
        if [ ! -n "${cmd}" ]
                then
                        cmd="status"
        fi

        case ${cmd} in
           "stop")
                        stop
           ;;
           "start")
                        start >>while_ping.log 2>&1 &
           ;;
           "restart")
                        stop
                        echo "----"
                        start >>while_ping.log 2>&1 &
           ;;
           "status")
                        status
           ;;
           *) echo "不支持的命令:${cmd}, [start|stop|restart]"
           ;;
        esac

}

#执行脚本
main $1
