#!/bin/bash
#Author:Leon
#create_ts:2023-07-20

#gitlabBasePath
gitlabBasePath="https://gitlab.gujing.com/gjcloud/gjcloud/raw/uat%23%E6%80%BB%E9%83%A8%E7%9B%B4%E6%8A%95_%E9%A2%86%E7%94%A8_%E9%A2%86%E9%80%80%E5%8D%95/web-parent/ccs-web/WebContent/WEB-INF"

#tomcat home
tomcatHome="/home/tomcat8080/tomcat/webapps/ccs-web/WEB-INF"

#脚本入口
function main(){
	filePath=$1
	if [ ! -n "${filePath}" ]
		then
			echo "文件路径必填,例如:/pages/supplyChain/departReceive/forPoint/outBillList.jsp"
			exit 1
	fi

  filename=$(basename "${filePath}")
  echo "文件名为:${filename}"

	gitlabCookie=$2
	if [ ! -n "${gitlabCookie}" ]
		then
			echo "cookie必填,例如:_gitlab_session=80b4cb524601603b1bf37487f60bd17e"
			exit 1
	fi

  httpCode=$(curl -s -b "${gitlabCookie}" -o /tmp/${filename} -w "%{http_code}" "${gitlabBasePath}${filePath}")

  # 判断状态码
  if [ $httpCode != 200 ]; then
      echo "curl -b \"${gitlabCookie}\" -o /tmp/${filename} \"${gitlabBasePath}${filePath}\""
      echo "curl请求失败"
      exit 1
  fi

  mv -f "/tmp/${filename}" "${tomcatHome}${filePath}"

  echo "cat ${tomcatHome}${filePath}"
  echo "vi ${tomcatHome}${filePath}"

}

#执行脚本
main $1 $2
