#!/bin/bash
#Author:Leon
#create_ts:2022-10-26

# 待部署的war包, 将脚本传参得到
tomcatHome=""
warNameArr=()
paramIndex=0
while [ "$#" -ge "1" ];do
  if [ $paramIndex == 0 ]; then
    tomcatHome=$1
  else
    warNameArr[$paramIndex-1]=$1
  fi

  let paramIndex=$paramIndex+1
  shift
done
# echo ${#warNameArr[*]}
echo "tomcatHome:${tomcatHome}"
if [ ${#warNameArr[*]} == 0 ]
then
  echo "待部署的war包为空, 请通过参数传递过来, 以空格隔开, 示例 deploy.sh /home/tomcat8081 a.war b.war "
  exit 1
fi
