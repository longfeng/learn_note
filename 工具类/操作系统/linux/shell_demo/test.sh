#!/bin/bash
#Author:Leon
#create_ts:2022-08-10

#warSourcePath
warSourcePath="/home/deploy/webapps"
#tomcat home
tomcatHome="/home/tomcat8082"
#tomcat home
tomcatWebapps="${tomcatHome}/webapps"

# 主方法
function shutdownTomcat(){
  echo "shutdownTomcat"
  stopProgram `basename ${tomcatHome}` "${tomcatHome}/bin/shutdown.sh" 60
}

function startTomcat(){
  echo "startTomcat"
  sh ${tomcatHome}/bin/startup.sh
}

# 删除war包并且copyWar包
function cleanAndCopyWar(){
  echo "cleanAndCopyWar"
   rm -rf ${tomcatWebapps}
   mkdir ${tomcatWebapps}
   mvWarToWebapps ${warSourcePath}/base_rpc.war
   mvWarToWebapps ${warSourcePath}/finance_rpc.war
   mvWarToWebapps ${warSourcePath}/interfaces_rpc.war
}
# 主方法 end


#### 工具方法
# 移动并检查
function mvWarToWebapps(){
  warPath=$1
  echo "mvWarToWebapps ${warPath} ${tomcatWebapps}"
  fileNotExistsThrow ${warPath} "[${warPath}]文件不存在, 请等待ci/cd执行完成再部署!"
  cp ${warPath}  ${tomcatWebapps}
}

# 文件不存在时抛异常
function fileNotExistsThrow(){
	if [ ! -f "$1" ]; then
		echo $2
		exit 1
	fi
}


#尝试正常关闭程序,若超时未关闭,则kill-9
function stopProgram(){
  #程序名称
  programName=$1
  #正常关闭脚本
  shutdownSh=$2
  #超时时间(秒)
  timeOut=$3

  fileNotExistsThrow ${shutdownSh} "用于正常停止程序的脚本[${shutdownSh}]不存在!"

	pid=`ps -ef|grep ${programName}|grep "bootstrap.jar"|awk '{print $2}'`
	if [ ! -n "${pid}" ]
		then
			echo "未监听到[${programName}]程序进程!"
		else
			echo "sh ${shutdownSh}"
			sh ${shutdownSh}

			stopped=0
			i=${timeOut}
			while (($i > 0));do
			
				programIsStop ${pid}
				stopped=$?
				if [ ${stopped} -eq 1 ]
					then
						break
				fi
				
				#sleep
				echo "等待进程[${pid}]结束,剩余时间[${i}s]"
				sleep 1s
				(( i-- )) 
			done
			
			if [ ${stopped} -eq 0 ]
				then
					echo "kill -9 ${pid}"
					kill -9 ${pid}
			fi
	fi
	echo "stopped ${programName}"
}

#程序是否停止
function programIsStop(){
    pid=$1
    result=`ps -ax | awk '{ print $1 }' | grep -e "^${pid}$"`

    if [ ! -n "${result}" ]
		then
    	    return 1
	    else
	        return 0
	fi
}

#脚本入口
shutdownTomcat
cleanAndCopyWar
startTomcat
