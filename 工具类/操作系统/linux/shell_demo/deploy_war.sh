#!/bin/bash

export JAVA_OPTS=""
export CATALINA_HOME="/home/admin/package/apache-tomcat-7.0.65"
export CATALINA_BASE="`pwd`"

# 等待60s
WAIT_TIME=60


# 如果没有提供参数，将第一个参数设置为start
if [ -z "$1" ]; then
    set -- "start"
fi


function stop_tomcat() {
	pid=`ps -ef|grep java |grep ${CATALINA_BASE}|awk '{print $2}'`
	if [ ! -n "${pid}" ]
		then
			echo "未监听到${CATALINA_BASE}程序进程!"
		else
			echo "kill ${pid}"
			kill ${pid}

			stoped=0
			i=${WAIT_TIME}
			while (($i > 0));do
			
				is_top ${pid}
				stoped=$?
				if [ ${stoped} -eq 1 ]
					then
						break
				fi
				
				#sleep
				echo "等待进程[${pid}]结束,剩余时间[${i}s]"
				sleep 1s
				(( i-- )) 
			done
			
			if [ ${stoped} -eq 0 ]
				then
					echo "kill -9 ${pid}"
					kill -9 ${pid}
			fi			
	fi
	echo "stopped"
}

#进程是否停止
function is_top(){
    pid=$1
    result=`ps -ax | awk '{ print $1 }' | grep -e "^${pid}$"`

    if [ ! -n "${result}" ]
		then
    	    return 1
	    else
	        return 0
	fi
}


case $1 in
    start)
        $CATALINA_HOME/bin/catalina.sh start
        echo "start success!!"
    ;;
    stop)
        stop_tomcat
    ;;
    restart)
        stop_tomcat
        $CATALINA_HOME/bin/catalina.sh start
        echo "start success!!"
    ;;
    version)
        $CATALINA_HOME/bin/catalina.sh version
        echo "version success!!"
    ;;
    configtest)
        $CATALINA_HOME/bin/catalina.sh configtest
        echo "configtest success!!"
    ;;
    *)
        echo "Invalid option: $1"
        echo "Usage: $0 {start|stop|restart|version|configtest}"
        exit 1
    ;;
esac

exit 0
