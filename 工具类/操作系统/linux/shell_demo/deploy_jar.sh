#!/bin/bash
#Author:Leon
#create_ts:2021-03-02

#环境
env="uat"
#当前目录
localDir=`pwd`
#项目名称
projectName=`basename ${localDir}`
#运行中的jar名称
oldJar="${localDir}/${projectName}.jar"
#新jar名称(部署前, 部署后会将oldJar备份,并且将newJar重命名为oldJar)
newJar="${localDir}/${projectName}.jar.${env}"
#等待进程结束时间(秒)
waitTime=60

#检查
function checkDeployFile(){
	if [ ! -f "${newJar}" ]; then
		echo "当前目录下[${projectName}.jar.${env}]文件不存在,程序终止!"
		exit 1
	fi
}

#停止项目
function stop(){
	pid=`ps -ef|grep java |grep ${projectName}|awk '{print $2}'`
	if [ ! -n "${pid}" ]
		then
			echo "未监听到${projectName}程序进程!"
		else
			echo "kill ${pid}"
			kill ${pid}

			stoped=0
			i=${waitTime}
			while (($i > 0));do
			
				isStop ${pid}
				stoped=$?
				if [ ${stoped} -eq 1 ]
					then
						break
				fi
				
				#sleep
				echo "等待进程[${pid}]结束,剩余时间[${i}s]"
				sleep 1s
				(( i-- )) 
			done
			
			if [ ${stoped} -eq 0 ]
				then
					echo "kill -9 ${pid}"
					kill -9 ${pid}
			fi			
	fi
	echo "stopped"
}

#进程是否停止
function isStop(){
    pid=$1
    result=`ps -ax | awk '{ print $1 }' | grep -e "^${pid}$"`

    if [ ! -n "${result}" ]
		then
    	    return 1
	    else
	        return 0
	fi
}

#备份文件
function bk(){
	if [ -f "${newJar}" ]
		then
			dateStr="`date +%Y%m%d%H%m`"
			mv "${oldJar}" "${oldJar}.bk.${dateStr}"
			mv "${newJar}" "${oldJar}"
			echo "备份完毕:${oldJar}.bk.${dateStr}"
		else
			echo "跳过备份:路径${newJar}不存在!"
	fi	
}

#将当前进程写入文件
function writePid(){
	if [$1 -eq 0]
		then
			return 0
	fi
	echo `ps -ef|grep java |grep ${projectName}|awk '{print $2}'` > "${localDir}/${projectName}.pid"
}

#启动项目
function start() {
    nohup java -Dlogging.path=${localDir}/logs/ -jar ${oldJar} >/dev/null 2>&1 &
	echo "started"
}

#输出日志
function showLog(){
	logName=${projectName}
	logPath="${localDir}/logs/${logName}_info.log";
	if [ -f "${logPath}" ];
		then
			tail -f ${logPath}
			return 0;
	fi
	
	logName=${projectName//-/}
	logPath="${localDir}/logs/${logName}_info.log";
	if [ -f "${logPath}" ];
		then
			tail -f ${logPath}
			return 0;
	fi
	
	logName=${projectName//-/_}
	logPath="${localDir}/logs/${logName}_info.log";
	if [ -f "${logPath}" ];
		then
			tail -f ${logPath}
			return 0;
	fi
}

#脚本入口
function main(){
	cmd=$1
	if [ ! -n "${cmd}" ]
		then
			cmd="dw"
	fi
	
	case ${cmd} in
	   "stop")
			echo "stopping..."
			stop
	   ;;
	   "start")
			echo "starting..."
			start
	   ;;
	   "restart")
			stop
			start
	   ;;
	   "deploy")
			echo "deploy..."
			checkDeployFile
			stop
			bk
			start
	   ;;
	   "dw")
			echo "deploy and show log"
			checkDeployFile
			stop
			bk
			start
			showLog
	   ;;
	   *) echo "不支持的命令:${cmd}, [start|stop|restart|deploy|dw(默认参数,deploy且显示日志)]"
	   ;;
	esac
	
	#writePid
}

#执行脚本
main $1
