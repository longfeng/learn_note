# 安装
- 参考地址: http://cdn.elephdev.com/Terraform/415.html
## 下载clash for linux,并安装
- git地址: https://github.com/Dreamacro/clash/
- 版本选择 clash-linux-amd64-v1-vXXX.gz/clash-linux-amd64-v3-vXXX.gz
  - 其中V1和V3表示go语言对linux系统的优化, 选择大的即可
```shell
# 下载文件
wget -O clash.gz https://github.com/Dreamacro/clash/releases/download/v1.14.0/clash-linux-amd64-v3-v1.14.0.gz
# 创建目录
mkdir -p /usr/local/clash/bin
# 解压到`/usr/local/bin/clash`目录
gzip -dc clash.gz > /usr/local/clash/bin/clash
# 给与执行权限
chmod +x /usr/local/clash/bin/clash
# 删除源文件
#rm -f clash.gz
```

---
## 配置clash
### 方式1: 通过网络直接下载
```shell
## 配置Country.mmdb
mkdir /usr/local/clash/config/
wget -O /usr/local/clash/config/Country.mmdb https://www.sub-speeder.com/client-download/Country.mmdb
## 配置 config.yaml, 'https://example.com/url'是机场提供的订阅地址
wget -O /usr/local/clash/config/config.yaml 'https://example.com/url'
```
### 方式2: 根据windows中已安装且正常运行的`clash for windows` 配置linux
```shell
## 配置Country.mmdb
mkdir /usr/local/clash/config/
cd /usr/local/clash/config/
## 将windows系统中`$User$\.config\clash\Country.mmdb` 上传到这个目录
## 将windows系统中`$User$\.config\clash\profiles\xxxxx.yml`上传到这个目录,(文件结构参考下文中`关于clash的配置文件说明`)
##     - 建议根据实际情况改造配置文件
```

---
##  [可选]配置clash的external-ui(图形界面)
### 下载ui的编译文件(官方提供的最新编译版本在`gh-pages`这个分支)
- `git clone -b gh-pages --depth 1 https://github.com/Dreamacro/clash-dashboard /usr/local/clash/html/clash-dashboard`
### 修改config.yml文件中的
- `secret` 这是登录时用到的密码,如果配置文件中没有这个配置, 在根节点新增一个即可
- `external-ui` 这是ui的编译文件目录, 与上面的git下载地址对应
### 注意: 访问地址是`http://127.0.0.1:9090/ui`, 其中`127.0.0.1:9090`由config.yml的`external-controller`字段确定

---
## 设置本机代理
1. 新增本机代理配置文件: `vim /etc/profile.d/proxy.sh` 按`i`进行输入, 复制以下内容后, 按`shift+:`,然后输入`wq`进行保存
```shell
export http_proxy="127.0.0.1:7890"
export https_proxy="127.0.0.1:7890"
export no_proxy="localhost, 127.0.0.1"
```
2. 更新代理配置: `source /etc/profile`

---
##  配置clash系统服务,并且启动clash
- 新增服务配置: `vim /etc/systemd/system/clash.service`, 按`i`进行输入, 复制以下内容后, 按`shift+:`,然后输入`wq`进行保存
```
[Unit]
Description=clash

[Service]
Type=simple
User=root
ExecStart=/usr/local/clash/bin/clash -d /usr/local/clash/config/
Restart=on-failure

[Install]
WantedBy=multi-user.target
```
- 刷新系统服务: `systemctl daemon-reload`
- 启动clash: `systemctl start clash`



---
# 其他
## 关于clash的配置文件说明
```yaml
# 聚合代理端口(相当port/socks-port/redir-port的聚合)
mixed-port: 7890

# http/https的代理端口
# port: 7890
# socks的代理端口
# socks-port: 7891
# redir模式的代理端口
# redir-port: 7892

# 是否开启局域网代理, 开启后可以通过配置其他机器指向本机的mixed-port端口
allow-lan: false

# 限定地址, 如果配置了此项且不是*或者0.0.0.0(默认), 则只开放对此ip的访问
#bind-address: '*'

#代理模式(值区分大小写)
#global:所有流量都会走代理
#rule:根据rules中的配置来决定哪些流量走代理
#direct:所有流量都不走代理
#script:根据配置脚本来决定哪些流量走代理
#还有redir与tun模式
mode: rule

# 配置日志等级 trace、debug、info(默认)、warn、error和fatal
# 关闭日志 silent
log-level: info
#设置日志文件路径，可以是绝对路径或相对路径，默认为空，表示输出到控制台。
log-file: /usr/local/clash/log/clash.log
#日志文件的最大保存时间(默认7天)
log-max-age: 1
#设置日志文件的最大大小，单位为 MB，默认为 10 MB。
#log-max-size: 10
#设置日志文件的最大备份数量，默认为 5。
#log-max-backups: 5
#设置日志文件的滚动策略
#log-rotate: daily

# 允许用户通过 TCP 协议连接到 Clash 的控制接口, 用户可以通过自己编写的脚本、应用程序或者第三方工具来实现对 Clash 的控制
# 配置为0.0.0.0:9090时允许所有ip访问, 配置为127.0.0.1:9090时external-ui需要与clash在同一台服务器
external-controller: '0.0.0.0:9090'
#这是external-controller对应的访问秘钥
secret: qq000000
#这是external-controller对应的ui,通过 http://external-controller/ui 来访问
#目前主流的external-ui有`https://github.com/Dreamacro/clash-dashboard`,这里使用此项目编译后的本地目录(也可以直接下载编译好的代码)
#external-ui: /usr/local/clash/html/clash-dashboard/

#dns优化配置
dns:
  enable: false
  ipv6: false
  default-nameserver: [223.5.5.5, 119.29.29.29]
  enhanced-mode: fake-ip
  fake-ip-range: 198.18.0.1/16
  use-hosts: true
  nameserver: ['https://doh.pub/dns-query', 'https://dns.alidns.com/dns-query']
  fallback: ['tls://1.0.0.1:853', 'https://cloudflare-dns.com/dns-query', 'https://dns.google/dns-query']
  fallback-filter: { geoip: true, ipcidr: [240.0.0.0/4, 0.0.0.0/32] }

# 代理服务器信息
proxies:
  - { name: '东京', type: vmess, server: 00000000000000000000000000000000000000000000000000000000000004a.node-for-bigairport.win, port: 443, uuid: 64f2410e-4226-4cd3-a6d5-eb0e14fbe6ae, alterId: 0, cipher: auto, udp: true }
  - { name: '狮城', type: vmess, server: 00000000000000000000000000000000000000000000000000000000000006a.node-for-bigairport.win, port: 443, uuid: 64f2410e-4226-4cd3-a6d5-eb0e14fbe6ae, alterId: 0, cipher: auto, udp: true }
  - { name: '首尔', type: vmess, server: 00000000000000000000000000000000000000000000000000000000000008a.node-for-bigairport.win, port: 443, uuid: 64f2410e-4226-4cd3-a6d5-eb0e14fbe6ae, alterId: 0, cipher: auto, udp: true }
  - { name: '美国西区', type: vmess, server: 00000000000000000000000000000000000000000000000000000000000010a.node-for-bigairport.win, port: 443, uuid: 64f2410e-4226-4cd3-a6d5-eb0e14fbe6ae, alterId: 0, cipher: auto, udp: true }
# 代理服务器分组
proxy-groups:
  - { name: 分组1, type: select, proxies: ['东京','狮城','首尔','美国西区'] }

# 规则 (配置google.com走分组1'
rules:
  - 'DOMAIN-SUFFIX,google.com,分组1'
  - 'MATCH,*,DIRECT'
```
## 问题汇总





https://github.com/Dreamacro/clash-dashboard
http://cdn.elephdev.com/Terraform/415.html
