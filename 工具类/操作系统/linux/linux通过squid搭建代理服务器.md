## [安装squid实现HTTP代理](https://www.cnblogs.com/new_2050/p/7612691.html)
### [可选]先检查是否安装过
`rpm -qa | grep squid`
### 安装squid
`yum -y install squid`

----

## [可选]生成密码
- squid通过文件来指定用户名和密码, 可以自己设定,也可通通过工具设定格式 user_name:password
### 安装apache httpd 用于生成密码
`yum install httpd`
### 生成密码 user_name可以替换成预设的用户名,运行指令后输入两次密码即可
`/usr/bin/htpasswd -c /etc/squid/passwd  user_name`
### 查看生成的用户名和密码
`cat /etc/squid/passwd`

----
## 配置squid
### 备份配置文件
`mv /etc/squid/squid.conf /etc/squid/squid.conf.bk`
### 编辑 新配置文件
`vim /etc/squid/squid.conf`
- 追加需要联网的ip到配置文件中, 加在acl localnet src xxx 后面
  - `acl localnet src 123.125.153.526 # 在此填上你的ip地址,如果是内网,请忽略`
- 修改端口`http_port 3128`
- 追加以下配置 到末尾
```conf
auth_param basic program /usr/lib64/squid/basic_ncsa_auth /etc/squid/passwd
auth_param basic children 5
auth_param basic realm hehe
auth_param basic credentialsttl 2 hours
acl myproxy proxy_auth REQUIRED
http_access allow myproxy
via off
forwarded_for delete
```

----
## 启动Squid
### squid 参数检查
`squid -k parse`
### 初始化缓存
`squid -z`
### 启动服务
`systemctl start squid.service`
### 查看[3128]端口是否已经在运行服务
- `netstat -ntpl`
- 理论上会出现` tcp6       0      0 :::3128                 :::*                    LISTEN      29764/(squid-1)`

### 开机自启动 squid
`systemctl enable squid.service`

### 查看squid日志
`tail -f  /var/log/squid/access.log`

### 测试代理是否生效
1. 为本机配置代理
```shell
# 配置临时http代理
export http_proxy=http://10.0.8.4:3322
# 配置临时https代理
export https_proxy=http://10.0.8.4:3322
```
2. 通过 curl http://www.baidu.com 或者https://www.baidu.com 进行访问测试

### 备注
- 谷歌浏览器代理插件`Proxy SwitchyOmega`, 可通过此插件根据配置的域名来选择性的使用代理服务
- squid 本身暴露出去的连接方式是http的(即主机连接到squid服务器时需要使用http的形式连接), 若需要变成https的方式, 需要结合`stunnel`工具来实现
  - 我并未尝试, `https://blog.51cto.com/anfishr/2463687`
- 还有一种Linux环境搭建代理服务器的工具`TinyProxy`

