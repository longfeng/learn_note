# Windows技巧
## Win10禁用搜索菜单中的网页内容的方法
  - 方法1: 小娜已删除或者禁用,直接修改注册表
    - 注册表(regedit):HKEY_LOCAL_MACHINE \ SOFTWARE \ Policies \ Microsoft \ Windows \ Windows Search
      0. 参考地址:http://www.win10.systems/tips/2017-08-19/6736.html
      1. 新增 AllowCortana 为0
      2. 修改 ConnectedSearchUseWeb 为0
      3. 修改 DisableWebSearch 为1
      4. 重启电脑(很重要)
  - 方法1:Cortana(小娜)未禁用
    - 设置小娜的web搜索方式,再禁用小娜(可选)
---

## windows实用工具
- `Dism++` : win10 功能设置
- `Snipaste` : 截屏工具
- `Unlocker` : 解查看文件,且可以解锁或强制删除
- `CPU-Z/GPU-Z/Aida64` : 查看系统硬件配置(CPU显卡内存)
- `CrystalDiskMark` : 磁盘测试
- `SumatraPDF` : PDF查看
- `ReNamer` : 批量重命名(支持正则)
- `TranslucentTB` : 任务栏透明(可搜汉化)
- `Goldwave` : 音乐剪辑
-  `BandicamPortableNonAdmin` :录屏软件

## windows查看端口占用
- `netstat -ano |findstr "8080"`

## 如何实现本地路由
- 场景: 在A公司的网络环境, 登录B公司的VPN之后, 
- 原因: 登录B公司后, 会默认使用vpn指定的网关做寻址, 这时候导致地址不可用
- 思路: 在本机将A公司的内网IP路由到A公司的默认网关地址
- 方法:
  - 关闭B公司的VPN
  - 通过tracert命令得到A公司的网关地址(第一条记录就是A公司的网关地址)
    - tracert 10.103.16.16
  - 通过route命令将将A公司的网关指定
    - route -p add 10.103.16.0 mask 255.255.255.0 10.100.128.1
      - 10.103.16.0表示整个网段(结合mask)路由到10.100.128.1网关
    - macOS命令: `sudo route add -net 10.103.16.0 -netmask 255.255.255.0 10.100.128.1`