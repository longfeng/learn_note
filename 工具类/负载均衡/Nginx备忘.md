# [nginx中文文档](https://www.nginx.cn/doc/index.html)
## 常用命令
### 启动/停止
```shell
# 编辑
vi /usr/local/nginx-1.16.1/conf/nginx.conf
# 启动 (nginx启动或者使用命令停止时会自动进行语法检查)
/usr/local/nginx-1.16.1/sbin/nginx -c /usr/local/nginx-1.16.1/conf/nginx.conf
ps -ef|grep nginx

# 检查
/usr/local/nginx-1.16.1/sbin/nginx -t
ps -ef|grep nginx

# 停止
kill -QUIT 32198
ps -ef|grep nginx

# 不停机热更新,默认加载conf/nginx.conf
/usr/local/nginx-1.16.1/sbin/nginx -s reload
# 立刻停止服务
/usr/local/nginx-1.16.1/sbin/nginx -s stop
# 优雅停止服务
/usr/local/nginx-1.16.1/sbin/nginx -s quit
# 重新写入日志
/usr/local/nginx-1.16.1/sbin/nginx -s reopen
# 查看版本号
/usr/local/nginx-1.16.1/sbin/nginx -v

```

## 版本选择[下载地址](http://nginx.org/en/download.html)
  - Mainline version主线版本(开发版)
  - Stable version 稳定版本 
  - Legacy versions(历史版本)
  - 备注每个版本链接后面都会带一个pgp链接,包含了用于校验当前版本的加密信息(防篡改)

## 安装步骤[参考地址](https://www.runoob.com/linux/nginx-install-setup.html)

### 安装编译工具及库文件
`yum -y install make zlib zlib-devel gcc-c++ libtool  openssl openssl-devel`
### 安装 PCRE
- ***如果已安装pcre也需要将源码下载下来,等下编译nginx的时候需要用到依赖的库否则会报错
  [/usr/share/doc/pcre-8.32/Makefile Error 127](https://blog.csdn.net/feinifi/article/details/77249225没去深究为什么)***
- 安装命令
```shell
#下载解压
cd /usr/local/src/
wget http://downloads.sourceforge.net/project/pcre/pcre/8.35/pcre-8.35.tar.gz
tar -zxvf pcre-8.35.tar.gz
mv pcre-8.35 /usr/local/

cd /usr/local/pcre-8.35

#如果已安装则跳过此处安装
./configure
sh ./pcre-config --version
```
### 安装nginx centos7.x及以下
```shell
#下载解压
cd /usr/local/src/
wget http://nginx.org/download/nginx-1.16.1.tar.gz
tar -zxvf nginx-1.16.1.tar.gz
mv nginx-1.16.1 /usr/local
cd /usr/local/nginx-1.16.1
# 编译nginx,后缀用到了pcre源码文件夹下的一些类库文件,
./configure --prefix=/usr/local/nginx-1.16.1 --with-http_stub_status_module --with-stream --with-http_ssl_module --with-pcre=/usr/local/pcre-8.35
make
make install

mkdir /usr/local/nginx-1.16.1/logs/
```

### ssh代理
- 增加一个stream模块即可,使用前请确保已安装了nginx的stream模块(就是在./configure 后面带有`--with-stream`参数)
- stream模块与http模块平级,各自独立,需要分配不同端口,因此无法做到80端口既做http又做ssh
  - 我尝试过在http模块的location中通过子路径来跳转到8081端口,但是失败了,或许是http协议无法转换为ssh协议
- 使用时 `ssh xxx.com:8081` 或者 `ssh xxx.com -p8081`
```nginx
#在nginx.conf中追加以下内容即可以实现8081端口的ssh代理
stream { 
  #stream模块，就跟http模块一样 
  upstream ssh {
    #这里IP是虚拟机的，对应虚拟机的IP+Port
    server 192.168.193.12:22;
  }
  server { 
    #里面可以有多个监听服务
    #配置监听端口和代理的ip和端口就可以进行tcp代理了。 
    listen 8081;  #外层通信需要的tcp端口
    proxy_pass ssh;
    proxy_connect_timeout 1h;
    proxy_timeout 1h;
  }
}
```

### 导入https ssl证书
- 若仅本地域名,可以通过自制证书来实现https [自制https ssl证书](制作https证书.md)
```nginx
http{
  ...
  server{
    listen 80;
    listen 443 ssl;
    # 域名, 多个以空格分开
    server_name leon.com;

		# ssl证书地址
		ssl_certificate     /usr/local/nginx/cert/ssl.pem;  # pem文件的路径
		ssl_certificate_key  /usr/local/nginx/cert/ssl.key; # key文件的路径
		
		# ssl验证相关配置
		ssl_session_timeout  5m;    #缓存有效期
		ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4;    #加密算法
		ssl_protocols TLSv1 TLSv1.1 TLSv1.2;    #安全链接可选的加密协议
		ssl_prefer_server_ciphers on;   #使用服务器端的首选算法
		...
	}
}
```

# 配置文件描述
## nginx 文件结构
```nginx

### 全局配置
...        
      
events {}

stream{
  upstream{}
  server{}
}

http{
  upstream{}
  server {
    location [PATTERN]   
    {  ... }
    location [PATTERN]
    { ... }
  }
  
  ### server块
  server { ... }
  
}
```
1. 全局块：配置影响nginx全局的指令。一般有运行nginx服务器的用户组，nginx进程pid存放路径，日志存放路径，配置文件引入，允许生成worker process数等。
2. events块：配置影响nginx服务器或与用户的网络连接。有每个进程的最大连接数，选取哪种事件驱动模型处理连接请求，是否允许同时接受多个网路连接，开启多个网络连接序列化等。
3. http块：可以嵌套多个server，配置代理，缓存，日志定义等绝大多数功能和第三方模块的配置。如文件引入，mime-type定义，日志自定义，是否使用sendfile传输文件，连接超时时间，单连接请求数等。
4. server块：配置虚拟主机的相关参数，一个http中可以有多个server。
5. location块：配置请求的路由，以及各种页面的处理情况。
5. stream块：1.9新增功能,可以实现ssh代理

### 详细配置
```nginx
########### 每个指令必须有分号结束。#################
#配置用户或者组，默认为nobody nobody。
#user administrator administrators; 

#进程数，建议设置为cpu数量
#worker_processes 2;  

#指定nginx进程运行文件存放地址
#pid /nginx/pid/nginx.pid;

# 日志路径,这个设置可以放入全局块，http块，server
# 日志级别：debug|info|notice|warn|error|crit|alert|emerg
error_log log/error.log debug;  

events {
    #设置网路连接序列化，防止惊群现象发生，默认为on
    accept_mutex on;
    #设置一个进程是否同时接受多个网络连接，默认为off
    multi_accept on;
    #事件驱动模型，select|poll|kqueue|epoll|resig|/dev/poll|eventport
    #use epoll;
    #最大连接数
    worker_connections  1024;
}
http {
    #文件扩展名与文件类型映射表
    include       mime.types;   
    #默认文件类型，默认为text/plain
    default_type  application/octet-stream;
    #取消服务日志 
    #access_log off;
    #自定义格式
    log_format myFormat '$remote_addr–$remote_user [$time_local] $request $status $body_bytes_sent $http_referer $http_user_agent $http_x_forwarded_for'; 
    #combined为日志格式的默认值
    access_log log/access.log myFormat;  
    #允许sendfile方式传输文件，默认为off，可以在http块，server块，location块。
    sendfile on;  
    #每个进程每次调用传输数量不能大于设定的值，默认为0，即不设上限。
    sendfile_max_chunk 100k; 
    #连接超时时间，默认为75s，可以在http，server，location块。
    keepalive_timeout 65;  
    #错误页
    error_page 404 https://www.baidu.com; 

    upstream mysvr {   
      server 127.0.0.1:7878;
      server 192.168.10.121:3333 backup;  #热备
    }
    server {
        #单连接请求上限次数。
        keepalive_requests 120; 
        #监听端口
        listen       4545;
        #监听地址  
        server_name  127.0.0.1;        
        #请求的url过滤，正则匹配，~为区分大小写，~*为不区分大小写。
        location  ~*^.+$ {     
          #根目录  
          #root path;  
          #设置默认页
          #index index.html;  
          #请求转向mysvr 定义的服务器列表
          proxy_pass  http://mysvr;  
          #拒绝的ip
          #deny 127.0.0.1;  
          #允许的ip  
          #allow 172.18.5.54;          
        } 
    }
}
```

### 配置目录访问密码
- 使用htpasswd生成密码
```shell
# 例如创建 test_user 并且设置密码(回车之后)输入两次密码即可, 密码文件的路径是/etc/nginx/auth/test_auth
htpasswd -c -d /etc/nginx/auth/test_auth test_user
```
- 配置路径
```shell
location /test/ {
  alias   /home/test/;
  auth_basic "Input password 这里可以是任意字符";
  # 这是密码文件的路径
  auth_basic_user_file /etc/nginx/auth/test_auth;
}
```

### [root和alias的区别](https://www.cnblogs.com/kevingrace/p/6187482.html)
- 在nginx配置中的良好习惯是：
  - 在location /中配置root目录；
  - 在location /path中配置alias虚拟目录。

### nginx 配置上传文件大小限制
设置config中 `http` 结构下的 `client_max_body_size` 参数 例如`client_max_body_size 1024M`