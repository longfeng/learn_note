# [ngrok中文文档](https://ngrok.com/docs)
## 安装
### 配置域名(如果是本机做host则跳过此步骤)
| 主机记录    | 记录类型 | (公网)服务器IP       |
|---------|------|-----------------|
| ngrok   | A    | xxx.xxx.xxx.xxx |
| *.ngrok | A    | xxx.xxx.xxx.xxx |

### 安装环境
- 安装git, golang和openssl
```
yum install -y git golang openssl
```