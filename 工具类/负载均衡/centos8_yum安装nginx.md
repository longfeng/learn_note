## yum方式安装
```shell
# 查看当前系统版本在yum仓库中支持哪些nginx版本
yum module list nginx
# 查看当前系统默认安装了哪个版本的nginx
yum list nginx
# 重置nginx 配置(相当于重装)
yum module reset nginx
# 选择yum仓库中的版本
dnf module enable nginx:1.20
# 安装选择的版本
dnf install nginx

```
## 安装完成后
- nginx的配置文件在`/etc/nginx/nginx.conf`
- 自定义的配置文件放在`/etc/nginx/conf.d`
- 项目文件存放在`/usr/share/nginx/html/`
- 日志文件存放在`/var/log/nginx/`
- 还有一些其他的安装文件都在`/etc/nginx`
- 常见命令
```shell
#启动
systemctl start nginx

#查看状态
systemctl status nginx

#重启nginx
systemctl restart nginx

#停止nginx
systemctl stop nginx

```

## 常见问题
- nginx报错: nginx: [emerg] module “/usr/lib64/nginx/modules/ngx_http_geoip_module.so“ version 1012002
    - https://blog.csdn.net/enthan809882/article/details/118671648