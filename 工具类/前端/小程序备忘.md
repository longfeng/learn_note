bindtap

### block标签 https://www.cnblogs.com/a973692898/p/13131660.html
block只是一个标签：   <block></block>
并且 block 标签是不会被渲染出来的
block只接收控制属性： wx：if   || wx:for
那么它的作用到底是什么？
它的作用就是充当一个容器，类似于 <view></view>

### pen-data标签 https://blog.csdn.net/weixin_42289080/article/details/119953013
open-data 用于展示微信开放的数据
获取拉群头像等信息