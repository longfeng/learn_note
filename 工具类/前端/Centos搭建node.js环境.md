# 搭建node.js环境
|深度|期望|达成|时间|
|---|---|---|---|
|了解|<input type="checkbox" checked/>|<input type="checkbox" checked/>|2022-03-01|
|demo|<input type="checkbox" checked/>|<input type="checkbox" checked/>|2022-03-07|
|总结|<input type="checkbox" />|<input type="checkbox" />|

## 背景
- 了解概念
  - 版本管理工具:
    - nvm: 用于管理nodejs与依赖管理工具(比如npm)的版本升级与降级
  - 包(依赖)管理工具: npm、yarn、cnpm、pnpm
    - npm(node package manager): nodejs默认包含的依赖管理工具, 依赖版本时可能存在不确定性
    - 其他,都是在npm基础上的优化, 安装方式也是通过npm来安装  
      - yarn: 针对npm做的依赖优化, 精确依赖, 且提升编译速度
      - cnpm: 阿里针对npm的封装, 仓库指向国内淘宝的仓库
- 虽然可以直接解压nodejs安装, 但是在执行npm命令时会遇到权限问题(`EACCES: permission denied`), 因此建议使用nvm安装node环境
  - 权限问题: Unable to save binary '/xxx/node_modules/xxx' : Error: EACCES: permission denied, mkdir '/xxx/node_modules/xxx'


## Linux 安装node.js
### 第一步, 安装Nodejs版本管理工具nvm
- 前置说明: 这个工具是非必须的, 但是安装这个工具后可以自由安装和切换nodejs版本
- 官方说明: https://github.com/nvm-sh/nvm#install--update-script
- 通过脚本安装nvm
```shell
# 请先确保网络可以访问github
curl https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh
# 下载并运行install.sh脚本, 出现`Downloading nvm as script to /root.nvm`时表示开始安装
wget -O- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
# 若出现 Failed to download 'https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/xxx', 请重新尝试, 

# 刷新
source ~/.bashrc

# 验证nvm版本
nvm -v
```
### 第二步, 通过nvm安装nodejs, 此版本会自动安装npm
```shell
## 安装node版本
nvm install 12
## nvm install node, nvm会安装最新的release版本
## nvm install 12.22.10, nvm会安装12.22.10版本
## nvm install 12, nvm会自动安装12版本的last release版本
```


### nvm 常用命令
```shell
# 列出本机已安装的所有版本
nvm ls

# 列出所以远程服务器的版本（官方node version list）
nvm ls-remote

# 显示当前的版本
nvm current

# 切换当前版本
# vesion 当值为node时,表示使用最新版本
# vesion 当值为16时, 表示使用16.xx.xx最新版本
# vesion 当值为16.13时, 表示使用16.13.xx最新版本
# vesion 当值为16.13.2时, 表示使用16.13.2版本
nvm use [vesion]

# 安装指定版, 
# vesion 语义请参考nvm use [version]
nvm install [vesion]

# 删除已安装的指定版本
# vesion 语义请参考nvm use [version]
nvm uninstall [vesion]
```

## 安装yarn[可选], 基于nvm安装nodejs后
```shell
# 全局安装yarn
npm install yarn -g
# 验证版本
yarn -v
# 使用yarn加载全部项目依赖
#yarn
```
