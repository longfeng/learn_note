## [官网文档](https://arthas.aliyun.com/doc/commands.html)
### 安装
- 可访问aliyun[推荐]
    - `curl -O https://arthas.aliyun.com/arthas-boot.jar`
- 不可访问aliyun
    - 下载zip包：`wget -O arthas-latest.zip https://arthas.aliyun.com/download/latest_version?mirror=aliyun`
    - 解压 `unzip`

### 使用
1. `java -jar arthas-boot.jar`
2. 选择线程对应的数字,然后回车

### 线程/JVM/Runtime
- dashboard

### 打印线程ID 1的栈
- thread 1 [|grep xx]

### 方法耗时
- 示例
    - `trace com.package.ClassName methodName`
- 示例 只显示大于10毫秒执行
    - `trace com.package.ClassName methodName '#cost > 10'`

### 方法出入参
- 示例
    - `watch com.package.ClassName methodName`
- 示例 显示参数与返回值(通过OGNL变量，参考下方）
    - `watch com.package.ClassName methodName '{params,returnObj}' -x 2`
- 示例 显示sql
    - `watch com.leon.ccs.core.mybatis.plugins.PerformanceInterceptor getSql '{returnObj}' -v -n 5 -x 3 '1==1'`

### 方法的调用父路径（递归）
- 示例
  stack com.leon.mcu.ecu.admin.impl.biz.impl.ShopBizImpl listShopPage -n 3
    - `stack com.package.ClassName methodName -n 2`
- 示例 只显示第一个参数小于零的执行
    - `stack com.package.ClassName methodName 'params[0]<0' -n 2`

### 方法调用次数统计
- 示例：300秒钟统计一次
    - `monitor -c 300 com.package.ClassName methodName`
- 示例：300秒钟统计一次 并且过滤xxx
    - `monitor -c 300 com.package.ClassName methodName`
- 参数：
    - 参数名称		参数说明
    - `class-pattern`	类名表达式匹配
    - `method-pattern`	方法名表达式匹配
    - `condition-express`	条件表达式
    - `[E]`		开启正则表达式匹配，默认为通配符匹配
    - `[c:]`	统计周期，默认值为120秒
    - `[b]`		在方法调用之前计算condition-express
- 监控的维度说明
    - 监控项	说明
    - `timestamp`	时间戳
    - `class`	Java类
    - `method`	方法（构造方法、普通方法）
    - `total`	调用次数
    - `success`	成功次数
    - `fail`	失败次数
    - `rt`	    平均RT
    - `fail-rate`	失败率

### ognl表达式
- 变量名	变量解释
- `loader`	本次调用类所在的 ClassLoader
- `clazz`	本次调用类的 Class 引用
- `method`	本次调用方法反射引用
- `target`	本次调用类的实例
- `params`	本次调用参数列表，这是一个数组，如果方法是无参方法则为空数组
- `returnObj`	本次调用返回的对象。当且仅当 isReturn==true 成立时候有效，表明方法调用是以正常返回的方式结束。如果当前方法无返回值 void，则值为 null
- `throwExp`	本次调用抛出的异常。当且仅当 isThrow==true 成立时有效，表明方法调用是以抛出异常的方式结束。
- `isBefore`	辅助判断标记，当前的通知节点有可能是在方法一开始就通知，此时 isBefore==true 成立，同时 isThrow==false 和 isReturn==false，因为在方法刚开始时，还无法确定方法调用将会如何结束。
- `isThrow`	辅助判断标记，当前的方法调用以抛异常的形式结束。
- `isReturn`	辅助判断标记，当前的方法调用以正常返回的形式结束。

### 常见问题
- java -jar arthas-boot.jar自动更新的包在哪个目录下?
  - /root/.arthas
  - 如果想要在容器里面使用, 将一个读写目录挂载到到容器的/root/.arthas
  - 示例:
    - -v /data/arthas:/home/admin/arthas
    - -v /data/arthas/.arthas:/root/.arthas
- target process not responding or HotSpot VM not loaded
  - 先查出要监听的java进程对应的用户，切换到这个用户再用arthas进行监听
- The telnet port 3658 is used by process 1135 instead of target process 2621, you will connect to an unexpected process.(进程 1135 使用 telnet 端口 3658 而不是目标进程 2621，您将连接到意外进程。)  
  1. Try to restart arthas-boot, select process 1135, shutdown it first with running the 'stop' command.(1.尝试重新启动arthas-boot，选择进程1135，首先运行'stop'命令将其关闭。)
  - 讲人话: 上一次arthas没有正常关闭, 端口还被1135这个进程占用着, 建议重新启动arthas-boot, 重新连接到1135上, 然后执行`stop`命令释放这个端口就可以了
- 配合idea arthas插件生产arthas命令
- watch命令字段不显示?
  - arthas 输出的对象时调用的是对象的toString方法,所以字段需要包含在toString方法中
- 怎么监听mybatis中mapper的方法?
  - 他们之间没有区别, 直接从mapper.java中生成命令执行即可
- 如何查看源码
  - 通过jad命令: 例如: `jad com.package.ClassName methodName`
  - 输出示例
  ```
  private void methodName() {
  /*101*/    int param = 1;
  /*102*/    System.out.printf("param:"+param);
  }
  ```
- 如何查看源码? 在arthas命令失效的情况下, 可以通过cfr查看jar中的源码
  - 下载` wget https://www.benf.org/other/cfr/cfr-0.152.jar `
  - 命令: java -jar cfr-0.152.jar <jar路径> <class路径> |grep -A 50 <方法名>
      - 参数: jar路径: /home/tomcat/webapps/ccs-web/WEB-INF/lib/test.jar
      - 参数: class路径: com/leon/ccs/common/web/test.java
      - 参数: 方法名: "findPersonByCustomer"
