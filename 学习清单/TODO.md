
# ELK日志采集
|深度|期望|达成|时间|
|---|---|---|---|
|了解|<input type="checkbox" checked/>|<input type="checkbox" />|
|demo|<input type="checkbox" />|<input type="checkbox" />|
|总结|<input type="checkbox" />|<input type="checkbox" />|

# j2cache
|深度|期望|达成|时间|
|---|---|---|---|
|了解|<input type="checkbox" checked/>|<input type="checkbox" />|
|demo|<input type="checkbox" />|<input type="checkbox" />|
|总结|<input type="checkbox" />|<input type="checkbox" />|

# rocketmq 4.7.1
|深度|期望|达成|时间|
|---|---|---|---|
|了解|<input type="checkbox" checked/>|<input type="checkbox" />|
|demo|<input type="checkbox" />|<input type="checkbox" />|
|总结|<input type="checkbox" />|<input type="checkbox" />|

# shiro 1.4.0
|深度|期望|达成|时间|
|---|---|---|---|
|了解|<input type="checkbox" checked/>|<input type="checkbox" />|
|demo|<input type="checkbox" />|<input type="checkbox" />|
|总结|<input type="checkbox" />|<input type="checkbox" />|

# swagger ??
|深度|期望|达成|时间|
|---|---|---|---|
|了解|<input type="checkbox" checked/>|<input type="checkbox" />|
|demo|<input type="checkbox" />|<input type="checkbox" />|
|总结|<input type="checkbox" />|<input type="checkbox" />|

# mongodb ??
|深度|期望|达成|时间|
|---|---|---|---|
|了解|<input type="checkbox" checked/>|<input type="checkbox" />|
|demo|<input type="checkbox" />|<input type="checkbox" />|
|总结|<input type="checkbox" />|<input type="checkbox" />|

# 拆分多个微服务之后, 数据库怎么设计
- 怎么冗余字段?
    - 比如订单关联商品, name订单中保存商品的什么信息?只保存id还是code name都保存?
        - 基础数据通过缓存来提供快速组装的能力??? 

# 了解linux磁盘挂载机制

# gitlab-runner 怎么工作的?
- 它会在/home/gitlab-runner/build下面生成一个编译目录, 但是我不同的分支在跑job的时候怎么区分的不同的目录?

# linux 环境变量


# 开源项目
jvm-sandbox
Star 5.3k
Fork 1.4k
Watch 387
一种 JVM 的非侵入式运行期 AOP 解决方案。简单点说就是如果线上 Java 服务出现故障，需要加一条日志定位问题，通过该项目可以在不重新部署服务的情况下，完成增加日志的操作。它还支持线上故障模拟、请求录制和结果回放等功能。