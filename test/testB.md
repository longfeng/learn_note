# 测试多远程分支提交文件.
## 对象
- A客户端(乙方 连接 A库)
- B客户端(甲方 连接 B库)
- C客户端(堡垒机 连接A库和B库)
## 测试项(期待结果:双方库文件内容一致)
- A客户端提交新文件testA.md,并push到A库, C客户端接收A库并push到B库
- B客户端提交新文件testB.md,并push到B库, C客户端接收B库并push到A库
- A客户端提交修改testA.md第8行,并push到A库,B客户端提交修改testA.md第8行,并push到B库,  C客户端接收A库, C客户端处理冲突并push到B库, C客户端接收B库并push到A库,
