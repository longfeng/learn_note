# Java
## 基础知识
### ☆ [计算机网络](基础知识/计算机网络.md)
- 4/7层网络模型
- TCP协议(3次握手/4次挥手)✔
- UDP
- HTTP
  - 状态码(200/404/503)
  - 协议(MethodCookies/Header)
  - Https原理
  - Http2/3
### ☆ [操作系统](基础知识/操作系统.md)
- 线程与进程
  - 区别联系
  - 线程调度
  - 线程切换步骤
  - Linux下的IPC
  - 协程(用户级,比进程切换代价小)
- Linux常用命令
  - awk
  - top ✔
  - netstat`netstat -anp` ✔
  - grep ✔
  - less
  - tail `tail -100f -A10 -B20 /test.log` ✔
  - 管道  ✔
- 死锁
- 内存分页管理与swap
### ☆ [数据结与算法](基础知识/数据结与算法.md)
- 数据结构
  - ☆表(数组/单链表/双向链表/散链表)
  - 栈(先进先出/后进先出)
  - 队列
  - 图(有向图/无向图/带权图)
  - 二叉树(平衡二叉树/红黑树/)
  - ☆多叉树(B树/B+树/字典树)
- 算法
- 常用算法思路(分治/动态规划/贪心/回溯/分支界定)
- 复杂度(时间复杂度/空间复杂度)
- 字符串匹配(BF/BM/Sunday/KMP/Tire等算法)
- 排序(插入(希尔/直播)/交换(冒泡/快速排序)/选择(简单选择/对)/归并/基数)
- 查找(二分法/二叉排序树/B树/Hash/BloomFilter)
### ☆☆ [java语言特性](基础知识/java语言特性.md)
- ☆Java版本特性
  - ☆1.8 (Lambda/Stream/长期支持版)
  - 1.9-1.10 (G1/模块系统/接口私有方法)
  - ☆1.11 (ZGC/长期支持版)
- 动态代理与反射
- ☆线程常用工具类（JUC）
- ☆常用集合
  - ☆HashMap
  - ☆ConcurrentHashMap
  - ArrayList&LinkedList
  - HashSet
  - TreeMap
- 对象引用
  - 强引用
  - 弱引用
  - 软引用
  - 虚引用
- 数据类型
### ☆☆ [多线程](基础知识/多线程.md)
- 机制
  - ThreadLocal
  - Fork/Join
  - Volatile
  - Interrupt
- ☆☆线程池
- ☆☆同步与互斥
- ☆死锁
- 线程状态转换
- 线程通信
- ☆线程常用工具类（JUC）
  - ConcurrentXXX
  - AtomicXXX
  - Executor
  - Caller&&Future
  - Queue
  - Locks
### ☆☆ [jvm](基础知识/jvm.md)
- 编译器优化
  - 指令重排
  - 内联
  - 逃逸分析
- ☆☆jvm内存模型
  - 其他:方法区/程序计数器
  - ☆☆堆:Eden/s1/s0/Old/
  - 栈
- 类加载器
  - 双亲委派(Bootstrap类加载器/Extension类加载器/System类加载器/自定义类加载器)
- ☆☆垃圾回收算法
- ☆cms/g1(JDK1.7+支持 JDK9默认)/zgc
- ☆☆JVM调优 JVM参数/ arthas / jstat /jmap / 
### ☆☆ [设计模式](基础知识/设计模式.md)
- 单例模式 ✔
  - 静态初始化(饿汉模式)/双重检查(懒汉模式 volatile)/单例注册表
- 工厂模式 spring创建bean
- 代理模式 缺
- 责任链模式 spring拦截器
- 构造者模式 slf4j如何支持log4j
- 适配器模式 Grpc如何支持流式请求
- 观察者模式 pb序列化中的builder

## ☆☆☆架构能力
### Spring
### Dubbo
### Mybatis
### 消息队列-Kafka
### 消息队列-RocketMQ
### 数据库-Mysql
### 缓存-Redis
### Docker
### ZK
### SC
### netty

## 项目能力f
### 项目描述/难点/问题/改进

## 应用能力
### ☆☆ [常用工具](应用能力/常用工具.md)
- 容器与编排
  - docker
  - k8s/k3s
  - rancher
- 团队协作
  - Ant
  - Maven
  - Gradle
  - Git
  - Svn
- 质量保证
  - Checkstyle
  - FindBugs
  - SonarQube
- 压测
  - JMeter
  - JMH
  - AB
  - LoadRunner
- ServLet容器与代理
  - Tomcat
  - Jetty
  - Nginx
  - Envoy
  - OpenResty
  - Kong
- CI/CD
  - Gitlab-CI
  - Jenkins
  - Travls
  - CircleCI
- JVM相关
  - Arthas
  - JMC(JFR)
  - jstack/jmap/jstat
  - jinfo
  - jcmd
  - btrace
  - MAT
- 系统分析
  - vmstat
  - iostat & lotop
  - ifstat & iftop
  - netstat
  - dstat
  - strace
  - GDB
  - lsof
  - tcpdump
  - traceroute
- 文档管理
  - javaDoc
  - Swagger
  - Yapi
- 网络工具
  - PostMan
  - WireShark
  - Fiddler
  - Charles




> 有个好插件https://markmap.js.org/docs/packages--markmap-cli


## 扩展
### vue
- ☆UniApp